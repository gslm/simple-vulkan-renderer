# Simple Vulkan Renderer

Simple Vulkan Renderer is an application created solely to introduce myself to
the Vulkan API using the vulkan-hpp C++ bindings.  The scope of the project is
limited to load and render a mesh with a simplified PBR material (hardcoded
solid material, no textures) using image based lighting.

## Usage 
```
svk-renderer options flags
```
### Options

* **-m** mesh\_path
  
   Required. Specifies the mesh file. Any format supported by the assimp
   library (<https://github.com/assimp/assimp/>) should be good.
   
* **-e** env\_map\_path

  Required. Specifies the environment map file. Environment files must be a
  equirectangular projection preferably in hdr format. A good collection of
  valid enviroment maps can be found here: <https://hdrihaven.com/hdris/>.

* **-w** width**x**height

  Optional. If present the application opens in a window sized as indicated by
  width and height. Otherwise it opens in fullscreen at native resolution.  Note
  that width and height must be strictly positive integers. Also note that space
  between the values and the 'x' character is not allowed. 

* **-h**

  Optional. Prints the usage message and exits.

### Flags

* **-\-fullscreen**

  Force fullsceen when **-w** option is present. Useful when the desired full
  screen resolution differs form the native. 

## Build

Build is done using the meson build system. Run the following commands in
the project's root directory.  
``` 
$ meson build 
$ ninja -C build
``` 
If no errors are emitted an executable named svk-renderer should be created
inside the build directory.

### Tools

* meson build system. 
* c++ compiler. Must fully support c++17 (clang 8 and gcc 9.2 are known to
  work). 
* glslLangValidator.


### Dependecies

* Assimp
* OpenImageIO
* GLFW3
* Vulkan with vulkan-hpp
* GLM

## Implementation Overview

As stated before the main objective of this project was to get a first contact
with the Vulkan API. Because of that, achieving maximum performance was not a
priority, nevertheless some work and experimentation has been put in that
regard. 

### Renderer

The renderer uses a single render pass on a framebuffer with a color and a
depth-stencil attachment. 

Commands are recorded once and reused. Render commands are split in two parts.
A primary command where the render pass is specified and the secondary commands
where all the render commands are recorded.

Two uniform buffers are allocated for scene update. That way concurrent scene
update is allowed. This implies that, because render command buffers are
recorded once, two render command buffers must be allocated, one for each
uniform buffer.

Because only a render pass is used, the interface with the presentation engine
is done by copying the color attachment onto the acquired swapchain image every
frame.

### Resource Loading

Resource loading (vertex data and textures) is done using a ping pong memory
scheme. A relatively small fixed amount of host-visible memory is allocated and
reused. The memory is split in two chunks so copy operations, witch will be
split in as many sub-transactions as needed, are somewhat concurrent between
the host and the device. Note that in order to move data from system memory to
video memory two transactions are needed (system to host-visible followed by
host-visible to device).

Another solution would have been using a ring buffer. Similar idea but more
flexible. Also not so easy to implement.

### Image Based Lighting Precomputation

The image base lighting (IBL for short) method implemented is the one described
here <https://learnopengl.com/PBR/IBL/Specular-IBL>. All the computations are
done using compute shaders.

