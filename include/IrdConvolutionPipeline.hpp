#ifndef IRD_CONVOLUTION_PIPELINE_HPP
#define IRD_CONVOLUTION_PIPELINE_HPP

#include <vulkan/vulkan.hpp>

class IrdConvolutionPipeline {
  vk::PipelineLayout pipeline_layout_;
  vk::Pipeline pipeline_;

public:
  static constexpr uint32_t local_size_x = 1 << 4;
  static constexpr uint32_t local_size_y = 1 << 4;

  IrdConvolutionPipeline();
  IrdConvolutionPipeline(
      vk::Device device,
      vk::DescriptorSetLayout descriptor_set_layout);

  operator vk::Pipeline() const;

  vk::PipelineLayout layout() const;

};

#endif //IRD_CONVOLUTION_PIPELINE_HPP
