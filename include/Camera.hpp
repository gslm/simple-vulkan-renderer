#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <glm_user_flags.hpp>
#include <glm/glm.hpp>

#include <vkwapp/App.h>

#include <glm/gtc/quaternion.hpp>

class Camera {
  glm::mat4 projection_;

  glm::quat orientation_;
  glm::vec3 translation_;
  
  glm::vec3 target_position_;
  float dist_;

  float aspect_;
  float focal_length_;

public:
  Camera();
  Camera(float fov, float aspect, float near, float far);

  void set_target(const glm::vec3& target, float dist);

  void update(
      const vkwapp::App::Input& input, 
      const vkwapp::App::Time& time);

  float aspect();
  float focal_length();
  glm::vec3 position();
  glm::quat orientation();
  glm::mat4 world();
  glm::mat4 view();
  glm::mat4 projection();
};


#endif //CAMERA_HPP

