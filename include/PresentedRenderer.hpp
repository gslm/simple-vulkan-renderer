#ifndef VKUT__PRESENTED_RENDERER_HPP
#define VKUT__PRESENTED_RENDERER_HPP

#include <vulkan/vulkan.hpp>

#include <DeviceManager.hpp>

#include <array>
#include <vector>
#include <bitset>

namespace PresentedRenderer_detail {
  struct AttachmentResource {
    vma::Allocation allocation;
    vk::Image image;
    vk::ImageView view;
  };

  struct SubmitResources {
    vk::Semaphore swapchain_acquire_semaphore;
    vk::Semaphore copy_semaphore;
    vk::Semaphore present_acquire_semaphore;
    vk::Fence fence;
  };

}

class PresentedRenderer {

  using AttachmentResource = PresentedRenderer_detail::AttachmentResource;
  using SubmitResources = PresentedRenderer_detail::SubmitResources;

  static constexpr size_t RenderCmdSetInstances = 2;

public:
  static constexpr size_t QueuedFramesCap = 2;

private:
  vk::Device device_;
  vma::Allocator alloc_;
  vk::Queue graphics_queue_;
  vk::Queue present_queue_;
 
  std::bitset<32> state_bits_;
  /*struct {
    uint32_t cmd_submited_ : 1;
    uint32_t unused_bits_ : 31;
  } state_bits_;*/

  uint32_t current_cmd_set_;

  std::array<
    vk::CommandBuffer, QueuedFramesCap*RenderCmdSetInstances> render_cmd_sets_;
  std::array<SubmitResources, QueuedFramesCap> submit_resources_;

  vk::RenderPassBeginInfo render_pass_begin_info_;
  std::array<vk::ClearValue, 2> clear_values_;

  std::vector<vk::CommandBuffer> copy_cmds_;
  std::vector<vk::CommandBuffer> present_acquire_cmds_;

  vk::CommandPool graphics_command_pool_;
  vk::CommandPool present_command_pool_;

  vk::SwapchainKHR swapchain_;
  std::array<AttachmentResource, 2> attachments_;

public:
  PresentedRenderer(
      DeviceManager& device_manager, 
      vk::Extent2D desired_output_extent);
  
  ~PresentedRenderer();

  std::vector<vk::CommandBuffer> AllocateCommandBuffers(
      uint32_t count, 
      vk::CommandBufferUsageFlags usage = vk::CommandBufferUsageFlags());

  void FreeCommandBuffers(
      const vk::ArrayProxy<const vk::CommandBuffer>& command_buffers);

  vk::RenderPass render_pass();
  vk::Extent2D output_extent();

  void SetColorClearValue(std::array<float, 4> color);
  void SetDepthClearValue(float depth);
  void SetStencilClearValue(uint32_t stencil);

  void SetCommandBuffersSet(
      vk::Fence fence,
      const std::array<
        vk::ArrayProxy<const vk::CommandBuffer>, 2>& command_buffers_set);
/* 
  void SetCommandBuffersSet(
      const vk::ArrayProxy<const vk::CommandBuffer>& command_buffers_set);
*/
  void SubmitRender(
      uint32_t commnad_index,
      vk::Fence fence,
      vk::Semaphore semaphore,
      const vk::ArrayProxy<const vk::Semaphore>& waitSemaphores,
      const vk::ArrayProxy<const vk::PipelineStageFlags>& waitMasks);
  
  vk::Result SubmitPresent(
      uint32_t command_index, 
      vk::Semaphore render_semaphore);

};

#endif //VKUT__PRESENTED_RENDERED_HPP
