#ifndef VKWAPP__DETAIL__APP_INTERNAL_H
#define VKWAPP__DETAIL__APP_INTERNAL_H

#include <vkwapp/App.h>
#include <vkwapp/detail/TimeInternal.h>

#include <GLFW/glfw3vk.h>

#include <vulkan/vulkan.hpp>

namespace vkwapp {

namespace detail {

class AppInternal {
  TimeInternal time_;
  GLFWwindow* window_;
  vk::Instance instance_;
  vk::SurfaceKHR surface_;

public:
  AppInternal(
    const App::Args& args,
    const std::vector<const char*>& extencions,
    const std::vector<const char*>& layers);
  ~AppInternal();

  GLFWwindow* window() const;
  vk::Instance instance() const;
  vk::SurfaceKHR surface() const;
  const TimeInternal& time() const;

  App public_interface();
  void update();
  bool should_close() const;
};

}


}
#endif
