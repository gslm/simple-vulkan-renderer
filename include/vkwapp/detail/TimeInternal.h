#ifndef VKWAPP__DETAIL__TIME_INTERNAL_H
#define VKWAPP__DETAIL__TIME_INTERNAL_H

#include <chrono>
#include <type_traits>

namespace vkwapp {
	namespace detail {

		class TimeInternal {
			using clock = std::chrono::steady_clock;
			int64_t frames_;

			std::chrono::time_point<clock> start_;
			std::chrono::time_point<clock> before_;
			std::chrono::time_point<clock> now_;

		public:
			TimeInternal()
				: frames_(0)
				, start_(clock::now())
				, before_(start_)
				, now_(start_)
			{}

			void reset() {
				frames_ = 0;
				start_ = clock::now();
				before_ = start_;
				now_ = start_;
			}

			void update() {
				before_ = now_;
				now_ = clock::now();
				++frames_;
			}

			template<class Duration>
			std::enable_if_t<not std::is_floating_point_v<Duration>,
				Duration> time() const {
				using namespace std::chrono;
				return duration_cast<Duration>(now_ - start_);
			}

			template<class Float>
			std::enable_if_t<std::is_floating_point_v<Float>,
				Float> time() const {
				using namespace std::chrono;
				return duration_cast<duration<Float>>(now_ - start_).count();
			}

			template<class Duration>
			std::enable_if_t<not std::is_floating_point_v<Duration>,
				Duration> delta() const {
				using namespace std::chrono;
				return duration_cast<Duration>(now_ - before_);
			}

			template<class Float>
			std::enable_if_t<std::is_floating_point_v<Float>,
				Float> delta() const {
				using namespace std::chrono;
				return duration_cast<duration<Float>>(now_ - before_).count();
			}

			template<class Duration>
			std::enable_if_t<not std::is_floating_point_v<Duration>,
				Duration> now() const {
				using namespace std::chrono;
				return duration_cast<Duration>(clock::now() - start_);
			}

			template<class Float>
			std::enable_if_t<std::is_floating_point_v<Float>,
				Float> now() const {
				using namespace std::chrono;
				return duration_cast<duration<Float>>(clock::now() - start_).count();
			}

			int64_t frames() const {
				return frames_;
			}
		};

	}
}
#endif