#ifndef VKWAPP__VKWAPP_H
#define VKWAPP__VKWAPP_H

#include <vkwapp/detail/AppInternal.h>
#include <vkwapp/App.h>

#include <vector>
namespace vkwapp {

template<class Logic>
int run_app(const App::Args& app_args, const typename Logic::Args& args)
{	
  detail::AppInternal app_internal(app_args, Logic::extensions(), Logic::layers());
	App app = app_internal.public_interface();

	Logic logic(app, args);
	logic.start(app);

	while (!app_internal.should_close()) {
		app_internal.update();
		if (!logic.loop(app)) break;
	}

	return logic.end(app);
}

}


#endif
