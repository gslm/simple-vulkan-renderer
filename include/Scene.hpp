#ifndef VKUT__SCENE_HPP
#define VKUT__SCENE_HPP

#include <glm_user_flags.hpp>
#include <vkwapp/App.h>
#include <DeviceManager.hpp>

#include <PbrPipeline.hpp>
#include <EnvPipeline.hpp>

#include <TextureLoader.hpp>
#include <MeshLoader.hpp>

#include <Camera.hpp>

namespace Scene_detail {

struct BufferAlloc {
  vk::Buffer buffer;
  vma::Allocation allocation;
};

struct SampledImage {
  vk::Image image;
  vma::Allocation allocation;
  vk::ImageView view;
  vk::Sampler sampler;
};

} //Scene_detail;


class Scene {
  using BufferAlloc = Scene_detail::BufferAlloc;
  using SampledImage = Scene_detail::SampledImage;
  using Mesh = MeshLoader::Mesh;
  using Texture = TextureLoader::Texture;

  const vk::Device device_;
  vma::Allocator alloc_;
  MeshLoader mesh_loader_;
  TextureLoader texture_loader_;

  vk::Queue graphics_queue_;
  uint32_t graphics_family_;

  vk::PipelineCache pipeline_cache_;
 
  vk::DescriptorSetLayout env_descriptor_set_layout_;
  vk::DescriptorSetLayout transform_descriptor_set_layout_;
  vk::DescriptorSetLayout view_descriptor_set_layout_;

  vk::DescriptorSet env_descriptor_set_;
  std::array<vk::DescriptorSet, 2> transform_descriptor_set_;
  std::array<vk::DescriptorSet, 2> view_descriptor_set_;

  PbrPipeline pbr_pipeline_;
  EnvPipeline env_pipeline_;

  std::array<BufferAlloc, 2> uniform_buffer_allocs_;

  vk::DescriptorPool descriptor_pool_;
  
  //Texture envmap_texture_;

  SampledImage env_sampled_image_;
  SampledImage ird_sampled_image_;
  SampledImage brdf_sampled_image_;
 
  float env_miplevel_count_;

  Camera camera_;
  Mesh mesh_;

public:
  struct SceneDescription {
    const char* model_path;
    const char* env_path;
  };

  Scene(
      DeviceManager& device_manager,
      const vk::Extent2D render_extent,
      const vk::RenderPass render_pass,
      const SceneDescription& scene_description); 
 
  ~Scene();

  void RecordRenderCommands(const std::array<vk::CommandBuffer, 2>& cmds);
  void Update(
      uint32_t buffer_index,
      vk::Fence wait_fence,
      const vkwapp::App::Input& input, 
      const vkwapp::App::Time& time);
};

#endif //VKUT_SCENE_HPP
