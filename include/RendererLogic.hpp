#ifndef RENDERER_LOGIC_HPP
#define RENDERER_LOGIC_HPP

#include <vkwapp/App.h>

#include <vulkan/vulkan.hpp>

#include <Scene.hpp>
#include <PresentedRenderer.hpp>
#include <DeviceManager.hpp>

#include <array>
#include <memory>

namespace RenderLogic_detail {
  
struct RenderSyncResources {
  vk::Semaphore semaphore;
  vk::Fence fence;
};

} // namespace RenderLogic_detail

class RendererLogic {
  using RenderSyncResources = RenderLogic_detail::RenderSyncResources;

  static constexpr auto QueuedFramesCap = PresentedRenderer::QueuedFramesCap;
  static_assert(QueuedFramesCap == 2);
  
  DeviceManager device_manager_;
  PresentedRenderer renderer_;
  Scene scene_;
  
  uint64_t frame_;
  std::array<RenderSyncResources, QueuedFramesCap> render_sync_resources_;

  std::vector<vk::CommandBuffer> render_commands_;

  std::unique_ptr<vk::DispatchLoaderDynamic> instance_dynamic_dispatch_;

#if !NDEBUG
  vk::DebugUtilsMessengerEXT debug_messenger_;
#endif

public:
  struct Args {
    const char* model_path;
    const char* env_path;
  };

  RendererLogic(vkwapp::App& app, const Args& args) noexcept;
  ~RendererLogic();

  void start(vkwapp::App& app);
  bool loop(vkwapp::App& app);
  int end(vkwapp::App& app);

  static std::vector<const char*> layers() {
    return {
#if !NDEBUG
      "VK_LAYER_LUNARG_standard_validation"
#endif
    };
  }

  static std::vector<const char*> extensions() {
    return {
#if !NDEBUG
      VK_EXT_DEBUG_UTILS_EXTENSION_NAME
#endif
    };
  } 
  
  static vk::PhysicalDeviceFeatures features() {
    return vk::PhysicalDeviceFeatures()
      .setShaderStorageImageExtendedFormats(vk::Bool32(true));
  };

};

#endif //RENDERER_LOGIC_HPP
