#ifndef PBR_PIPELINE_HPP
#define PBR_PIPELINE_HPP

#include <vulkan/vulkan.hpp>
#include <glm_user_flags.hpp>
#include <glm/glm.hpp>

class PbrPipeline {
  vk::PipelineLayout pipeline_layout_;
  vk::Pipeline pipeline_;

public:
  using position_type = glm::vec3;
  using normal_type = glm::vec3;

  PbrPipeline();
  PbrPipeline(
      vk::Device device,
      vk::PipelineCache pipeline_cache,
      vk::DescriptorSetLayout envmap_descriptor_set_layout,
      vk::DescriptorSetLayout transform_descriptor_set_layout,
      vk::Extent2D render_extent,
      vk::RenderPass render_pass);


  //operator=(const PbrPipeline&) = default;

  operator vk::Pipeline() const;

  vk::PipelineLayout layout() const;
};


#endif //PBR_PIPELINE_HPP
