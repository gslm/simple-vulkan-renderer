#ifndef BRDF_INTEGRATION_PIPELINE_HPP
#define BRDF_INTEGRATION_PIPELINE_HPP

#include <vulkan/vulkan.hpp>

class BrdfIntegrationPipeline {
  vk::PipelineLayout pipeline_layout_;
  vk::Pipeline pipeline_;

public:
  static constexpr uint32_t local_size_x = 1 << 4;
  static constexpr uint32_t local_size_y = 1 << 4;

  BrdfIntegrationPipeline();
  BrdfIntegrationPipeline(
      vk::Device device,
      vk::DescriptorSetLayout descriptor_set_layout);

  operator vk::Pipeline() const;

  vk::PipelineLayout layout() const;
};


#endif //BRDF_INTEGRATION_PIPELINE_HPP
