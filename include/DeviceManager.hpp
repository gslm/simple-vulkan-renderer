#ifndef DEVICE_MANAGER_HPP
#define DEVICE_MANAGER_HPP

#include <vulkan/vulkan.hpp>
#include <vma/Allocator.hpp>

#include <vector>


class DeviceManager {

  using command_pool_t = 
    std::tuple<vk::CommandPool, vk::CommandPoolCreateFlags>;

  vk::PhysicalDevice pdevice_;
  vk::SurfaceKHR surface_;
  vk::Device device_;
  vma::Allocator allocator_;

  std::vector<uint32_t> queue_families_;
  std::vector<std::vector<command_pool_t>> command_pools_;

public:
  DeviceManager(
      vk::PhysicalDevice, 
      const vk::PhysicalDeviceFeatures&, 
      vk::SurfaceKHR); 
  ~DeviceManager();

  const vk::PhysicalDevice& physicalDevice() const;
  const vk::SurfaceKHR& surface() const;
  const vk::Device& device() const; 
  const vma::Allocator& allocator() const;

  std::tuple<vk::Queue, uint32_t> getGraphicsQueue();
  std::tuple<vk::Queue, uint32_t> getComputeQueue();
  std::tuple<vk::Queue, uint32_t> getPresentQueue();

  vk::CommandPool getCommandPool(
      uint32_t queueFamilyIndex, 
      vk::CommandPoolCreateFlags flags = vk::CommandPoolCreateFlags());
  
};


#endif
