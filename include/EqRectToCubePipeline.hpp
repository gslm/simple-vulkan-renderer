#ifndef EQRECT_TO_CUBE_PIPELINE_HPP
#define EQRECT_TO_CUBE_PIPELINE_HPP

#include <vulkan/vulkan.hpp>

class EqRectToCubePipeline {
  vk::PipelineLayout pipeline_layout_;
  vk::Pipeline pipeline_;

public:
  static constexpr uint32_t local_size_x = 1 << 4;
  static constexpr uint32_t local_size_y = 1 << 4;

  EqRectToCubePipeline();
  EqRectToCubePipeline(
      vk::Device device,
      vk::DescriptorSetLayout descriptor_set0_layout,
      vk::DescriptorSetLayout descriptor_set1_layout);

  operator vk::Pipeline() const;

  vk::PipelineLayout layout() const;

};

#endif //EQRECT_TO_CUBE_PIPELINE_HPP
