#ifndef TEXTURE_LOADER_HPP
#define TEXTURE_LOADER_HPP

#include <vulkan/vulkan.hpp>
#include <vma/Allocator.hpp>
#include <DeviceManager.hpp>
#include <string>
#include <array>

namespace TextureLoader_detail {
   struct StagingResources {
    static constexpr size_t StagingBufferSize = 3*(1 << 11); // Multiple of 1,2,3,4,6,8,12,16; ~6K. 

    vk::Buffer buffer;
    vma::Allocation allocation;
    vk::Fence fence;
    vk::CommandBuffer cmd;
  };

  struct MipLevelsComputationResources {
    vk::CommandBuffer cmd;
    vk::Fence fence;
  };

}

class TextureLoader {
  using StagingResources = TextureLoader_detail::StagingResources;
  //using MipLevelsComputationResources =
  //    TextureLoader_detail::MipLevelsComputationResources;

public:
  struct Texture {
    vk::Image image;
    vk::Extent2D extent;
    uint32_t mip_levels;
    vk::Format format;
    vma::Allocation allocation;
  };

  enum class LoadFlags : uint32_t {
    AllocateMipLevels = 0,
    GenerateMipLevels = 1 << 1,
    Hdr = 1 << 2
  };

private:
  vk::Device device_;
  vma::Allocator alloc_;
  vk::Queue queue_;
  //vk::CommandPool command_pool_;

  std::array<StagingResources, 2> staging_resources_;

public:
  TextureLoader(DeviceManager& device_manger);
  ~TextureLoader();

  Texture load(const std::string& file_path, LoadFlags flags);

};

inline TextureLoader::LoadFlags operator|(
    const TextureLoader::LoadFlags& lhs, 
    const TextureLoader::LoadFlags& rhs)
{
  return static_cast<TextureLoader::LoadFlags>(
      static_cast<uint32_t>(lhs) | static_cast<uint32_t>(rhs));
}

inline TextureLoader::LoadFlags operator&(
    const TextureLoader::LoadFlags& lhs, 
    const TextureLoader::LoadFlags& rhs)
{
  return static_cast<TextureLoader::LoadFlags>(
      static_cast<uint32_t>(lhs) & static_cast<uint32_t>(rhs));
}

#endif //TEXTURE_LOADER_HPP
