#ifndef VMA__ALLOCATOR_HPP
#define VMA__ALLOCATOR_HPP

#include <vulkan/vulkan.hpp>
#include <vma/vk_mem_alloc.h> 

namespace vma {

template<typename BitType, typename MaskType>
using Flags = vk::Flags<BitType, MaskType>;

template <typename FlagBitsType> struct FlagTraits
{
  enum { allFlags = 0 };
};

enum class AllocationCreateFlagBits
{
    eDedicatedMemory = VMA_ALLOCATION_CREATE_DEDICATED_MEMORY_BIT,
    eNeverAllocate = VMA_ALLOCATION_CREATE_NEVER_ALLOCATE_BIT,
    eMapped = VMA_ALLOCATION_CREATE_MAPPED_BIT,	
    eCanBecomeLost = VMA_ALLOCATION_CREATE_CAN_BECOME_LOST_BIT,	
    eCanMakeOtherLost = VMA_ALLOCATION_CREATE_CAN_MAKE_OTHER_LOST_BIT,
    eUserDataCopyString = VMA_ALLOCATION_CREATE_USER_DATA_COPY_STRING_BIT, 	
    eUpperAddress = VMA_ALLOCATION_CREATE_UPPER_ADDRESS_BIT, 	
    eStrategyBestFit = VMA_ALLOCATION_CREATE_STRATEGY_BEST_FIT_BIT ,	
    eStrategyWorstFit = VMA_ALLOCATION_CREATE_STRATEGY_WORST_FIT_BIT,
    eStrategyFirtsFit = VMA_ALLOCATION_CREATE_STRATEGY_FIRST_FIT_BIT, 	
    eStrategyMinMemory = VMA_ALLOCATION_CREATE_STRATEGY_MIN_MEMORY_BIT, 	
    eStrategyMinTime = VMA_ALLOCATION_CREATE_STRATEGY_MIN_TIME_BIT,
    eStrategyMinFragmentation = VMA_ALLOCATION_CREATE_STRATEGY_MIN_FRAGMENTATION_BIT
};

using AllocationCreateFlags = Flags<AllocationCreateFlagBits, VmaAllocationCreateFlags>;

VULKAN_HPP_INLINE AllocationCreateFlags operator|( AllocationCreateFlagBits bit0, AllocationCreateFlagBits bit1 )
{
  return AllocationCreateFlags( bit0 ) | bit1;
}

VULKAN_HPP_INLINE AllocationCreateFlags operator~( AllocationCreateFlagBits bits )
{
  return ~( AllocationCreateFlags( bits ) );
}

template <> struct FlagTraits<AllocationCreateFlagBits>
{
    enum
    {
      allFlags = VkFlags(AllocationCreateFlagBits::eDedicatedMemory) 
             | VkFlags(AllocationCreateFlagBits::eNeverAllocate)  
             | VkFlags(AllocationCreateFlagBits::eCanBecomeLost)
             | VkFlags(AllocationCreateFlagBits::eCanMakeOtherLost)
             | VkFlags(AllocationCreateFlagBits::eUserDataCopyString)
             | VkFlags(AllocationCreateFlagBits::eUpperAddress)
             | VkFlags(AllocationCreateFlagBits::eStrategyBestFit)
             | VkFlags(AllocationCreateFlagBits::eStrategyWorstFit)
             | VkFlags(AllocationCreateFlagBits::eStrategyFirtsFit)
             | VkFlags(AllocationCreateFlagBits::eStrategyMinMemory)
             | VkFlags(AllocationCreateFlagBits::eStrategyMinTime)
             | VkFlags(AllocationCreateFlagBits::eStrategyMinFragmentation),

      strategyFlags = VkFlags(AllocationCreateFlagBits::eStrategyBestFit)
             | VkFlags(AllocationCreateFlagBits::eStrategyWorstFit)
             | VkFlags(AllocationCreateFlagBits::eStrategyFirtsFit)
    };
};

enum class AllocatorCreateFlagBits
{
  eExternalySynchronized = VMA_ALLOCATOR_CREATE_EXTERNALLY_SYNCHRONIZED_BIT, 	
  eKHRDedicatedAllocation = VMA_ALLOCATOR_CREATE_KHR_DEDICATED_ALLOCATION_BIT
};

using AllocatorCreateFlags = Flags<AllocatorCreateFlagBits,  VmaAllocatorCreateFlags>;

VULKAN_HPP_INLINE AllocatorCreateFlags operator|( AllocatorCreateFlagBits bit0, AllocatorCreateFlagBits bit1 )
{
  return AllocatorCreateFlags( bit0 ) | bit1;
}

VULKAN_HPP_INLINE AllocatorCreateFlags operator~( AllocatorCreateFlagBits bits )
{
  return ~( AllocatorCreateFlags( bits ) );
}

template <> struct FlagTraits<AllocatorCreateFlagBits>
{
    enum
    {
      allFlags = VkFlags(AllocatorCreateFlagBits::eExternalySynchronized) 
             | VkFlags(AllocatorCreateFlagBits::eKHRDedicatedAllocation)  
    };
};

enum class PoolCreateFlagBits {
    eIgnoreBufferImageGranularity = VMA_POOL_CREATE_IGNORE_BUFFER_IMAGE_GRANULARITY_BIT,
    eLinearAlgorithm = VMA_POOL_CREATE_LINEAR_ALGORITHM_BIT,
    eBuddyAlgorithm = VMA_POOL_CREATE_BUDDY_ALGORITHM_BIT
};

using PoolCreateFlags = Flags<PoolCreateFlagBits,  VmaPoolCreateFlags>;

VULKAN_HPP_INLINE PoolCreateFlags operator|( PoolCreateFlagBits bit0, PoolCreateFlagBits bit1 )
{
  return PoolCreateFlags( bit0 ) | bit1;
}

VULKAN_HPP_INLINE PoolCreateFlags operator~( PoolCreateFlagBits bits )
{
  return ~( PoolCreateFlags( bits ) );
}

template <> struct FlagTraits<PoolCreateFlagBits>
{
    enum
    {
      allFlags = VkFlags(PoolCreateFlagBits::eIgnoreBufferImageGranularity) 
             | VkFlags(PoolCreateFlagBits::eLinearAlgorithm)
             | VkFlags(PoolCreateFlagBits::eBuddyAlgorithm),
      
      algorithmFlags = VkFlags(PoolCreateFlagBits::eLinearAlgorithm)
             | VkFlags(PoolCreateFlagBits::eBuddyAlgorithm)
    };
};

enum class RecordFlagBits 
{
  eFlushAfterCall = VMA_RECORD_FLUSH_AFTER_CALL_BIT
};

using RecordFlags = Flags<RecordFlagBits,  VmaRecordFlags>;

VULKAN_HPP_INLINE RecordFlags operator|( RecordFlagBits bit0, RecordFlagBits bit1 )
{
  return RecordFlags( bit0 ) | bit1;
}

VULKAN_HPP_INLINE RecordFlags operator~( RecordFlagBits bits )
{
  return ~( RecordFlags( bits ) );
}

template <> struct FlagTraits<RecordFlagBits>
{
    enum
    {
      allFlags = VkFlags(RecordFlagBits::eFlushAfterCall) 
    };
};

enum class DefragmentationFlagBits 
{
};

using DefragmentationFlags = Flags<DefragmentationFlagBits, VmaDefragmentationFlags>;


enum class MemoryUsage {
    eUnknown = VMA_MEMORY_USAGE_UNKNOWN,
    eGpuOnly = VMA_MEMORY_USAGE_GPU_ONLY,
    eCpuOnly = VMA_MEMORY_USAGE_CPU_ONLY,
    eCpuToGpu = VMA_MEMORY_USAGE_CPU_TO_GPU,
    eGpuToCpu = VMA_MEMORY_USAGE_GPU_TO_CPU
};

struct DeviceMemoryCallbacks;
struct VulkanFunctions;
struct RecordSettings;

struct AllocatorCreateInfo
{
  AllocatorCreateInfo( 
          AllocatorCreateFlags flags_ = AllocatorCreateFlags(),
          vk::PhysicalDevice physicalDevice_ = vk::PhysicalDevice(),
          vk::Device device_ = vk::Device(),
          vk::DeviceSize preferredLargeHeapBlockSize_ = 0,
          const vk::AllocationCallbacks* pAllocationCallbacks_ = nullptr,
          const DeviceMemoryCallbacks* pDeviceMemoryCallbacks_ = nullptr,
          uint32_t frameInUseCount_ = 0,
          const vk::DeviceSize* pHeapSizeLimit_ = nullptr,
          const VulkanFunctions* pVulkanFunctions_ = nullptr,
          const RecordSettings* pRecordSettings_ = nullptr)
      : flags(flags_)
      , physicalDevice(physicalDevice_)
      , device(device_)
      , preferredLargeHeapBlockSize(preferredLargeHeapBlockSize_)
      , pAllocationCallbacks(pAllocationCallbacks_)
      , pDeviceMemoryCallbacks(pDeviceMemoryCallbacks_)
      , frameInUseCount(frameInUseCount_)
      , pHeapSizeLimit(pHeapSizeLimit_)
      , pVulkanFunctions(pVulkanFunctions_)
      , pRecordSettings(pRecordSettings_)
  {
  }

  AllocatorCreateInfo(AllocatorCreateInfo const &) = default;

  AllocatorCreateInfo( VmaAllocatorCreateInfo const & rhs )
  {
    *reinterpret_cast<VmaAllocatorCreateInfo*>(this) = rhs;
  }

  AllocatorCreateInfo& operator=( VmaAllocatorCreateInfo const & rhs )
  {
    *reinterpret_cast<VmaAllocatorCreateInfo*>(this) = rhs;
    return *this;
  }
  
  AllocatorCreateInfo& operator=( AllocatorCreateInfo const & ) = default;

  AllocatorCreateInfo& setFlags( AllocatorCreateFlags flags_ )
  {
    flags = flags_;
    return *this;
  }

  AllocatorCreateInfo& setPhysicalDevice( vk::PhysicalDevice physicalDevice_ )
  {
    physicalDevice = physicalDevice_;
    return *this;
  }

  AllocatorCreateInfo& setDevice( vk::Device device_ )
  {
    device = device_;
    return *this;
  }

  AllocatorCreateInfo& setPreferredLargeHeapBlockSize( vk::DeviceSize preferredLargeHeapBlockSize_)
  {
    preferredLargeHeapBlockSize = preferredLargeHeapBlockSize_;
    return *this;
  }

  AllocatorCreateInfo& setAllocationCallbacks( const vk::AllocationCallbacks*  pAllocationCallbacks_)
  {
    pAllocationCallbacks = pAllocationCallbacks_;
    return *this;
  }

  AllocatorCreateInfo& setDeviceMemoryCallbacks( const vma::DeviceMemoryCallbacks*  pDeviceMemoryCallbacks_)
  {
    pDeviceMemoryCallbacks = pDeviceMemoryCallbacks_;
    return *this;
  }   

  AllocatorCreateInfo& setFrameInUseCount( uint32_t frameInUseCount_)
  {
    frameInUseCount = frameInUseCount_;
    return *this;
  }

  AllocatorCreateInfo& setHeapSizeLimit( const vk::DeviceSize*  pHeapSizeLimit_)
  {
    pHeapSizeLimit = pHeapSizeLimit_;
    return *this;
  }

  AllocatorCreateInfo& setVulkanFunctions( const vma::VulkanFunctions*  pVulkanFunctions_)
  {
    pVulkanFunctions = pVulkanFunctions_;
    return *this;
  }

  AllocatorCreateInfo& setRecordSettings( const vma::RecordSettings*  pRecordSettings_)
  {
    pRecordSettings = pRecordSettings_;
    return *this;
  }   

  operator VmaAllocatorCreateInfo const&() const
  {
    return *reinterpret_cast<const VmaAllocatorCreateInfo*>(this);
  }

  operator VmaAllocatorCreateInfo &()
  {
    return *reinterpret_cast<VmaAllocatorCreateInfo*>(this);
  }

  bool operator==( AllocatorCreateInfo const& rhs ) const
  {
    return ( flags == rhs.flags )
        && ( physicalDevice == rhs.physicalDevice )
        && ( device == rhs.device )
        && ( preferredLargeHeapBlockSize == rhs.preferredLargeHeapBlockSize )
        && ( pAllocationCallbacks == rhs.pAllocationCallbacks )
        && ( pDeviceMemoryCallbacks == rhs.pDeviceMemoryCallbacks )
        && ( frameInUseCount == rhs.frameInUseCount )
        && ( pHeapSizeLimit == rhs.pHeapSizeLimit )
        && ( pVulkanFunctions == rhs.pVulkanFunctions )
        && ( pRecordSettings == rhs.pRecordSettings );
  }

  bool operator!=( AllocatorCreateInfo const& rhs ) const
  {
    return !operator==( rhs );
  }

public:
  AllocatorCreateFlags flags;
  vk::PhysicalDevice physicalDevice;
  vk::Device device;
  vk::DeviceSize preferredLargeHeapBlockSize;
  const vk::AllocationCallbacks* pAllocationCallbacks;
  const vma::DeviceMemoryCallbacks* pDeviceMemoryCallbacks;
  uint32_t frameInUseCount;
  const vk::DeviceSize* pHeapSizeLimit;
  const vma::VulkanFunctions* pVulkanFunctions;
  const vma::RecordSettings* pRecordSettings;
};

static_assert( sizeof( AllocatorCreateInfo ) == sizeof( VmaAllocatorCreateInfo ), "struct and wrapper have different size!" );

struct PoolCreateInfo
{

  PoolCreateInfo(uint32_t memoryTypeIndex_ = 0,
        PoolCreateFlags flags_ = PoolCreateFlags(),
        vk::DeviceSize blockSize_ = 0,
        size_t minBlockCount_ = 0,
        size_t maxBlockCount_ = 0,
        uint32_t frameInUseCount_ = 0)
    : memoryTypeIndex(memoryTypeIndex_)
    , flags(flags_)
    , blockSize(blockSize_)
    , minBlockCount(minBlockCount_)
    , maxBlockCount(maxBlockCount_)
    , frameInUseCount(frameInUseCount_)
  {
  }

  PoolCreateInfo( VmaPoolCreateInfo const & rhs )
  {
    *reinterpret_cast<VmaPoolCreateInfo*>(this) = rhs;
  }

  PoolCreateInfo& operator=( VmaPoolCreateInfo const & rhs )
  {
    *reinterpret_cast<VmaPoolCreateInfo*>(this) = rhs;
    return *this;
  }

  PoolCreateInfo& getMemoryTypeIndex(uint32_t memoryTypeIndex_)
  {
      memoryTypeIndex = memoryTypeIndex_;
      return *this;
  }

  PoolCreateInfo& getFlags(PoolCreateFlags flags_)
  {
      flags = flags_;
      return *this;
  }

  PoolCreateInfo& getBlockSize(vk::DeviceSize blockSize_)
  {
      blockSize = blockSize_;
      return *this;
  }

  PoolCreateInfo& getMinBlockCount(size_t minBlockCount_)
  {
      minBlockCount = minBlockCount_;
      return *this;
  }

  PoolCreateInfo& getMaxBlockCount(size_t maxBlockCount_)
  {
      maxBlockCount = maxBlockCount_;
      return *this;
  }

  PoolCreateInfo& getFrameInUseCount(uint32_t frameInUseCount_)
  {
      frameInUseCount = frameInUseCount_;
      return *this;
  }

  operator VmaPoolCreateInfo const&() const
  {
    return *reinterpret_cast<const VmaPoolCreateInfo*>(this);
  }

  operator VmaPoolCreateInfo &()
  {
    return *reinterpret_cast<VmaPoolCreateInfo*>(this);
  }

  bool operator==( PoolCreateInfo const& rhs ) const
  {
    return ( memoryTypeIndex == rhs.memoryTypeIndex )
         && ( flags == rhs.flags )
         && ( blockSize == rhs.blockSize )
         && ( minBlockCount == rhs.minBlockCount )
         && ( maxBlockCount == rhs.maxBlockCount )
         && ( frameInUseCount == rhs.frameInUseCount );
  }

  bool operator!=( PoolCreateInfo const& rhs ) const
  {
    return !operator==( rhs );
  }

public:
    uint32_t memoryTypeIndex;
    PoolCreateFlags flags;
    vk::DeviceSize blockSize;
    size_t minBlockCount;
    size_t maxBlockCount;
    uint32_t frameInUseCount;
};

static_assert( sizeof( PoolCreateInfo ) == sizeof( VmaPoolCreateInfo ), "struct and wrapper have different size!" );

class Pool {
    VmaPool m_pool;

  public:
    VULKAN_HPP_CONSTEXPR Pool()
      : m_pool(VK_NULL_HANDLE)
    {}

    VULKAN_HPP_CONSTEXPR Pool( std::nullptr_t )
      : m_pool(VK_NULL_HANDLE)
    {}

    VULKAN_HPP_TYPESAFE_EXPLICIT Pool( VmaPool pool)
      : m_pool( pool )
    {}

#if defined(VULKAN_HPP_TYPESAFE_CONVERSION)
    Pool & operator=(VmaPool pool)
    {
      m_pool = pool;
      return *this; 
    }
#endif

    Pool & operator=( std::nullptr_t )
    {
      m_pool = VK_NULL_HANDLE;
      return *this;
    }

    bool operator==( Pool const & rhs ) const
    {
      return m_pool == rhs.m_pool;
    }

    bool operator!=(Pool const & rhs ) const
    {
      return m_pool != rhs.m_pool;
    }

    bool operator<(Pool const & rhs ) const
    {
      return m_pool < rhs.m_pool;
    }
};

struct AllocationCreateInfo
{
  AllocationCreateInfo(
        AllocationCreateFlags flags_ = AllocationCreateFlags(),
        MemoryUsage usage_ = MemoryUsage::eUnknown,
        vk::MemoryPropertyFlags requiredFlags_ = vk::MemoryPropertyFlags(),
        vk::MemoryPropertyFlags preferredFlags_ = vk::MemoryPropertyFlags(),
        uint32_t memoryTypeBits_ = 0,
        Pool pool_ = Pool(),
        void* pUserData_ = nullptr)
    : flags(flags_)
    , usage(usage_)
    , requiredFlags(requiredFlags_)
    , preferredFlags(preferredFlags_)
    , memoryTypeBits(memoryTypeBits_)
    , pool(pool_)
    , pUserData(pUserData_)
  {
  }

  AllocationCreateInfo(AllocationCreateInfo const &) = default;

  AllocationCreateInfo( VmaAllocationCreateInfo const & rhs )
  {
    *reinterpret_cast<VmaAllocationCreateInfo*>(this) = rhs;
  }

  AllocationCreateInfo& operator=( VmaAllocationCreateInfo const & rhs )
  {
    *reinterpret_cast<VmaAllocationCreateInfo*>(this) = rhs;
    return *this;
  }
  
  AllocationCreateInfo& operator=( AllocationCreateInfo const & ) = default;

  AllocationCreateInfo& setFlags(AllocationCreateFlags flags_)
  {
    flags = flags_;
    return *this;
  }

  AllocationCreateInfo& setUsage(MemoryUsage usage_)
  {
    usage = usage_;
    return *this;
  }

  AllocationCreateInfo& setRequieredFlags(vk::MemoryPropertyFlags requiredFlags_)
  {
    requiredFlags = requiredFlags_;
    return *this;
  }

  AllocationCreateInfo& setPreferredFlags(vk::MemoryPropertyFlags preferredFlags_)
  {
    preferredFlags = preferredFlags_;
    return *this;
  }

  AllocationCreateInfo& setPool(Pool pool_)
  {
    pool = pool_;
    return *this;
  }

  AllocationCreateInfo& setUserData(void* pUserData_)
  {
    pUserData = pUserData_;
    return *this;
  }

  operator VmaAllocationCreateInfo const&() const
  {
    return *reinterpret_cast<const VmaAllocationCreateInfo*>(this);
  }

  operator VmaAllocationCreateInfo &()
  {
    return *reinterpret_cast<VmaAllocationCreateInfo*>(this);
  }

  bool operator==( AllocationCreateInfo const& rhs ) const
  {
    return ( flags == rhs.flags )
        && ( usage == rhs.usage )
        && ( requiredFlags == rhs.requiredFlags )
        && ( preferredFlags == rhs.preferredFlags )
        && ( memoryTypeBits == rhs.memoryTypeBits )
        && ( pool == rhs.pool )
        && ( pUserData == rhs.pUserData );
  }

  bool operator!=( AllocationCreateInfo const& rhs ) const
  {
    return !operator==( rhs );
  }

public:
  AllocationCreateFlags flags;
  MemoryUsage usage;
  vk::MemoryPropertyFlags requiredFlags;
  vk::MemoryPropertyFlags preferredFlags;
  uint32_t memoryTypeBits;
  Pool pool;
  void* pUserData;
};

static_assert( sizeof( AllocationCreateInfo ) == sizeof( VmaAllocationCreateInfo ), "struct and wrapper have different size!" );

class Allocation {
  VmaAllocation m_allocation;

public:
  VULKAN_HPP_CONSTEXPR Allocation()
    : m_allocation(VK_NULL_HANDLE)
  {}

  VULKAN_HPP_CONSTEXPR Allocation( std::nullptr_t )
    : m_allocation(VK_NULL_HANDLE)
  {}

  VULKAN_HPP_TYPESAFE_EXPLICIT Allocation( VmaAllocation allocation)
    : m_allocation( allocation )
  {}

#if defined(VULKAN_HPP_TYPESAFE_CONVERSION)
  Allocation & operator=(VmaAllocation allocation)
  {
    m_allocation = allocation;
    return *this; 
  }
#endif

  Allocation & operator=( std::nullptr_t )
  {
    m_allocation = VK_NULL_HANDLE;
    return *this;
  }

  bool operator==( Allocation const & rhs ) const
  {
    return m_allocation == rhs.m_allocation;
  }

  bool operator!=(Allocation const & rhs ) const
  {
    return m_allocation != rhs.m_allocation;
  }

  bool operator<(Allocation const & rhs ) const
  {
    return m_allocation < rhs.m_allocation;
  }

  VULKAN_HPP_TYPESAFE_EXPLICIT operator VmaAllocation() const
  {
    return m_allocation;
  }

  explicit operator bool() const
  {
    return m_allocation != VK_NULL_HANDLE;
  }

  bool operator!() const
  {
    return m_allocation == VK_NULL_HANDLE;
  }
};


struct DefragmentationInfo2
{
  DefragmentationInfo2(
        DefragmentationFlags flags_ = DefragmentationFlags(),
        uint32_t allocationCount_ = 0,
        Allocation* pAllocations_ = nullptr,
        vk::Bool32* pAllocationsChanged_ = nullptr,
        uint32_t poolCount_ = 0,
        Pool* pPools_ = nullptr,
        vk::DeviceSize maxCpuBytesToMove_ = 0,
        uint32_t maxCpuAllocationsToMove_ = 0,
        vk::DeviceSize maxGpuBytesToMove_ = 0,
        uint32_t maxGpuAllocationsToMove_ = 0,
        vk::CommandBuffer commandBuffer_ = vk::CommandBuffer())
    : flags(flags_)
    , allocationCount(allocationCount_)
    , pAllocations(pAllocations_)
    , pAllocationsChanged(pAllocationsChanged_)
    , poolCount(poolCount_)
    , pPools(pPools_)
    , maxCpuBytesToMove(maxCpuBytesToMove_)
    , maxCpuAllocationsToMove(maxCpuAllocationsToMove_)
    , maxGpuBytesToMove(maxGpuBytesToMove_)
    , maxGpuAllocationsToMove(maxGpuAllocationsToMove_)
    , commandBuffer(commandBuffer_)
  {}

  DefragmentationInfo2& getFlags(DefragmentationFlags flags_)
  {
      flags = flags_;
      return *this;
  }

  DefragmentationInfo2& getAllocationCount(uint32_t allocationCount_)
  {
      allocationCount = allocationCount_;
      return *this;
  }

  DefragmentationInfo2& getAllocations(Allocation* pAllocations_)
  {
      pAllocations = pAllocations_;
      return *this;
  }

  DefragmentationInfo2& getAllocationsChanged(vk::Bool32* pAllocationsChanged_)
  {
      pAllocationsChanged = pAllocationsChanged_;
      return *this;
  }

  DefragmentationInfo2& getPoolCount(uint32_t poolCount_)
  {
      poolCount = poolCount_;
      return *this;
  }

  DefragmentationInfo2& getPools(Pool* pPools_)
  {
      pPools = pPools_;
      return *this;
  }

  DefragmentationInfo2& getMaxCpuBytesToMove(vk::DeviceSize maxCpuBytesToMove_)
  {
      maxCpuBytesToMove = maxCpuBytesToMove_;
      return *this;
  }

  DefragmentationInfo2& getMaxCpuAllocationsToMove(uint32_t maxCpuAllocationsToMove_)
  {
      maxCpuAllocationsToMove = maxCpuAllocationsToMove_;
      return *this;
  }

  DefragmentationInfo2& getMaxGpuBytesToMove(vk::DeviceSize maxGpuBytesToMove_)
  {
    maxGpuBytesToMove = maxGpuBytesToMove_;
    return *this;
  }

  DefragmentationInfo2& getMaxGpuAllocationsToMove(uint32_t maxGpuAllocationsToMove_)
  {
    maxGpuAllocationsToMove = maxGpuAllocationsToMove_;
    return *this;
  }

  DefragmentationInfo2& getCommandBuffer(vk::CommandBuffer commandBuffer_)
  {
    commandBuffer = commandBuffer_;
    return *this;
  }

  operator VmaDefragmentationInfo2 const&() const
  {
    return *reinterpret_cast<const VmaDefragmentationInfo2*>(this);
  }

  operator VmaDefragmentationInfo2 &()
  {
    return *reinterpret_cast<VmaDefragmentationInfo2*>(this);
  }

  bool operator==(const DefragmentationInfo2& rhs) const
  {
    return ( flags == rhs.flags )
       &&( allocationCount == rhs.allocationCount )
       &&( pAllocations == rhs.pAllocations )
       &&( pAllocationsChanged == rhs.pAllocationsChanged )
       &&( poolCount == rhs.poolCount )
       &&( pPools == rhs.pPools )
       &&( maxCpuBytesToMove == rhs.maxCpuBytesToMove )
       &&( maxCpuAllocationsToMove == rhs.maxCpuAllocationsToMove )
       &&( maxGpuBytesToMove == rhs.maxGpuBytesToMove )
       &&( maxGpuAllocationsToMove == rhs.maxGpuAllocationsToMove )
       &&( commandBuffer == rhs.commandBuffer );
  }

  bool operator!=( DefragmentationInfo2 const& rhs ) const
  {
    return !operator==( rhs );
  }

  DefragmentationFlags flags;
  uint32_t allocationCount;
  Allocation* pAllocations;
  vk::Bool32* pAllocationsChanged;
  uint32_t poolCount;
  Pool* pPools;
  vk::DeviceSize maxCpuBytesToMove;
  uint32_t maxCpuAllocationsToMove;
  vk::DeviceSize maxGpuBytesToMove;
  uint32_t maxGpuAllocationsToMove;
  vk::CommandBuffer commandBuffer;
};

static_assert( sizeof( DefragmentationInfo2 ) == sizeof( VmaDefragmentationInfo2 ), "struct and wrapper have different size!" );

class Allocator {
  VmaAllocator m_allocator;

public:
  VULKAN_HPP_CONSTEXPR Allocator()
    : m_allocator(VK_NULL_HANDLE)
  {}

  VULKAN_HPP_CONSTEXPR Allocator( std::nullptr_t )
    : m_allocator(VK_NULL_HANDLE)
  {}

  VULKAN_HPP_TYPESAFE_EXPLICIT Allocator( VmaAllocator allocator)
    : m_allocator( allocator )
  {}

#if defined(VULKAN_HPP_TYPESAFE_CONVERSION)
  Allocator & operator=(VmaAllocator allocator)
  {
    m_allocator = allocator;
    return *this; 
  }
#endif

  Allocator & operator=( std::nullptr_t )
  {
    m_allocator = VK_NULL_HANDLE;
    return *this;
  }

  bool operator==( Allocator const & rhs ) const
  {
    return m_allocator == rhs.m_allocator;
  }

  bool operator!=(Allocator const & rhs ) const
  {
    return m_allocator != rhs.m_allocator;
  }

  bool operator<(Allocator const & rhs ) const
  {
    return m_allocator < rhs.m_allocator;
  }

  VULKAN_HPP_TYPESAFE_EXPLICIT operator VmaAllocator() const
  {
    return m_allocator;
  }

  explicit operator bool() const
  {
    return m_allocator != VK_NULL_HANDLE;
  }

  bool operator!() const
  {
    return m_allocator == VK_NULL_HANDLE;
  }

  void destroy();
/*
  getPhysicalDeviceProperties();
  
  getMemoryProperties();
  
  vk::MemeoryPropertyFlags getMemoryTypeProperties(uint32_t memoryTypeIndex);
  
  void setCurrentFrameIndex(uint32_t frameIndex);
  
  Stats calculateStats();

#if VMA_STATS_STRING_ENABLED

  void buildStatsString(
      char** ppStatsString, 
      vk::Bool32 detailedMap);
  
  void freeStatsString(char* pStatsString);

#endif

  vk::ResultValueType<uint32_t>::type findMemoryTypeIndex(
      uint32_t memoryTypeBist, 
      const AllocationCreateInfo& allocationCreateInfo);
  
  vk::ResultValueType<uint32_t>::type findMemoryTypeIndexBufferInfo(
      const vk::BufferCreateInfo& bufferCreateInfo,
      const AllocatorCreateInfo& allocatorCreateInfo);

  vk::ResultValueType<uint32_t>::type findMemoryTypeIndexImageInfo(
      const vk::ImageCreateInfo& imageCreateInfo,
      const AllocatorCreateInfo& allocatorCreateInfo);

  vk::ResultValueType<Pool>::type createPool(const PoolCreateInfo& createInfo);
  
  void destroyPool(Pool pool);
  
  void destroy(Pool pool);
  
  PoolState getPoolStats(Pool pool);

  size_t makePoolAllocationsLost(Pool pool);
  
  vk::ResultValueType<void>::type checkPoolCorruption(Pool pool);
  
  vk::ResultValueType<std::tuple<Allocation, AllocationInfo>>::type allocateMemory(
      const vk::MemoryRequirements& memoryRequierements,
      const AllocationCreateInfo& createInfo);

  vk::ResultValueType<Allocation>::type allocateMemory(
      const vk::MemoryRequirements& memoryRequierements,
      const AllocationCreateInfo& createInfo);

  vk::ResultValueType<std::tuple<std::vector<Allocation>, std::vector<AllocationInfo>>::type 
  allocateMemoryPages(
      const vk::MemoryRequirements& memoryRequierements,
      const AllocationCreateInfo& createInfo,
      size_t allocationCount);

  vk::ResultValueType<std::vector<Allocation>>::type allocateMemoryPages(
      const vk::MemoryRequirements& memoryRequierements,
      const AllocationCreateInfo& createInfo,
      size_t allocationCount);
  
  vk::ResultValueType<std::tuple<Allocation, AlloactionInfo>>::type allocateMemoryForBuffer(
      vk::Buffer,
      const AllocationCreateInfo& createInfo);

  vk::ResultValueType<Allocation>::type allocateMemoryForBuffer(
      vk::Buffer,
      const AllocationCreateInfo& createInfo); 
  
  vk::ResultValueType<std::tuple<Allocation, AllocationInfo>>::type 
  allocateMemoryForImage(
      vk::Image,
      const AllocationCreateInfo& createInfo);

  vk::ResultValueType<Allocation>::type 
  allocateMemoryForImage(
      vk::Image,
      const AllocationCreateInfo& createInfo);

  vk::ResultValueType<void>::type freeMemory(Allocation allocation);
  
  vk::ResultValueType<std::vector<Allocation>>::type 
  freeMemoryPages(vk::ArrayProxy<Allocation> allocations);

  vk::ResultValueType<void>::type resizeAllocation(
      Allocation allocation,
      vk::DeviceSize newSize);

  AllocationInfo getAllocationInfo(Allocation allocation);
  
  vk::Bool32 touchAllocation(Allocation allocation);
  
  void setAllocationUserData(
      Allocation allocation, 
      void* pUserData);

  vk::ResultValueType<Allocation>::type createLostAllocation();
*/  
  vk::ResultValueType<void*>::type mapMemory(Allocation allocation);

  void unmapMemory(Allocation allocation);

/*
  void flushAllocation(
      Allocation allocation, 
      vk::DeviceSize offset, 
      vk::DeviceSize size); 

  void invalidateAllocation(
      Allocation allocation, 
      vk::DeviceSize offset, 
      vk::DeviceSize size);

  vk::ResultValueType<void>::type checkCorruption(uint32_t memoryTypeBits);

  vk::ResultValueType<std::tuple<DefragmentationStats, DefragmentationContext>>:type 
  defragmentationBegin(const DefragmentationInfo2& info);
  
  vk::ResultValueType<void>::type defragmentationEnd(DefragmentationContext context);

  vk::ResultValueType<DefragmentationStats>::type defragment(
      vk::ProxyArray<vma::Allocation> allocations,
      vk::ProxyArray<vk::Bool32>& allocationsChanged,
      const DefragmentationInfo& defragmentationInfo);

  vk::ResultValueType<void>::type bindBufferMemory(
      Allocation allocation, 
      vk::Buffer buffer);
  
  vk::ResultValueType<void>::type bindImageMemory(
      Allocation allocation, 
      vk::Image);
  
  vk::ResultValueType<std::tuple<vk::Buffer, Allocation, AllocationInfo>>::type 
  createBuffer(
      const vk::BufferCreateInfo& bufferCreateInfo,
      const vma::AllocationCreateInfo& allocationCreateInfo);
*/ 
  vk::ResultValueType<std::tuple<vk::Buffer, Allocation>>::type 
  createBuffer(
      const vk::BufferCreateInfo& bufferCreateInfo,
      const vma::AllocationCreateInfo& allocationCreateInfo);
  
  void destroyBuffer(
      vk::Buffer buffer, 
      Allocation allocation);
  
  void destroy(
      vk::Buffer buffer, 
      Allocation allocation);

  /*
  vk::ResultValueType<std::tuple<vk::Image, Allocation, AllocationInfo>>::type 
  createImage(
      const vk::ImageCreateInfo& ImageCreateInfo,
      const vma::AllocationCreateInfo& allocationCreateInfo) 
  {  
  }
 */
  vk::ResultValueType<std::tuple<vk::Image, Allocation>>::type 
  createImage(
      const vk::ImageCreateInfo& imageCreateInfo,
      const vma::AllocationCreateInfo& allocationCreateInfo); 
  
  void destroyImage(
      vk::Image image, 
      vma::Allocation allocation);
  
  void destroy(
      vk::Image image, 
      vma::Allocation allocation);
};

inline void Allocator::destroy()
{
  vmaDestroyAllocator(m_allocator);	
}

inline vk::ResultValueType<void*>::type Allocator::mapMemory(
    Allocation allocation)
{
  void* ptr;
  vk::Result result = static_cast<vk::Result>(vmaMapMemory(
      m_allocator, allocation, &ptr));
  
  return vk::createResultValue(result, ptr, "vma::Allocator::mapMemory" );
}

inline void Allocator::unmapMemory(
    Allocation allocation)
{
  vmaUnmapMemory(
      m_allocator, allocation); 
}

inline vk::ResultValueType<std::tuple<vk::Image, Allocation>>::type 
Allocator::createImage(
    const vk::ImageCreateInfo& imageCreateInfo,
    const vma::AllocationCreateInfo& allocationCreateInfo)
{
  std::tuple<vk::Image, Allocation> allocation;
  
  vk::Result result = static_cast<vk::Result>(vmaCreateImage(
      m_allocator, 
      reinterpret_cast<const VkImageCreateInfo*>(&imageCreateInfo), 
      reinterpret_cast<const VmaAllocationCreateInfo*>(&allocationCreateInfo), 
      reinterpret_cast<VkImage*>(&std::get<0>(allocation)), 
      reinterpret_cast<VmaAllocation*>(&std::get<1>(allocation)),
      nullptr));

  return vk::createResultValue(result, allocation, "vma::Allocator::createImage" );
}
  
inline void Allocator::destroyImage(
    vk::Image image, 
    vma::Allocation allocation)
{
  vmaDestroyImage(m_allocator,
      static_cast<VkImage>(image), 
      static_cast<VmaAllocation>(allocation));
}
  
inline void Allocator::destroy(
    vk::Image image, 
    vma::Allocation allocation)
{
  this->destroyImage(image, allocation);
}

inline vk::ResultValueType<std::tuple<vk::Buffer, Allocation>>::type 
Allocator::createBuffer(
    const vk::BufferCreateInfo& bufferCreateInfo,
    const vma::AllocationCreateInfo& allocationCreateInfo)
{
  std::tuple<vk::Buffer, Allocation> allocation;
  
  vk::Result result = static_cast<vk::Result>(vmaCreateBuffer(
      m_allocator, 
      reinterpret_cast<const VkBufferCreateInfo*>(&bufferCreateInfo), 
      reinterpret_cast<const VmaAllocationCreateInfo*>(&allocationCreateInfo), 
      reinterpret_cast<VkBuffer*>(&std::get<0>(allocation)), 
      reinterpret_cast<VmaAllocation*>(&std::get<1>(allocation)),
      nullptr));

  return vk::createResultValue(result, allocation, "vma::Allocator::createBuffer" );
}
  
inline void Allocator::destroyBuffer(
    vk::Buffer buffer, 
    vma::Allocation allocation)
{
  vmaDestroyBuffer(m_allocator,
      static_cast<VkBuffer>(buffer), 
      static_cast<VmaAllocation>(allocation));
}
  
inline void Allocator::destroy(
    vk::Buffer buffer, 
    vma::Allocation allocation)
{
  this->destroyBuffer(buffer, allocation);
}


class DefragmentationContext {
    VmaDefragmentationContext m_defragmentationContext;

  public:
    VULKAN_HPP_CONSTEXPR DefragmentationContext()
      : m_defragmentationContext(VK_NULL_HANDLE)
    {}

    VULKAN_HPP_CONSTEXPR DefragmentationContext( std::nullptr_t )
      : m_defragmentationContext(VK_NULL_HANDLE)
    {}

    VULKAN_HPP_TYPESAFE_EXPLICIT DefragmentationContext( VmaDefragmentationContext defragmentationContext)
      : m_defragmentationContext( defragmentationContext )
    {}

#if defined(VULKAN_HPP_TYPESAFE_CONVERSION)
    DefragmentationContext & operator=(VmaDefragmentationContext defragmentationContext)
    {
      m_defragmentationContext = defragmentationContext;
      return *this;
    }
#endif

    DefragmentationContext & operator=( std::nullptr_t )
    {
      m_defragmentationContext = VK_NULL_HANDLE;
      return *this;
    }

    bool operator==( DefragmentationContext const & rhs ) const
    {
      return m_defragmentationContext == rhs.m_defragmentationContext;
    }

    bool operator!=(DefragmentationContext const & rhs ) const
    {
      return m_defragmentationContext != rhs.m_defragmentationContext;
    }

    bool operator<(DefragmentationContext const & rhs ) const
    {
      return m_defragmentationContext < rhs.m_defragmentationContext;
    }
};

inline Allocator createAllocator(const AllocatorCreateInfo& createInfo) 
{
    VmaAllocator allocator;
    vmaCreateAllocator(
        reinterpret_cast<const VmaAllocatorCreateInfo*>(&createInfo), 
        &allocator);
    
    return Allocator(allocator);
}

}

#endif //VMA__ALLOCATOR_HPP
