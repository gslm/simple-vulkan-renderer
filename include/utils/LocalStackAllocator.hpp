#ifndef UT__LOCAL_STACK_ALLOCATOR_HPP
#define UT__LOCAL_STACK_ALLOCATOR_HPP

#include <array>
#include <memory>

namespace ut {


template<class T, size_t N, class FallbackAlloc = std::allocator<T>>
struct LocalStackAllocator {

  struct Interface;

  //std::array<T, N> mem;
  //T* end = mem.data();
  //FallbackAlloc alloc;

  T* allocate(size_t n) 
  {
    if (end + n >= mem.end()) return alloc.allocate(n);
    
    auto ptr = end;
    end += n;

    return ptr;
  }

  void deallocate(T* ptr, size_t n)
  {
    if (ptr < mem.begin() or ptr >= mem.end()) {
      alloc.deallocate(ptr, n);
    }
  }
 
  Interface interface() 
  {
    return Interface(this);
  }

};

template<class T, size_t N, class FbAlloc>
struct LocalStackAllocator<T, N, FbAlloc>::Interface {
  
  friend class LocalStackAllocator<T, N, FbAlloc>;

  using value_type = T;
  using size_type = std::size_t;
  using difference_type = std::ptrdiff_t;
  
  using void_pointer = value_type*;
  using const_void_pointer = value_type const*;

  using is_always_equal = std::false_type;

//private:  

  //LocalStackAllocator<T, N, FbAlloc> *const ref_ = nullptr;

  Interface(LocalStackAllocator<T, N, FbAlloc> *const ref = nullptr) 
    : ref_(ref) 
  {
  }

public:
  value_type* allocate(size_t n) 
  {
    return ref_->allocate(n);
  }

  void deallocate(value_type* ptr, size_t n)
  {
    return ref_->deallocate(ptr, n);
  }

};

} //namespace ut;

#endif //UT__LOCAL_STACK_ALLOCATOR_HPP
