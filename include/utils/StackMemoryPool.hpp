#ifndef UT__LOCAL_STACK_MEMORY_POOL_HPP
#define UT__LOCAL_STACK_MEMORY_POOL_HPP

#include <>
#include <array>

template<class T, size_t Size>
class LocalStackMemoryPool {
  std::array<T, Size> memory_;
  T* end_;

  template<class FbAlloc>
  struct Allocator {

    LocalStackMemoryPool* const pool_;
    FbAlloc fb_alloc;

    Allocator() {}

    Allocator(LocalStackMemoryPool* const pool) 
      : pool_(pool) {}

    T* allocate(size_t n) 
    {
      if (pool_->memory_.end() <= pool_->end_ + n) {
        return fbAlloc.allocate(n);
      }
      
      T* ptr = pool_->end_;
      pool_->end += n;

      return ptr;
    }

    void deallocate(T* ptr, size_t n)
    {
      if (pool_->memory_.end() <= ptr and pool_->memory_.begin() > ptr) {
        fb_alloc.deallocate(ptr, n);
      }
    }
  };

  template<class FbAlloc = std::allocator<T>>
  Allocator allocator(FbAlloc& fb_alloc = FbAlloc()) {
    return Allocator(this, fb_alloc);
  }
};

#endif

