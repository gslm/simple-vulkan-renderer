#ifndef UT__PRIVATE_IMPL_HPP
#define UT__PRIVATE_IMPL_HPP

#include <new>
#include <type_traits>
#include <memory>

namespace ut {

template<class T>
class PrivateImpl {
    
  std::aligned_storage_t<
    sizeof(std::unique_ptr<T>), 
    alignof(std::unique_ptr<T>)
  > storage_;

public:

  PrivateImpl() {
    if constexpr (sizeof(storage_) >= sizeof(T)) {
      new(&storage_) T();
    }
    else {
      new(&storage_) std::unique_ptr<T>(new T());
    }
  }

  ~PrivateImpl() { 
    if constexpr (sizeof(storage_) >= sizeof(T)) {
      const auto ptr = reinterpret_cast<T*>(&storage_);
      std::launder(ptr)->~T();
    }
    else {
      const auto ptr = reinterpret_cast<std::unique_ptr<T>*>(&storage_);
      std::launder(ptr)->~unique_ptr();
    }
  }

  PrivateImpl(PrivateImpl& o) {
    if constexpr (sizeof(storage_) >= sizeof(T)) {
      new(&storage_) T(o.get());
    }
    else {
      new(&storage_) std::unique_ptr<T>(new T(o.get()));
    }
  }

  PrivateImpl(PrivateImpl&& o) {
    storage_ = o.storage_;
    
    if constexpr (!(sizeof(storage_) >= sizeof(T))) { 
      //Set storage so the pointee destructor 
      //is not called on destruction.
      new(&o.storage_) T*(); 
    }
  };

  T& get() { 
    if constexpr (sizeof(storage_) >= sizeof(T)) {
      auto ptr = reinterpret_cast<T*>(&storage_);
      return *std::launder(ptr);
    } 
    else {
      auto ptr = reinterpret_cast<std::unique_ptr<T>*>(&storage_); 
      return **std::launder(ptr);
    }
  }

  const T& get() const { 
    if constexpr (sizeof(storage_) >= sizeof(T)) {
      auto ptr = reinterpret_cast<const T*>(&storage_);
      return *std::launder(ptr);
    } 
    else {
      auto ptr = reinterpret_cast<std::unique_ptr<const T>*>(&storage_);
      return **std::launder(ptr);
    }
  }

};

}
#endif
