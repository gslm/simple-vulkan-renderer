#ifndef UT__LOCAL_STACK_MEMORY_POOL_HPP
#define UT__LOCAL_STACK_MEMORY_POOL_HPP

#include <memory>
#include <array>
#include <iostream>

namespace ut {

template<class T, size_t Size>
class LocalStackMemoryPool {
  std::array<T, Size> memory_;
  T* end_ = memory_.begin();

public:
  template<class U = T, class FbAlloc = std::allocator<T>>
  struct Allocator {
    
    using value_type = U; 

    static_assert(
        std::is_same_v<value_type, 
                       typename std::allocator_traits<FbAlloc>::value_type>);


    Allocator() {}

    Allocator(LocalStackMemoryPool* const pool, const FbAlloc& fb_alloc) 
      : pool_(pool)
      , fb_alloc_(fb_alloc)
    {}

    Allocator(const Allocator& other) 
      : pool_(other.pool_)
      , fb_alloc_(other.fb_alloc_)
    {}

    value_type* allocate(size_t n) 
    {
      if (pool_->memory_.end() < pool_->end_ + n) {
        return fb_alloc_.allocate(n);
      }
      
      T* ptr = pool_->end_;
      pool_->end_ += n;

      return ptr;
    }

    void deallocate(value_type* ptr, size_t n)
    {
      if (pool_->memory_.end() <= ptr or pool_->memory_.begin() > ptr) {
        fb_alloc_.deallocate(ptr, n);
      }
    }

    private:
      LocalStackMemoryPool* const pool_ = nullptr;
      FbAlloc fb_alloc_ = FbAlloc();
  };

  template<class FbAlloc = std::allocator<T>>
  Allocator<T, FbAlloc> allocator(const FbAlloc& fb_alloc = FbAlloc()) 
  {
    return Allocator<T, FbAlloc>(this, fb_alloc);
  }

  void free_memory() 
  {
    end_ = memory_.begin();
  }
};

} //namespace ut
#endif

