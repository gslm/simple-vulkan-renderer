#ifndef ENVIROMENT_PIPELINE_HPP
#define ENVIROMENT_PIPELINE_HPP

#include <vulkan/vulkan.hpp>

class EnvPipeline {

  vk::PipelineLayout pipeline_layout_;
  vk::Pipeline pipeline_;

public:
  EnvPipeline();
  EnvPipeline(
      vk::Device device,
      vk::PipelineCache pipeline_cache,
      vk::DescriptorSetLayout envmap_descriptor_set_layout,
      vk::DescriptorSetLayout view_descriptor_set_layout,
      vk::Extent2D render_extent,
      vk::RenderPass render_pass);

  operator vk::Pipeline() const;

  vk::PipelineLayout layout() const;
};

#endif //ENVIROMENT_PIPELINE_HPP
