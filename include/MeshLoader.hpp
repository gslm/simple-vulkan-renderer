#ifndef MESH_LOADER_HPP
#define MESH_LOADER_HPP

#include <utils/PrivateImpl.hpp>

#include <DeviceManager.hpp>

#include <vulkan/vulkan.hpp>
#include <vma/Allocator.hpp>

#include <glm_user_flags.hpp>
#include <glm/glm.hpp>

#include <array>
#include <string>

namespace MeshLoader_detail {
   struct StagingResources {
    static constexpr size_t Log2StagingBufferSize = 12; //4K bytes;

    vk::Buffer buffer;
    vma::Allocation allocation;
    vk::Fence fence;
    vk::CommandBuffer cmd;
  };  
}

class MeshLoader {
  using StagingResources = MeshLoader_detail::StagingResources;

public:
  struct Mesh {
    using vertex_type = glm::vec3;
    using normal_type = glm::vec3;
    using triangle_type = std::array<uint32_t, 3>;

    static constexpr vk::IndexType IndexType = vk::IndexType::eUint32;
    
    vk::Buffer vertices_buffer;
    vk::Buffer normals_buffer;
    vk::Buffer triangles_buffer;
  
    vma::Allocation vertices_alloc;
    vma::Allocation normals_alloc;
    vma::Allocation triangles_alloc;

    size_t vertices_size;
    size_t triangles_size;

    std::array<vertex_type, 2> bbox;
  };

private:
  struct private_data_t;
  ut::PrivateImpl<private_data_t> private_data_;

  vk::Device device_;
  vma::Allocator alloc_;
  vk::Queue queue_;

  std::array<StagingResources, 2> staging_resources_; 

public:
  MeshLoader(DeviceManager& device_manager);
  ~MeshLoader();

  Mesh load(const std::string& file_path);
};

#endif


