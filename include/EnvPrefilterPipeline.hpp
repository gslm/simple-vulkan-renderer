#ifndef ENV_PREFILTER_PIPELINE_HPP
#define ENV_PREFILTER_PIPELINE_HPP

#include <vulkan/vulkan.hpp>

class EnvPrefilterPipeline {
  vk::PipelineLayout pipeline_layout_;
  vk::Pipeline pipeline_;

public:
  static constexpr uint32_t local_size_x = 1 << 4;
  static constexpr uint32_t local_size_y = 1 << 4;

  EnvPrefilterPipeline();
  EnvPrefilterPipeline(
      vk::Device device,
      vk::DescriptorSetLayout env_sampler_descriptor_set_layout,
      vk::DescriptorSetLayout env_miplevel_descriptor_set_layout);

  operator vk::Pipeline() const;

  vk::PipelineLayout layout() const;

};

#endif //ENV_PREFILTER_PIPELINE_HPP
