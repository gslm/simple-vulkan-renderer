#ifndef IBL_PBR_PRECOMP_FN_HPP
#define IBL_PBR_PRECOMP_FN_HPP

#include <vulkan/vulkan.hpp>
#include <vma/Allocator.hpp>
#include <DeviceManager.hpp>

#include <array>

#include <EqRectToCubePipeline.hpp>
#include <IrdConvolutionPipeline.hpp>
#include <EnvPrefilterPipeline.hpp>
#include <BrdfIntegrationPipeline.hpp>

namespace IblPbrPrecompFn_detail {
struct IblPbrPrecompData {
  vma::Allocation brdf_allocation;
  vma::Allocation ird_allocation;
  vma::Allocation env_allocation;

  vk::Image brdf_image;
  vk::Image ird_image;
  vk::Image env_image;

  vk::Extent2D brdf_extent;
  uint32_t ird_side_extent;
  uint32_t env_side_extent;
  uint32_t env_miplevel_count;
};

class IblPbrPrecompFn {

  vk::Device device_;

  vk::Queue comp_queue_; 
  vk::Queue graph_queue_;
 
  uint32_t comp_queue_family_;
  uint32_t graph_queue_family_;
  
  vk::CommandBuffer graph_barrier_cmd_;
  vk::CommandBuffer comp_barrier_cmd_;
  vk::Semaphore semaphore_; 
  vk::Fence fence_;

  vk::CommandBuffer cmd_;
  
  vk::DescriptorPool descriptor_pool_;

  vk::DescriptorSetLayout eqrect_sampled_image_descriptor_set_layout_;
  vk::DescriptorSetLayout cube_sampled_descriptor_set_layout_;
  vk::DescriptorSetLayout cube_storage_descriptor_set_layout_; 
  vk::DescriptorSetLayout ird_convolution_descriptor_set_layout_;
  vk::DescriptorSetLayout brdf_integration_descriptor_set_layout_; 
  
  vk::DescriptorSet eqrect_sampled_image_descriptor_set_;
  vk::DescriptorSet cube_sampled_image_descriptor_set_;
  vk::DescriptorSet ird_convolution_descriptor_set_;
  vk::DescriptorSet brdf_integration_descriptor_set_;

  vk::Sampler eqrect_sampler_;
  vk::Sampler cube_sampler_;

  EqRectToCubePipeline eqrect_to_cube_pipeline_;
  IrdConvolutionPipeline ird_convolution_pipeline_;
  EnvPrefilterPipeline env_prefilter_pipeline_;
  BrdfIntegrationPipeline brdf_integration_pipeline_;

public:

  using IblPbrPrecompData = IblPbrPrecompFn_detail::IblPbrPrecompData;  

  IblPbrPrecompFn(
    vk::Device device,
    vk::Queue comp_queue, uint32_t comp_queue_family,
    vk::Queue graph_queue, uint32_t graph_queue_family,
    vk::CommandPool comp_command_pool,
    vk::CommandPool graph_command_pool);

  IblPbrPrecompFn(const IblPbrPrecompFn&) = delete;
  IblPbrPrecompFn& operator=(const IblPbrPrecompFn&) = delete;

  ~IblPbrPrecompFn();
  
  IblPbrPrecompData operator()(
      vk::Image eqrect_image, 
      vk::ImageLayout eqrect_layout,
      vk::ImageSubresourceRange eqrect_subresource,
      vk::Extent2D eqrect_extent,
      vma::Allocator alloc);
};

template<class DeviceManagerT>
IblPbrPrecompFn make_impl(DeviceManagerT& device_manager)
{
    vk::Device device = device_manager.device();
    auto [comp_queue, comp_queue_family] = 
      device_manager.getComputeQueue();
    auto [graph_queue, graph_queue_family] = 
      device_manager.getGraphicsQueue();

    vk::CommandPool comp_command_pool = 
      device_manager.getCommandPool(comp_queue_family,  
        vk::CommandPoolCreateFlagBits::eTransient | 
        vk::CommandPoolCreateFlagBits::eResetCommandBuffer);

    vk::CommandPool graph_command_pool = 
      device_manager.getCommandPool(graph_queue_family,  
        vk::CommandPoolCreateFlagBits::eTransient | 
        vk::CommandPoolCreateFlagBits::eResetCommandBuffer);

    return IblPbrPrecompFn(
        device,
        comp_queue, comp_queue_family,
        graph_queue, graph_queue_family,
        comp_command_pool,
        graph_command_pool);
}

}

template<class DeviceManagerT>
class IblPbrPrecompFn {
  IblPbrPrecompFn_detail::IblPbrPrecompFn impl_;

public:
  using IblPbrPrecompData = IblPbrPrecompFn_detail::IblPbrPrecompData;  

  IblPbrPrecompFn() {}
  IblPbrPrecompFn(DeviceManagerT& device_manager)
    : impl_(IblPbrPrecompFn_detail::make_impl(device_manager))
  {
  }

  IblPbrPrecompData operator()(
      vk::Image eqrect_image, 
      vk::ImageLayout eqrect_layout,
      vk::ImageSubresourceRange eqrect_subresource,
      vk::Extent2D eqrect_extent,
      vma::Allocator alloc) 
  {
    return impl_(
        eqrect_image, 
        eqrect_layout,
        eqrect_subresource,
        eqrect_extent,
        alloc);
  }


};

#endif //IBL_PBR_PRECOMP_FN_HPP

