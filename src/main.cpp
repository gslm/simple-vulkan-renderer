#include <RendererLogic.hpp>
#include <vkwapp/vkwapp.h>

#include <iostream>
#include <cstring>

void print_usage(const char* exec_name) {
  const char* usage_options = R"USAGE_OPTIONS(
        -m mesh_path            : Mesh path. 
        -e enviromen_map_path   : Enviroment Map path. Eqrectangular enviroment map.
        [-w widthxheight]       : Window dimensions, width and height > 0. 
        [-h]                    : Print help and exit.
        [--fullscreen]          : Force fullscreen.
  )USAGE_OPTIONS";

  std::cout << "Usage: " << exec_name << usage_options << std::endl;
}

const char* get_value(const char**& argv, const char** end)
{
  const char* curr_token = *argv + 1;

  if (*curr_token == '\0') {
    ++argv;
    return argv < end ? *argv : nullptr;
  }

  return curr_token;
}

bool parse_dims(const char* dim_str, int& w, int& h)
{
  std::string local_str(dim_str);

  char* x_ptr = std::strchr(local_str.data(), 'x');

  if (!x_ptr) return false;

  *x_ptr = '\0';

  w = std::atoi(local_str.data());
  h = std::atoi(x_ptr + 1);

  return (w > 0) && (h > 0);
}

bool parse_string(const char* str, const char* & value) 
{ 
  value = str;
  
  return true;
}

bool parse_flag(
    const char* flag,     
    std::bitset<2>& requiered_options,
    vkwapp::App::Args& app_args, 
    RendererLogic::Args& logic_args)
{
  bool res = false;

  if (std::strcmp("fullscreen", flag) == 0) {
    app_args.flags.set(0);
    res = true;
  } 

  return res;
}

bool parse_option(
    const char**& argv, 
    const char** end, 
    std::bitset<2>& required_options,
    vkwapp::App::Args& app_args, 
    RendererLogic::Args& logic_args) 
{

  if (**argv == '-') {
    return parse_flag(++*argv, required_options, app_args, logic_args);
  }

  const char option = **argv;
  const char* value_str = get_value(argv, end);
  if (!value_str) return false;

  switch (option) {
    case 'm': required_options.set(0);
              return parse_string(value_str, logic_args.model_path);

    case 'e': required_options.set(1);
              return parse_string(value_str, logic_args.env_path);
    
    case 'w': return parse_dims(value_str, app_args.width, app_args.height);
    
    default : break;
  }

  return false;
}

std::tuple<vkwapp::App::Args, RendererLogic::Args, bool> parse_args(
     int argc, const char** argv)
{
  vkwapp::App::Args app_args { 0, 0, 0, argv[0] };
  RendererLogic::Args logic_args {nullptr, nullptr};
 
  const char** end = argv + argc;
  std::bitset<2> required_options; 
  bool good = true;

  for(++argv; argv != end and good; ++argv) {
    good = *((*argv)++) == '-' 
      && parse_option(argv, end, required_options, app_args, logic_args);
  }

  return { app_args, logic_args, good && required_options.all() };
}

int main(int argc, const char** argv)
{
  auto [app_args, logic_args, good] = parse_args(argc, argv);

  if (!good) {
    print_usage(argv[0]);
    return 0;
  }

  return vkwapp::run_app<RendererLogic>(app_args, logic_args);  
}
