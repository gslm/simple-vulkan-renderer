#include <IblPbrPrecompFn.hpp>
#include <glm_user_flags.hpp>
#include <glm/glm.hpp>
#include <utils/LocalStackMemoryPool.hpp>

namespace {

using IblPbrPrecompData = IblPbrPrecompFn_detail::IblPbrPrecompData; 

class IblPbrPrecompDescriptorSetsUpdate {
  std::vector<vk::WriteDescriptorSet> write_descriptor_sets_;

public:
  IblPbrPrecompDescriptorSetsUpdate(size_t size) 
  {
    write_descriptor_sets_.reserve(size);  
  }

  void setEqrectSampledImage(
      vk::DescriptorSet desc_set, 
      const vk::DescriptorImageInfo& img_info)
  {
    write_descriptor_sets_.push_back(    
        vk::WriteDescriptorSet()
          .setDstBinding(0)
          .setDstSet(desc_set)
          .setDescriptorCount(1)
          .setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
          .setPImageInfo(&img_info) );
  }
  
  void setCubeSampledImage(
      vk::DescriptorSet desc_set, 
      const vk::DescriptorImageInfo& img_info)
  {
    write_descriptor_sets_.push_back(    
        vk::WriteDescriptorSet()
          .setDstBinding(0)
          .setDstSet(desc_set)
          .setDescriptorCount(1)
          .setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
          .setPImageInfo(&img_info) );
  }

  void setIrdConvolution(
      vk::DescriptorSet desc_set, 
      const vk::DescriptorImageInfo& img_info_0,
      const vk::DescriptorImageInfo& img_info_1) 
  {
    write_descriptor_sets_.push_back(
      vk::WriteDescriptorSet()
        .setDstBinding(0)
        .setDstSet(desc_set)
        .setDescriptorCount(1)
        .setDescriptorType(vk::DescriptorType::eStorageImage)
        .setPImageInfo(&img_info_0));

    write_descriptor_sets_.push_back(
      vk::WriteDescriptorSet()
        .setDstBinding(1)
        .setDstSet(desc_set)
        .setDescriptorCount(1)
        .setDescriptorType(vk::DescriptorType::eStorageImage)
        .setPImageInfo(&img_info_1));
  } 
    
  void setBrdfIntegration(
      vk::DescriptorSet desc_set, 
      const vk::DescriptorImageInfo& img_info)
  {
    write_descriptor_sets_.push_back(
      vk::WriteDescriptorSet()
        .setDstBinding(0)
        .setDstSet(desc_set)
        .setDescriptorCount(1)
        .setDescriptorType(vk::DescriptorType::eStorageImage)
        .setPImageInfo(&img_info) ); 
  }

  void setCubeMiplevelStorage(
      const std::vector<vk::DescriptorSet>& desc_sets, 
      const std::vector<vk::DescriptorImageInfo>& img_infos)
  {
    for (size_t i = 0; i < desc_sets.size(); ++i) {
      write_descriptor_sets_.push_back(
        vk::WriteDescriptorSet()
          .setDstBinding(0)
          .setDstSet(desc_sets[i])
          .setDescriptorCount(1)
          .setDescriptorType(vk::DescriptorType::eStorageImage)
          .setPImageInfo(&img_infos[i]) );
    }
  }

  void setEnvPrefilterStorage(
      const std::vector<vk::DescriptorSet>& desc_sets, 
      const std::vector<vk::DescriptorImageInfo>& img_infos)
  {
    for (size_t i = 0; i < desc_sets.size(); ++i) {
      write_descriptor_sets_.push_back(
        vk::WriteDescriptorSet()
          .setDstBinding(0)
          .setDstSet(desc_sets[i])
          .setDescriptorCount(1)
          .setDescriptorType(vk::DescriptorType::eStorageImage)
          .setPImageInfo(&img_infos[i]) );
    }

  }
  
  void update(vk::Device device) 
  {
    device.updateDescriptorSets(write_descriptor_sets_, {});
    write_descriptor_sets_.clear();
  }
};


uint32_t mip_level_count(uint32_t size)
{
  return static_cast<uint32_t>(std::floor(std::log2(size))) + 1; 
}

uint32_t mip_level_size(uint32_t base_size, uint32_t level)
{
  return std::max(1u, base_size >> level);
}

vk::DescriptorPool make_descriptor_pool(vk::Device dev) 
{
  const auto pool_sizes = std::array<vk::DescriptorPoolSize, 2> {
    vk::DescriptorPoolSize { vk::DescriptorType::eCombinedImageSampler, 2 },
    vk::DescriptorPoolSize { vk::DescriptorType::eStorageImage, 4 }
  };  

  const auto descriptor_pool_ci = vk::DescriptorPoolCreateInfo()
    .setMaxSets(4)
    .setPoolSizeCount(pool_sizes.size())
    .setPPoolSizes(pool_sizes.data());

  return dev.createDescriptorPool(descriptor_pool_ci);
}

vk::DescriptorPool make_miplevel_storage_sets_pool(
    vk::Device dev, 
    uint32_t miplevels)
{
  const auto pool_size = vk::DescriptorPoolSize {
    vk::DescriptorType::eStorageImage, miplevels 
  };

  const auto descriptor_pool_ci = vk::DescriptorPoolCreateInfo()
    .setMaxSets(miplevels)
    .setPoolSizeCount(1)
    .setPPoolSizes(&pool_size);

  return dev.createDescriptorPool(descriptor_pool_ci);
}

vk::DescriptorSetLayout make_eqrect_sampled_image_descriptor_set_layout(
    vk::Device dev)
{
  const auto layout_bindings = std::array<vk::DescriptorSetLayoutBinding, 1> {
    vk::DescriptorSetLayoutBinding()
      .setBinding(0)
      .setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
      .setDescriptorCount(1)
      .setStageFlags(vk::ShaderStageFlagBits::eCompute)
  };

  const auto descriptor_set_layout_ci = vk::DescriptorSetLayoutCreateInfo()
    .setBindingCount(layout_bindings.size())
    .setPBindings(layout_bindings.data());

  return dev.createDescriptorSetLayout(descriptor_set_layout_ci);
}

vk::DescriptorSetLayout make_cube_sampled_descriptor_set_layout(
  vk::Device dev)
{
  const auto layout_bindings = std::array<vk::DescriptorSetLayoutBinding, 1> {
    vk::DescriptorSetLayoutBinding()
      .setBinding(0)
      .setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
      .setDescriptorCount(1)
      .setStageFlags(vk::ShaderStageFlagBits::eCompute)
  };

  const auto descriptor_set_layout_ci = vk::DescriptorSetLayoutCreateInfo()
    .setBindingCount(layout_bindings.size())
    .setPBindings(layout_bindings.data());

  return dev.createDescriptorSetLayout(descriptor_set_layout_ci); 
}

vk::DescriptorSetLayout make_ird_convolution_descriptor_set_layout(
    vk::Device dev)
{
  const auto layout_bindings = std::array<vk::DescriptorSetLayoutBinding, 2> {
    vk::DescriptorSetLayoutBinding()
      .setBinding(0)
      .setDescriptorType(vk::DescriptorType::eStorageImage)
      .setDescriptorCount(1)
      .setStageFlags(vk::ShaderStageFlagBits::eCompute)
    ,
    vk::DescriptorSetLayoutBinding()
      .setBinding(1)
      .setDescriptorType(vk::DescriptorType::eStorageImage)
      .setDescriptorCount(1)
      .setStageFlags(vk::ShaderStageFlagBits::eCompute)
  };

  const auto descriptor_set_layout_ci = vk::DescriptorSetLayoutCreateInfo()
    .setBindingCount(layout_bindings.size())
    .setPBindings(layout_bindings.data());

  return dev.createDescriptorSetLayout(descriptor_set_layout_ci);
}

vk::DescriptorSetLayout make_cube_storage_descriptor_set_layout(
    vk::Device dev)
{
  const auto layout_bindings = std::array<vk::DescriptorSetLayoutBinding, 1> {
     vk::DescriptorSetLayoutBinding()
      .setBinding(0)
      .setDescriptorType(vk::DescriptorType::eStorageImage)
      .setDescriptorCount(1)
      .setStageFlags(vk::ShaderStageFlagBits::eCompute)
  };

  const auto descriptor_set_layout_ci = vk::DescriptorSetLayoutCreateInfo()
    .setBindingCount(layout_bindings.size())
    .setPBindings(layout_bindings.data());

  return dev.createDescriptorSetLayout(descriptor_set_layout_ci);
}

vk::DescriptorSetLayout make_brdf_integration_descriptor_set_layout(
    vk::Device dev)
{
  const auto layout_bindings = std::array<vk::DescriptorSetLayoutBinding, 1> {
     vk::DescriptorSetLayoutBinding()
      .setBinding(0)
      .setDescriptorType(vk::DescriptorType::eStorageImage)
      .setDescriptorCount(1)
      .setStageFlags(vk::ShaderStageFlagBits::eCompute)
  };

  const auto descriptor_set_layout_ci = vk::DescriptorSetLayoutCreateInfo()
    .setBindingCount(layout_bindings.size())
    .setPBindings(layout_bindings.data());

  return dev.createDescriptorSetLayout(descriptor_set_layout_ci);
}

vk::DescriptorSet make_descriptor_set(
    const vk::Device dev, 
    const vk::DescriptorPool pool,
    const vk::DescriptorSetLayout descriptor_set_layout)
{
  const auto set_layouts = std::array<vk::DescriptorSetLayout, 1> {
    descriptor_set_layout
  };

  const auto allocation_info = vk::DescriptorSetAllocateInfo()
    .setDescriptorPool(pool)
    .setDescriptorSetCount(set_layouts.size())
    .setPSetLayouts(set_layouts.data());

  ut::LocalStackMemoryPool<vk::DescriptorSet, 1> mem_pool;
  return dev.allocateDescriptorSets(
            allocation_info, 
            mem_pool.allocator(), 
            vk::DispatchLoaderStatic())[0];
}

std::vector<vk::DescriptorSet> allocate_miplevel_storage_descriptor_sets(
    const vk::Device dev,
    const vk::DescriptorPool pool,
    const vk::DescriptorSetLayout descriptor_set_layout,
    const uint32_t mip_level_count)
{
  std::vector<vk::DescriptorSetLayout> set_layouts(
      mip_level_count, 
      descriptor_set_layout);

  const auto allocation_info = vk::DescriptorSetAllocateInfo()
    .setDescriptorPool(pool)
    .setDescriptorSetCount(set_layouts.size())
    .setPSetLayouts(set_layouts.data());

  return dev.allocateDescriptorSets(allocation_info); 
}

vk::Sampler make_eqrect_sampler(vk::Device device) 
{
  const auto sampler_ci = vk::SamplerCreateInfo()
    .setMagFilter(vk::Filter::eLinear)
    .setMinFilter(vk::Filter::eLinear)
    .setMipmapMode(vk::SamplerMipmapMode::eLinear)
    .setAddressModeU(vk::SamplerAddressMode::eRepeat)
    .setAddressModeV(vk::SamplerAddressMode::eRepeat)
    .setAnisotropyEnable(vk::Bool32(false))
    .setCompareEnable(vk::Bool32(false))
    .setMinLod(0.0f)
    .setMaxLod(1000.0f)
    .setMipLodBias(0.0f)
    .setUnnormalizedCoordinates(vk::Bool32(false));

  return device.createSampler(sampler_ci);
}

vk::Sampler make_env_sampler(vk::Device device) 
{
  const auto sampler_ci = vk::SamplerCreateInfo()
    .setMagFilter(vk::Filter::eLinear)
    .setMinFilter(vk::Filter::eLinear)
    .setMipmapMode(vk::SamplerMipmapMode::eLinear)
    .setAddressModeU(vk::SamplerAddressMode::eRepeat)
    .setAddressModeV(vk::SamplerAddressMode::eRepeat)
    .setAnisotropyEnable(vk::Bool32(false))
    .setCompareEnable(vk::Bool32(false))
    .setMinLod(0.0f)
    .setMaxLod(1000.0f)
    .setMipLodBias(0.0f)
    .setUnnormalizedCoordinates(vk::Bool32(false));

  return device.createSampler(sampler_ci);
}

void submit_image_ownership_transfer(
    vk::ArrayProxy<const vk::ImageMemoryBarrier> image_barriers,
    vk::PipelineStageFlags src_stage_flags,
    vk::PipelineStageFlags dst_stage_flags,
    vk::Queue src_queue, vk::Queue dst_queue,
    uint32_t src_queue_family, uint32_t dst_queue_family,
    vk::CommandBuffer src_cmd, vk::CommandBuffer dst_cmd,
    vk::Semaphore semaphore, vk::Fence fence)
{
  const auto cmd_begin_i = vk::CommandBufferBeginInfo()
    .setFlags(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);

  src_cmd.begin(cmd_begin_i);
  src_cmd.pipelineBarrier(
      src_stage_flags, 
      dst_stage_flags, 
      vk::DependencyFlags(),
      {}, {}, image_barriers);
  src_cmd.end();

  const auto src_submit_i = std::array<vk::SubmitInfo, 1> {
    vk::SubmitInfo()
      .setCommandBufferCount(1)
      .setPCommandBuffers(&src_cmd)
      .setSignalSemaphoreCount(1)
      .setPSignalSemaphores(&semaphore)
  };

  const bool ownership_transfer_needed = src_queue_family != dst_queue_family;
  
  const auto src_fence = ownership_transfer_needed ? vk::Fence() : fence; 

  src_queue.submit(src_submit_i, src_fence);

  if (!ownership_transfer_needed) return;

  dst_cmd.begin(cmd_begin_i);
  dst_cmd.pipelineBarrier(
      src_stage_flags, 
      dst_stage_flags, 
      vk::DependencyFlags(),
      {}, {}, image_barriers);
  dst_cmd.end();

  const vk::PipelineStageFlags wait_stage_mask = 
    vk::PipelineStageFlagBits::eTopOfPipe;

  const auto dst_submit_i = std::array<vk::SubmitInfo, 1> {
    vk::SubmitInfo()
      .setWaitSemaphoreCount(1)
      .setPWaitSemaphores(&semaphore)
      .setPWaitDstStageMask(&wait_stage_mask)
      .setCommandBufferCount(1)
      .setPCommandBuffers(&dst_cmd)
  };

  dst_queue.submit(dst_submit_i, fence);
}

void input_image_ownership_transfer(
    vk::Image image, 
    vk::ImageLayout layout,
    vk::ImageSubresourceRange subresource_range,
    vk::Queue src_queue, vk::Queue dst_queue, 
    uint32_t src_queue_family, uint32_t dst_queue_family,
    vk::CommandBuffer src_cmd, vk::CommandBuffer dst_cmd,
    vk::Semaphore semaphore, vk::Fence fence)
{
  
  if (src_queue_family == dst_queue_family 
      && layout == vk::ImageLayout::eShaderReadOnlyOptimal) {
    return;
  } 

  const auto image_ownership_barrier = std::array<vk::ImageMemoryBarrier, 1> {
    vk::ImageMemoryBarrier()
      .setDstAccessMask(vk::AccessFlagBits::eShaderRead)
      .setOldLayout(layout)
      .setNewLayout(vk::ImageLayout::eShaderReadOnlyOptimal)
      .setSrcQueueFamilyIndex(src_queue_family)
      .setDstQueueFamilyIndex(dst_queue_family)
      .setImage(image)
      .setSubresourceRange(subresource_range)
  }; 

  submit_image_ownership_transfer(
      image_ownership_barrier,
      vk::PipelineStageFlagBits::eTopOfPipe,
      vk::PipelineStageFlagBits::eComputeShader,
      src_queue, dst_queue,
      src_queue_family, dst_queue_family,
      src_cmd, dst_cmd,
      semaphore, fence);
};

void output_images_ownership_transfer(
    const IblPbrPrecompData& images,
    vk::Queue src_queue, vk::Queue dst_queue, 
    uint32_t src_queue_family, uint32_t dst_queue_family,
    vk::CommandBuffer src_cmd, vk::CommandBuffer dst_cmd,
    vk::Semaphore semaphore, vk::Fence fence)
{

  const auto image_ownership_barrier_template = vk::ImageMemoryBarrier()
    .setDstAccessMask(vk::AccessFlagBits::eShaderRead)
    .setNewLayout(vk::ImageLayout::eShaderReadOnlyOptimal)
    .setSrcQueueFamilyIndex(src_queue_family)
    .setDstQueueFamilyIndex(dst_queue_family);

  const auto image_ownership_barriers = std::array<vk::ImageMemoryBarrier, 4> {
    vk::ImageMemoryBarrier(image_ownership_barrier_template)
      .setSrcAccessMask(vk::AccessFlagBits::eShaderRead)
      //.setOldLayout(vk::ImageLayout::eShaderReadOnlyOptimal)
      .setOldLayout(vk::ImageLayout::eGeneral)
      .setImage(images.env_image)
      .setSubresourceRange(vk::ImageSubresourceRange {
          vk::ImageAspectFlagBits::eColor, 0, 1, 0, 6 })
    ,
    vk::ImageMemoryBarrier(image_ownership_barrier_template)
      .setSrcAccessMask(vk::AccessFlagBits::eShaderWrite)
      .setOldLayout(vk::ImageLayout::eGeneral)
      .setImage(images.env_image)
      .setSubresourceRange(vk::ImageSubresourceRange {
          vk::ImageAspectFlagBits::eColor, 1, VK_REMAINING_MIP_LEVELS, 0, 6 })
    ,
    vk::ImageMemoryBarrier(image_ownership_barrier_template)
      .setOldLayout(vk::ImageLayout::eGeneral)
      .setImage(images.ird_image)
      .setSubresourceRange(vk::ImageSubresourceRange {
          vk::ImageAspectFlagBits::eColor, 0, VK_REMAINING_MIP_LEVELS, 0, 6 })
    ,
    vk::ImageMemoryBarrier(image_ownership_barrier_template)
      .setOldLayout(vk::ImageLayout::eGeneral)
      .setImage(images.brdf_image)
      .setSubresourceRange(vk::ImageSubresourceRange {
          vk::ImageAspectFlagBits::eColor, 0, VK_REMAINING_MIP_LEVELS, 0, 1 })
 
 };

  submit_image_ownership_transfer(
      image_ownership_barriers,
      vk::PipelineStageFlagBits::eComputeShader,
      vk::PipelineStageFlagBits::eFragmentShader,
      src_queue, dst_queue,
      src_queue_family, dst_queue_family,
      src_cmd, dst_cmd,
      semaphore, fence);
};

std::tuple<vk::Image, vma::Allocation> allocate_image(
    vma::Allocator alloc, 
    vk::Extent2D extent,
    uint32_t array_layers, 
    uint32_t mip_levels,
    vk::Format format)
{

  const auto flags = array_layers == 6 
                   ? vk::ImageCreateFlagBits::eCubeCompatible 
                   : vk::ImageCreateFlags();

  const auto image_ci = vk::ImageCreateInfo()
    .setFlags(flags)
    .setImageType(vk::ImageType::e2D)
    .setFormat(format)
    .setExtent(vk::Extent3D {extent, 1})
    .setMipLevels(mip_levels)
    .setArrayLayers(array_layers)
    .setSamples(vk::SampleCountFlagBits::e1)
    .setTiling(vk::ImageTiling::eOptimal)
    .setUsage(vk::ImageUsageFlagBits::eSampled
              | vk::ImageUsageFlagBits::eStorage)
    .setSharingMode(vk::SharingMode::eExclusive)
    .setInitialLayout(vk::ImageLayout::eUndefined);
  
  const auto allocation_ci = vma::AllocationCreateInfo()
    .setUsage(vma::MemoryUsage::eGpuOnly)
    .setPreferredFlags(vk::MemoryPropertyFlagBits::eDeviceLocal);

  return alloc.createImage(image_ci, allocation_ci);
}

std::tuple<vk::Image, vma::Allocation> allocate_cube_image(
    vma::Allocator alloc, 
    uint32_t cube_side, 
    uint32_t miplevel_count)
{
  return allocate_image(
      alloc, 
      vk::Extent2D(cube_side, cube_side),
      6,
      miplevel_count,
      vk::Format::eR32G32B32A32Sfloat);
}

void allocate_output_images(
    vma::Allocator alloc, 
    IblPbrPrecompData& images)
{
  std::tie(images.env_image, images.env_allocation) = allocate_image(
      alloc, 
      vk::Extent2D(images.env_side_extent, images.env_side_extent),
      6,
      images.env_miplevel_count,
      vk::Format::eR32G32B32A32Sfloat);

  std::tie(images.ird_image, images.ird_allocation) = allocate_image(
      alloc, 
      vk::Extent2D(images.ird_side_extent, images.ird_side_extent), 
      6,
      1,
      vk::Format::eR32G32B32A32Sfloat);

  std::tie(images.brdf_image, images.brdf_allocation) = allocate_image(
      alloc, 
      images.brdf_extent, 
      1,
      1,
      vk::Format::eR32G32Sfloat);
}

void record_layout_transition_to_general_cmd(
    vk::CommandBuffer cmd,
    vk::Image cube_image,
    const IblPbrPrecompData& images)
{
  const auto layout_transition_barrier_template = vk::ImageMemoryBarrier()
      .setSrcAccessMask(vk::AccessFlags())
      .setDstAccessMask(vk::AccessFlagBits::eShaderWrite)
      .setOldLayout(vk::ImageLayout::eUndefined)
      .setNewLayout(vk::ImageLayout::eGeneral)
      .setSrcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
      .setDstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED);

  const auto layout_transition_barriers = 
    std::array<vk::ImageMemoryBarrier, 4> {
      vk::ImageMemoryBarrier(layout_transition_barrier_template)
        .setImage(cube_image)
        .setSubresourceRange(vk::ImageSubresourceRange {
          vk::ImageAspectFlagBits::eColor, 0, VK_REMAINING_MIP_LEVELS, 0, 6 })
      ,
      vk::ImageMemoryBarrier(layout_transition_barrier_template)
        .setImage(images.env_image)
        .setSubresourceRange(vk::ImageSubresourceRange {
          vk::ImageAspectFlagBits::eColor, 0, VK_REMAINING_MIP_LEVELS, 0, 6 })
      ,
      vk::ImageMemoryBarrier(layout_transition_barrier_template)
        .setImage(images.ird_image)
        .setSubresourceRange(vk::ImageSubresourceRange {
          vk::ImageAspectFlagBits::eColor, 0, 1, 0, 6})
      ,
      vk::ImageMemoryBarrier(layout_transition_barrier_template)
        .setImage(images.brdf_image)
        .setSubresourceRange(vk::ImageSubresourceRange {
          vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1}) 
  };

  cmd.pipelineBarrier(
      vk::PipelineStageFlagBits::eTopOfPipe,
      vk::PipelineStageFlagBits::eComputeShader,
      vk::DependencyFlags(),
      {}, {}, layout_transition_barriers);
}

void record_cube_generation_dispatch_cmd(
    vk::CommandBuffer cmd, 
    EqRectToCubePipeline pipeline,
    vk::DescriptorSet eqrect_descriptor_set,
    const std::vector<vk::DescriptorSet>& miplevels_descriptor_sets,
    uint32_t env_cube_side)
{
  cmd.bindPipeline(vk::PipelineBindPoint::eCompute, pipeline);
  cmd.bindDescriptorSets(
      vk::PipelineBindPoint::eCompute, 
      pipeline.layout(),
      0, eqrect_descriptor_set, 
      {});

  for (size_t l = 0; l < miplevels_descriptor_sets.size(); ++l) { 
    cmd.bindDescriptorSets(
      vk::PipelineBindPoint::eCompute, 
      pipeline.layout(),
      1, miplevels_descriptor_sets[l], 
      {});
    
    cmd.pushConstants<float>(
        pipeline.layout(),
        vk::ShaderStageFlagBits::eCompute,
        0, std::array<float, 1>{static_cast<float>(l)});

    const uint32_t level_size = mip_level_size(
        env_cube_side, static_cast<uint32_t>(l));

    const uint32_t dispatch_count_x = 
       level_size / EqRectToCubePipeline::local_size_x;
    const uint32_t dispatch_count_y = 
       level_size / EqRectToCubePipeline::local_size_y;
  
    cmd.dispatch(dispatch_count_x + 1, dispatch_count_y + 1, 6);
  }
}

void record_cube_generation_barrier_cmd(
    vk::CommandBuffer cmd,
    vk::Image cube_image,
    uint32_t base_level, 
    uint32_t level_count)
{
  const auto barriers = std::array<vk::ImageMemoryBarrier, 1> {
    vk::ImageMemoryBarrier()
      .setSrcAccessMask(vk::AccessFlagBits::eShaderWrite)
      .setDstAccessMask(vk::AccessFlagBits::eShaderRead)
      .setOldLayout(vk::ImageLayout::eGeneral)
      //.setNewLayout(vk::ImageLayout::eShaderReadOnlyOptimal)
      .setNewLayout(vk::ImageLayout::eGeneral)
      .setSrcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
      .setDstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
      .setImage(cube_image)
      .setSubresourceRange(vk::ImageSubresourceRange {
          vk::ImageAspectFlagBits::eColor, base_level, level_count, 0, 6 })
  };

  cmd.pipelineBarrier(
      vk::PipelineStageFlagBits::eComputeShader,
      vk::PipelineStageFlagBits::eComputeShader,
      vk::DependencyFlags(),
      {}, {}, barriers);
}

void record_env_prefilter_dispatch_cmd(
    vk::CommandBuffer cmd,
    const EnvPrefilterPipeline& pipeline,
    vk::DescriptorSet cube_sampled_descriptor_set,
    const std::vector<vk::DescriptorSet>& 
      env_prefilter_miplevel_storage_descriptor_sets,
    vk::Image env_image,
    uint32_t env_cube_side)
{
  cmd.bindPipeline(vk::PipelineBindPoint::eCompute, pipeline);
  
  cmd.bindDescriptorSets(
        vk::PipelineBindPoint::eCompute,
        pipeline.layout(),
        0, 
        cube_sampled_descriptor_set,
        {});

  const size_t miplevel_count = 
    env_prefilter_miplevel_storage_descriptor_sets.size();

  const float cube_texel_count = float(6*env_cube_side*env_cube_side); 
  for (size_t i = 0; i < miplevel_count; ++i) {
    cmd.bindDescriptorSets(
        vk::PipelineBindPoint::eCompute,
        pipeline.layout(),
        1,
        env_prefilter_miplevel_storage_descriptor_sets[i],
        {});

    const float miplevel = float(i);
    const float roughness = miplevel/float(miplevel_count);
    
    cmd.pushConstants<float>(
        pipeline.layout(),
        vk::ShaderStageFlagBits::eCompute,
        0, std::array<float, 2>{cube_texel_count, roughness});

    const uint32_t dispatch_count_x =  std::max(1u, env_cube_side >> i)
      / EnvPrefilterPipeline::local_size_x;
    const uint32_t dispatch_count_y =  std::max(1u, env_cube_side >> i)
      / EnvPrefilterPipeline::local_size_y;

    cmd.dispatch(dispatch_count_x + 1, dispatch_count_y + 1, 6);
  }
}

/*
void record_env_prefilter_barrier_cmd(
    vk::CommandBuffer cmd,
    vk::Image env_image,
    uint32_t start_level,
    uint32_t level_count)
{
  const auto barrier = std::array<vk::ImageMemoryBarrier, 1> {
    vk::ImageMemoryBarrier()
      .setSrcAccessMask(vk::AccessFlagBits::eShaderRead)
      .setDstAccessMask(vk::AccessFlagBits::eShaderWrite)
      .setOldLayout(vk::ImageLayout::eGeneral)
      //.setNewLayout(vk::ImageLayout::eShaderReadOnlyOptimal)
      .setNewLayout(vk::ImageLayout::eGeneral)
      .setSrcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
      .setDstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
      .setImage(env_image)
      .setSubresourceRange(vk::ImageSubresourceRange {
          vk::ImageAspectFlagBits::eColor, start_level, level_count, 0, 6 })
  };

  cmd.pipelineBarrier(
      vk::PipelineStageFlagBits::eComputeShader,
      vk::PipelineStageFlagBits::eComputeShader,
      vk::DependencyFlags(),
      {}, {}, barrier);

}
*/

void record_ird_convolution_dispatch_cmd(
    vk::CommandBuffer cmd,
    const IrdConvolutionPipeline& pipeline,
    vk::DescriptorSet descriptor_set,
    uint32_t ird_cube_side)
{
  cmd.bindPipeline(vk::PipelineBindPoint::eCompute, pipeline);
  
  cmd.bindDescriptorSets(
      vk::PipelineBindPoint::eCompute, 
      pipeline.layout(),
      0, 
      descriptor_set, 
      {});

  const uint32_t dispatch_count_x = ird_cube_side;
  const uint32_t dispatch_count_y = ird_cube_side;
  
  cmd.dispatch(dispatch_count_x, dispatch_count_y, 6);
}

void record_brdf_integration_dispatch_cmd(
    vk::CommandBuffer cmd,
    const BrdfIntegrationPipeline& pipeline,
    vk::DescriptorSet descriptor_set,
    vk::Extent2D brdf_integration_extent)
{
  cmd.bindPipeline(vk::PipelineBindPoint::eCompute, pipeline);
  
  cmd.bindDescriptorSets(
      vk::PipelineBindPoint::eCompute, 
      pipeline.layout(),
      0, 
      descriptor_set, 
      {});

  const uint32_t dispatch_count_x = 
    brdf_integration_extent.width / BrdfIntegrationPipeline::local_size_x;
  const uint32_t dispatch_count_y = 
    brdf_integration_extent.height / BrdfIntegrationPipeline::local_size_y;
  
  cmd.dispatch(dispatch_count_x + 1, dispatch_count_y + 1, 1);
}

vk::CommandBuffer make_command_buffer(
    vk::Device device, 
    vk::CommandPool command_pool)
{
   const auto cmd_alloc_ci = vk::CommandBufferAllocateInfo()
    .setCommandPool(command_pool)
    .setLevel(vk::CommandBufferLevel::ePrimary)
    .setCommandBufferCount(1);
  
  ut::LocalStackMemoryPool<vk::CommandBuffer, 1> mem_pool;
  return device.allocateCommandBuffers(
      cmd_alloc_ci, 
      mem_pool.allocator(), 
      vk::DispatchLoaderStatic())[0];
}

vk::Semaphore make_semaphore(vk::Device device)
{
  const auto semaphore_ci = vk::SemaphoreCreateInfo();

  return device.createSemaphore(semaphore_ci);
}

vk::Fence make_fence(vk::Device device)
{
  const auto fence_ci = vk::FenceCreateInfo()
    .setFlags(vk::FenceCreateFlagBits::eSignaled);

  return device.createFence(fence_ci);
}

vk::ImageView make_eqrect_view(
    vk::Device device, 
    vk::Image eqrect_image, 
    vk::ImageSubresourceRange subresource_range)
{
  const auto image_view_ci = vk::ImageViewCreateInfo()
    .setImage(eqrect_image)
    .setViewType(vk::ImageViewType::e2D)
    .setFormat(vk::Format::eR32G32B32A32Sfloat)
    .setComponents(vk::ComponentMapping {
        vk::ComponentSwizzle::eR,
        vk::ComponentSwizzle::eG,
        vk::ComponentSwizzle::eB,
        vk::ComponentSwizzle::eA })
    .setSubresourceRange(subresource_range);

  return device.createImageView(image_view_ci);
}

vk::ImageView make_env_cube_view(
    vk::Device device, 
    vk::Image cube_image)
{
  const auto image_view_ci = vk::ImageViewCreateInfo()
    .setImage(cube_image)
    .setViewType(vk::ImageViewType::eCube)
    .setFormat(vk::Format::eR32G32B32A32Sfloat)
    .setComponents(vk::ComponentMapping {
        vk::ComponentSwizzle::eR,
        vk::ComponentSwizzle::eG,
        vk::ComponentSwizzle::eB,
        vk::ComponentSwizzle::eA })
    .setSubresourceRange(vk::ImageSubresourceRange{
        vk::ImageAspectFlagBits::eColor, 0, VK_REMAINING_MIP_LEVELS, 0, 6});
    
  return device.createImageView(image_view_ci);
}

std::vector<vk::DescriptorImageInfo> make_cube_storage_miplevel_image_infos(
    vk::Device device, 
    vk::Image cube_image,
    const uint32_t miplevels)
{ 
  std::vector<vk::DescriptorImageInfo> image_infos(miplevels);

  for (uint32_t i = 0; i < miplevels; ++i) {
    const auto image_view_ci = vk::ImageViewCreateInfo()
      .setImage(cube_image)
      .setViewType(vk::ImageViewType::eCube)
      .setFormat(vk::Format::eR32G32B32A32Sfloat)
      .setComponents(vk::ComponentMapping {
          vk::ComponentSwizzle::eR,
          vk::ComponentSwizzle::eG,
          vk::ComponentSwizzle::eB,
          vk::ComponentSwizzle::eA })
      .setSubresourceRange(vk::ImageSubresourceRange{
          vk::ImageAspectFlagBits::eColor, i, 1, 0, 6});
    
    image_infos[i] = vk::DescriptorImageInfo()
      .setImageView(device.createImageView(image_view_ci))
      .setImageLayout(vk::ImageLayout::eGeneral);
  }

  return image_infos;
}

vk::ImageView make_ird_cube_view(
    vk::Device device, 
    vk::Image ird_cube_image)
{
  const auto image_view_ci = vk::ImageViewCreateInfo()
    .setImage(ird_cube_image)
    .setViewType(vk::ImageViewType::eCube)
    .setFormat(vk::Format::eR32G32B32A32Sfloat)
    .setComponents(vk::ComponentMapping {
        vk::ComponentSwizzle::eR,
        vk::ComponentSwizzle::eG,
        vk::ComponentSwizzle::eB,
        vk::ComponentSwizzle::eA })
    .setSubresourceRange(vk::ImageSubresourceRange{
        vk::ImageAspectFlagBits::eColor, 0, 1, 0, 6});

  return device.createImageView(image_view_ci);
}

vk::ImageView make_brdf_image_view(
    vk::Device device,
    vk::Image brdf_precomp_image)
{
   const auto image_view_ci = vk::ImageViewCreateInfo()
    .setImage(brdf_precomp_image)
    .setViewType(vk::ImageViewType::e2D)
    .setFormat(vk::Format::eR32G32Sfloat)
    .setComponents(vk::ComponentMapping {
        vk::ComponentSwizzle::eR,
        vk::ComponentSwizzle::eG,
        vk::ComponentSwizzle::eB,
        vk::ComponentSwizzle::eA })
    .setSubresourceRange(vk::ImageSubresourceRange{
        vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1});

  return device.createImageView(image_view_ci); 
}

} //namespace

namespace IblPbrPrecompFn_detail {

IblPbrPrecompFn::IblPbrPrecompFn(
    vk::Device device,
    vk::Queue comp_queue, uint32_t comp_queue_family,
    vk::Queue graph_queue, uint32_t graph_queue_family,
    vk::CommandPool comp_command_pool,
    vk::CommandPool graph_command_pool)
  : device_(device)
  , comp_queue_(comp_queue)
  , graph_queue_(graph_queue)
  , comp_queue_family_(comp_queue_family)
  , graph_queue_family_(graph_queue_family)
{

  graph_barrier_cmd_ = make_command_buffer(device_, graph_command_pool);
  comp_barrier_cmd_ = make_command_buffer(device_, comp_command_pool);
  cmd_ = make_command_buffer(device_, comp_command_pool);

  semaphore_ = make_semaphore(device_);
  fence_ = make_fence(device_);

  descriptor_pool_ = make_descriptor_pool(device_);

  eqrect_sampled_image_descriptor_set_layout_ = 
      make_eqrect_sampled_image_descriptor_set_layout(device_);
  cube_sampled_descriptor_set_layout_ = 
      make_cube_sampled_descriptor_set_layout(device_);
  cube_storage_descriptor_set_layout_ = 
      make_cube_storage_descriptor_set_layout(device_);
  ird_convolution_descriptor_set_layout_ = 
      make_ird_convolution_descriptor_set_layout(device_);
  brdf_integration_descriptor_set_layout_ = 
      make_brdf_integration_descriptor_set_layout(device_);

  eqrect_sampled_image_descriptor_set_ = make_descriptor_set(
      device_, descriptor_pool_, eqrect_sampled_image_descriptor_set_layout_); 
  cube_sampled_image_descriptor_set_ = make_descriptor_set(
      device_, descriptor_pool_, cube_sampled_descriptor_set_layout_);
  ird_convolution_descriptor_set_ = make_descriptor_set(
      device_, descriptor_pool_, ird_convolution_descriptor_set_layout_);
  brdf_integration_descriptor_set_ = make_descriptor_set(
      device_, descriptor_pool_, brdf_integration_descriptor_set_layout_);

  eqrect_to_cube_pipeline_ = EqRectToCubePipeline(
      device_,
      eqrect_sampled_image_descriptor_set_layout_, 
      cube_storage_descriptor_set_layout_);
  
  ird_convolution_pipeline_ = IrdConvolutionPipeline(
      device_, 
      ird_convolution_descriptor_set_layout_);
  
  env_prefilter_pipeline_ = EnvPrefilterPipeline(
      device_, 
      cube_sampled_descriptor_set_layout_,
      cube_storage_descriptor_set_layout_);

  brdf_integration_pipeline_ = BrdfIntegrationPipeline(
      device_,
      brdf_integration_descriptor_set_layout_);

  eqrect_sampler_ = make_eqrect_sampler(device_);
  cube_sampler_ = make_env_sampler(device_);
}

IblPbrPrecompFn::~IblPbrPrecompFn()
{
 
  device_.destroy(semaphore_);
  device_.destroy(fence_);
  
  device_.destroy(eqrect_sampler_);
  device_.destroy(cube_sampler_);

  device_.destroy(eqrect_sampled_image_descriptor_set_layout_);
  device_.destroy(cube_sampled_descriptor_set_layout_);
  device_.destroy(cube_storage_descriptor_set_layout_);
  device_.destroy(ird_convolution_descriptor_set_layout_);
  device_.destroy(brdf_integration_descriptor_set_layout_);

  device_.destroy(descriptor_pool_);
  
  device_.destroy(eqrect_to_cube_pipeline_.layout());
  device_.destroy(ird_convolution_pipeline_.layout());
  device_.destroy(env_prefilter_pipeline_.layout());
  device_.destroy(brdf_integration_pipeline_.layout());
  
  device_.destroy(eqrect_to_cube_pipeline_);
  device_.destroy(ird_convolution_pipeline_);
  device_.destroy(env_prefilter_pipeline_);
  device_.destroy(brdf_integration_pipeline_);
  
}

IblPbrPrecompData IblPbrPrecompFn::operator()(
    vk::Image eqrect_image, 
    vk::ImageLayout eqrect_layout,
    vk::ImageSubresourceRange eqrect_subresource,
    vk::Extent2D eqrect_extent,
    vma::Allocator alloc)
{
  const uint64_t disable_timeout = std::numeric_limits<uint64_t>::max();

  input_image_ownership_transfer(
      eqrect_image, eqrect_layout, eqrect_subresource,
      graph_queue_, comp_queue_, 
      graph_queue_family_, comp_queue_family_, 
      graph_barrier_cmd_, comp_barrier_cmd_, 
      semaphore_, fence_);

  IblPbrPrecompData output;

  output.env_side_extent = std::max(1u, eqrect_extent.width >> 2);
  output.ird_side_extent = std::max(1u, output.env_side_extent >> 4);

  output.brdf_extent = vk::Extent2D(1u << 11, 1u << 11);

  uint32_t cube_miplevel_count = 
    mip_level_count(output.env_side_extent) ; 

  auto [cube_image, cube_allocation] = 
    allocate_cube_image(alloc, output.env_side_extent, cube_miplevel_count);

  constexpr uint32_t log2_min_lod_side = 5;
  output.env_miplevel_count = cube_miplevel_count - log2_min_lod_side;

  allocate_output_images(alloc, output);

  const auto eqrect_sampled_image_info = vk::DescriptorImageInfo()
    .setSampler(eqrect_sampler_)
    .setImageView(make_eqrect_view(device_, eqrect_image, eqrect_subresource))
    .setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);

  const auto cube_sampled_image_info = vk::DescriptorImageInfo()
    .setSampler(cube_sampler_)
    .setImageView( make_env_cube_view(device_, cube_image))
    .setImageLayout(vk::ImageLayout::eGeneral);

  const auto ird_storage_image_info = vk::DescriptorImageInfo()
    .setImageView( make_ird_cube_view(device_, output.ird_image))
    .setImageLayout(vk::ImageLayout::eGeneral);
  
  const auto brdf_storage_image_info = vk::DescriptorImageInfo()
    .setImageView( make_brdf_image_view(device_, output.brdf_image))
    .setImageLayout(vk::ImageLayout::eGeneral);

  std::vector<vk::DescriptorImageInfo> 
    cube_miplevel_storage_image_infos =
      make_cube_storage_miplevel_image_infos(
          device_,
          cube_image,
          cube_miplevel_count);

  std::vector<vk::DescriptorImageInfo> 
    env_prefilter_miplevel_storage_image_infos =
      make_cube_storage_miplevel_image_infos(
          device_, 
          output.env_image, 
          output.env_miplevel_count);

  const auto miplevel_storage_sets_pool = 
    make_miplevel_storage_sets_pool(
      device_,
      output.env_miplevel_count + cube_miplevel_count);

  std::vector<vk::DescriptorSet>
    cube_miplevel_storage_descriptor_sets = 
      allocate_miplevel_storage_descriptor_sets(
        device_,
        miplevel_storage_sets_pool,
        cube_storage_descriptor_set_layout_, 
        cube_miplevel_count);

  std::vector<vk::DescriptorSet> 
    env_prefilter_miplevel_storage_descriptor_sets = 
      allocate_miplevel_storage_descriptor_sets(
        device_,
        miplevel_storage_sets_pool,
        cube_storage_descriptor_set_layout_, 
        output.env_miplevel_count);
 
  IblPbrPrecompDescriptorSetsUpdate descriptor_sets_update(
      4 + cube_miplevel_count + output.env_miplevel_count);

  descriptor_sets_update.setEqrectSampledImage(
      eqrect_sampled_image_descriptor_set_,
      eqrect_sampled_image_info);

  descriptor_sets_update.setCubeSampledImage(
      cube_sampled_image_descriptor_set_,
      cube_sampled_image_info);

  descriptor_sets_update.setIrdConvolution(
      ird_convolution_descriptor_set_,
      cube_miplevel_storage_image_infos[0],
      ird_storage_image_info);

  descriptor_sets_update.setBrdfIntegration(
      brdf_integration_descriptor_set_,
      brdf_storage_image_info);
  
  descriptor_sets_update.setCubeMiplevelStorage(
      cube_miplevel_storage_descriptor_sets,
      cube_miplevel_storage_image_infos);

  descriptor_sets_update.setEnvPrefilterStorage(
      env_prefilter_miplevel_storage_descriptor_sets,
      env_prefilter_miplevel_storage_image_infos);

  descriptor_sets_update.update(device_);

  const auto cmd_begin_i = vk::CommandBufferBeginInfo()
    .setFlags(vk::CommandBufferUsageFlagBits::eOneTimeSubmit); 

  cmd_.begin(cmd_begin_i);

  record_layout_transition_to_general_cmd(
      cmd_,
      cube_image,
      output);

  record_cube_generation_dispatch_cmd(
      cmd_, 
      eqrect_to_cube_pipeline_,
      eqrect_sampled_image_descriptor_set_, 
      cube_miplevel_storage_descriptor_sets,
      output.env_side_extent);

  record_cube_generation_barrier_cmd(
      cmd_,
      cube_image,
      0, 1);

  record_ird_convolution_dispatch_cmd(
      cmd_, 
      ird_convolution_pipeline_,  
      ird_convolution_descriptor_set_, 
      output.ird_side_extent);

  record_cube_generation_barrier_cmd(
      cmd_,
      cube_image,
      1, VK_REMAINING_MIP_LEVELS);

  record_env_prefilter_dispatch_cmd(
      cmd_,
      env_prefilter_pipeline_,
      cube_sampled_image_descriptor_set_,
      env_prefilter_miplevel_storage_descriptor_sets,
      output.env_image,
      output.env_side_extent);

  record_brdf_integration_dispatch_cmd(
      cmd_,
      brdf_integration_pipeline_,
      brdf_integration_descriptor_set_,
      output.brdf_extent);

  cmd_.end();

  const auto submit_i = std::array<vk::SubmitInfo, 1> {
    vk::SubmitInfo()
      .setCommandBufferCount(1)
      .setPCommandBuffers(&cmd_)
  };

  //device_.resetFences(1, &fence_);
  comp_queue_.submit(submit_i, vk::Fence());

  //device_.waitForFences(1, &fence_, vk::Bool32(true), disable_timeout);
  device_.resetFences(1, &fence_);

  output_images_ownership_transfer(
    output,
    comp_queue_, graph_queue_, 
    comp_queue_family_, graph_queue_family_,
    comp_barrier_cmd_, graph_barrier_cmd_,
    semaphore_, fence_);

  device_.waitForFences(1, &fence_, vk::Bool32(true), disable_timeout);


  device_.destroy(eqrect_sampled_image_info.imageView);
  device_.destroy(cube_sampled_image_info.imageView);

  device_.destroy(ird_storage_image_info.imageView);
  device_.destroy(brdf_storage_image_info.imageView);
  for (const auto& ii : cube_miplevel_storage_image_infos) {
    device_.destroy(ii.imageView);
  }
  for (const auto& ii : env_prefilter_miplevel_storage_image_infos) {
    device_.destroy(ii.imageView);
  }
  device_.destroy(miplevel_storage_sets_pool);
  alloc.destroy(cube_image, cube_allocation);

  return output; 
}

}
