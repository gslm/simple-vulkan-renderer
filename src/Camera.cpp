#include <Camera.hpp>

#include <iostream>

namespace {
constexpr glm::mat4 clip = glm::mat4(
    1.0f,  0.0f, 0.0f, 0.0f,
    0.0f, -1.0f, 0.0f, 0.0f,
    0.0f,  0.0f, 0.5f, 0.0f,
    0.0f,  0.0f, 0.5f, 1.0f);
}

Camera::Camera() 
{
}

Camera::Camera(float fovy, float aspect, float near, float far)
{
  aspect_ = aspect;
  projection_ = clip * glm::perspective(glm::radians(fovy), aspect, near, far);
}

void Camera::set_target(const glm::vec3& target, float dist) 
{
  translation_ = target;
  dist_ = dist;
}

void Camera::update(
    const vkwapp::App::Input& input, 
    const vkwapp::App::Time& time)
{
  /*
  using Key = vkwapp::App::Input::Key;

  glm::vec2 i = glm::vec2(
      input.key(Key::W) - input.key(Key::S), 
      input.key(Key::A) - input.key(Key::D));
  
  float n = glm::abs(glm::abs(i.x) - glm::abs(i.y));
  i *= (n + (1.0f - n)*glm::inversesqrt(2.0f));
  */

  glm::vec2 m = static_cast<glm::vec2>(input.mouse_position()) * -0.01f;
  
  orientation_ = glm::angleAxis(m.x, glm::vec3(0.0f, 1.0f, 0.0f))
    * glm::angleAxis(m.y, glm::vec3(1.0f, 0.0f, 0.0f));

  orientation_ = glm::normalize(orientation_);
}

float Camera::aspect() 
{
  return aspect_;  
}

float Camera::focal_length()
{
  return projection_[1][1];
}

glm::mat4 Camera::world()
{
  glm::mat4 w = 
    glm::translate(glm::mat4(1.0), translation_) 
    * glm::mat4_cast(orientation_)
    * glm::translate(glm::mat4(1.0), glm::vec3(0.0f, 0.0f, dist_));

  return w;
}

glm::vec3 Camera::position()
{
  return translation_ + (orientation_ * glm::vec3(0.0f, 0.0f, dist_));
}

glm::quat Camera::orientation()
{
  return orientation_;
}

glm::mat4 Camera::view()
{  
  return inverse(world());
}

glm::mat4 Camera::projection()
{
  return projection_;
}
