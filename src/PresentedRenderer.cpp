#include <PresentedRenderer.hpp>

#include <limits>
#include <algorithm>
#include <tuple>

namespace {

//constexpr auto uint64_max = std::numeric_limits<uint64_t>::max();
using AttachmentResource = PresentedRenderer_detail::AttachmentResource;
using SubmitResources = PresentedRenderer_detail::SubmitResources;

vk::Extent2D clamp_output_extent
    (vk::Extent2D output_extent, 
     const vk::SurfaceCapabilitiesKHR& surf_cap)
{
  constexpr uint32_t lim = std::numeric_limits<uint32_t>::max();
  
  if (surf_cap.currentExtent != vk::Extent2D{ lim, lim }) {
    return surf_cap.currentExtent;
  }

  const auto clamp = 
    [](const vk::Extent2D& x, const vk::Extent2D& a, const vk::Extent2D& b) {
    return vk::Extent2D {
      std::clamp(x.width, a.width, b.width),
      std::clamp(x.height, a.height, b.height)
    };
  };

  return clamp(
    output_extent, surf_cap.minImageExtent, surf_cap.maxImageExtent);
}

vk::SurfaceFormatKHR select_surf_format(
    const std::vector<vk::SurfaceFormatKHR>& formats)
{
  constexpr auto default_format = vk::Format::eB8G8R8A8Unorm;

  if (formats.size() == 1 
      && formats[0].format == vk::Format::eUndefined) {
    return {default_format, formats[0].colorSpace};
  }

  return formats[0];
}

vk::PresentModeKHR select_present_mode(
    const std::vector<vk::PresentModeKHR>& modes)
{
  constexpr auto default_mode = vk::PresentModeKHR::eFifo;

  return default_mode;
}

vk::SurfaceTransformFlagBitsKHR select_pre_transform(
    const vk::SurfaceCapabilitiesKHR& surf_cap) 
{
  if (surf_cap.supportedTransforms 
      & vk::SurfaceTransformFlagBitsKHR::eIdentity) {
    return vk::SurfaceTransformFlagBitsKHR::eIdentity;
  }

  return surf_cap.currentTransform;
}

uint32_t select_image_count(
    const vk::SurfaceCapabilitiesKHR& surf_cap) 
{
  constexpr uint32_t desired_image_count = 3;

  auto image_count = 
    std::max(desired_image_count, surf_cap.minImageCount); 
  
  if (surf_cap.maxImageCount > 0) {
    image_count = std::min(image_count, surf_cap.maxImageCount);    
  }

  return image_count;
}

vk::CompositeAlphaFlagBitsKHR select_composite_alpha(
    const vk::SurfaceCapabilitiesKHR& surf_cap) 
{
  const std::array<vk::CompositeAlphaFlagBitsKHR, 4> composite_alphas = {
    vk::CompositeAlphaFlagBitsKHR::eOpaque,
    vk::CompositeAlphaFlagBitsKHR::ePreMultiplied,
    vk::CompositeAlphaFlagBitsKHR::ePostMultiplied,
    vk::CompositeAlphaFlagBitsKHR::eInherit
  };

  for (auto&& ca : composite_alphas) {
    if (surf_cap.supportedCompositeAlpha & ca) return ca;
  }

  return composite_alphas[0];
}

vk::Format select_depth_stencil_format(
    const vk::PhysicalDevice pdev)
{
  const auto depth_stencil_formats = std::array<vk::Format, 3> {
    vk::Format::eD24UnormS8Uint,
    vk::Format::eD32SfloatS8Uint,
    vk::Format::eD16UnormS8Uint,
  };

  for (auto f : depth_stencil_formats) {
    const auto f_props = pdev.getFormatProperties(f);
    
    if (f_props.optimalTilingFeatures != vk::FormatFeatureFlags()) {
      return f;
    }
  }

  throw(std::runtime_error("No Suitable Depth Stencil Format Supported."));

  return vk::Format::eD32SfloatS8Uint;
}

vk::SwapchainKHR make_swapchain(
    const vk::PhysicalDevice pdev,
    const vk::Device dev, 
    const vk::SurfaceKHR surf,
    const vk::SurfaceCapabilitiesKHR& surf_cap,
    const vk::Extent2D surf_extent,
    const vk::SurfaceFormatKHR surf_format)
{
  const auto image_count = select_image_count(surf_cap);
  const auto present_mode = select_present_mode(
      pdev.getSurfacePresentModesKHR(surf));
  const auto pre_transform = select_pre_transform(surf_cap);
  const auto composite_alpha = select_composite_alpha(surf_cap);

  const auto swapchain_ci = vk::SwapchainCreateInfoKHR()
    .setSurface(surf)
    .setMinImageCount(image_count)
    .setImageFormat(surf_format.format)
    .setImageColorSpace(surf_format.colorSpace)
    .setImageExtent(surf_extent)
    .setImageArrayLayers(1)
    .setImageUsage(vk::ImageUsageFlagBits::eTransferDst)
    .setImageSharingMode(vk::SharingMode::eExclusive)
    .setPreTransform(pre_transform)
    .setCompositeAlpha(composite_alpha)
    .setPresentMode(present_mode);

  return dev.createSwapchainKHR(swapchain_ci);
}

AttachmentResource make_attachment(
    vk::Device dev,
    vma::Allocator alloc,
    vk::Extent2D extent, 
    vk::Format format,
    vk::ImageUsageFlags usage,
    vk::ImageAspectFlags aspect)
{
  const auto image_ci = vk::ImageCreateInfo()
    .setImageType(vk::ImageType::e2D)
    .setFormat(format)
    .setExtent(vk::Extent3D{extent, 1})
    .setMipLevels(1)
    .setArrayLayers(1)
    .setSamples(vk::SampleCountFlagBits::e1)
    .setTiling(vk::ImageTiling::eOptimal)
    .setUsage(usage)
    .setSharingMode(vk::SharingMode::eExclusive)
    .setInitialLayout(vk::ImageLayout::eUndefined);

  const auto allocation_ci = vma::AllocationCreateInfo()
    .setFlags(vma::AllocationCreateFlagBits::eDedicatedMemory)
    .setUsage(vma::MemoryUsage::eGpuOnly)
    .setPreferredFlags(vk::MemoryPropertyFlagBits::eDeviceLocal);

  const auto [image, allocation] = alloc.createImage(image_ci, allocation_ci);

  const auto subresource_range = vk::ImageSubresourceRange {
     aspect, 0, 1, 0, 1
  };

  const auto image_view_ci = vk::ImageViewCreateInfo()
    .setImage(image)
    .setViewType(vk::ImageViewType::e2D)
    .setFormat(format)
    .setComponents(vk::ComponentSwizzle())
    .setSubresourceRange(subresource_range);
  
  const auto view = dev.createImageView(image_view_ci);

  return {allocation, image, view};
}

std::array<AttachmentResource, 2> make_attachments(
    const vk::Device dev, 
    const vma::Allocator alloc, 
    const vk::Extent2D extent, 
    const vk::Format color_format,
    const vk::Format depth_stencil_format)
{

  const auto make_attachment_l = [&](
      const vk::Format format, 
      const vk::ImageUsageFlags usage, 
      const vk::ImageAspectFlags aspect) 
  {
    return make_attachment(dev, alloc, extent, format, usage, aspect);
  };

  return std::array<AttachmentResource, 2> {
    make_attachment_l(
        color_format, 
        vk::ImageUsageFlagBits::eColorAttachment 
        | vk::ImageUsageFlagBits::eTransferSrc, 
        vk::ImageAspectFlagBits::eColor)
    ,
    make_attachment_l( 
        depth_stencil_format, 
        vk::ImageUsageFlagBits::eDepthStencilAttachment, 
        vk::ImageAspectFlagBits::eDepth 
        | vk::ImageAspectFlagBits::eStencil)
  };
}

vk::RenderPass make_render_pass(
    const vk::Device dev,
    const vk::Format color_format, 
    const vk::Format depth_stencil_format) 
{
	const std::array<vk::AttachmentDescription, 2> attachments = {
	  vk::AttachmentDescription { // Color
		  vk::AttachmentDescriptionFlags(),
			color_format, 
			vk::SampleCountFlagBits::e1,
			vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eStore,
			vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
			vk::ImageLayout::eUndefined,
			vk::ImageLayout::eTransferSrcOptimal
		},
		vk::AttachmentDescription { // DepthStencil
			vk::AttachmentDescriptionFlags(),
			depth_stencil_format,
			vk::SampleCountFlagBits::e1,
			vk::AttachmentLoadOp::eClear, vk::AttachmentStoreOp::eStore,
			vk::AttachmentLoadOp::eDontCare, vk::AttachmentStoreOp::eDontCare,
			vk::ImageLayout::eUndefined,
			vk::ImageLayout::eDepthStencilAttachmentOptimal
		}
	};

	const std::array<vk::AttachmentReference, 2> attachment_ref = {
		vk::AttachmentReference { // Color
			0, vk::ImageLayout::eColorAttachmentOptimal
		},
		vk::AttachmentReference { // DepthStencil
			1, vk::ImageLayout::eDepthStencilAttachmentOptimal
		}
	};

	const auto subpass_desc = vk::SubpassDescription()
		.setPipelineBindPoint(vk::PipelineBindPoint::eGraphics)
    .setColorAttachmentCount(1)
    .setPColorAttachments(&attachment_ref[0])
		.setPDepthStencilAttachment(&attachment_ref[1]);

	auto render_pass_ci = vk::RenderPassCreateInfo()
    .setAttachmentCount(attachments.size())
    .setPAttachments(attachments.data())
		.setSubpassCount(1)
    .setPSubpasses(&subpass_desc);

	return dev.createRenderPass(render_pass_ci);
}

vk::Framebuffer make_framebuffer(
    vk::Device dev,
    vk::Extent2D extent, 
    const std::array<AttachmentResource, 2>& attachments,
    vk::RenderPass render_pass)
{

  std::array<vk::ImageView, 2> attachment_views;
  std::transform(
      attachments.begin(), 
      attachments.end(), 
      attachment_views.begin(), 
      [](const AttachmentResource& a) { return a.view; });

  const auto framebuffer_ci = vk::FramebufferCreateInfo()
    .setRenderPass(render_pass)
    .setAttachmentCount(attachment_views.size())
    .setPAttachments(attachment_views.data())
    .setWidth(extent.width)
    .setHeight(extent.height)
    .setLayers(1);

  return dev.createFramebuffer(framebuffer_ci);
}

template<size_t N>
std::array<vk::CommandBuffer, N> allocate_render_cmds(
    vk::Device dev, vk::CommandPool pool) 
{
  std::array<vk::CommandBuffer, N> render_cmds;
  
  const auto alloc = dev.allocateCommandBuffers(
    vk::CommandBufferAllocateInfo {
      pool,
      vk::CommandBufferLevel::ePrimary,
      static_cast<uint32_t>(N)
    });

  std::copy(alloc.begin(), alloc.end(), render_cmds.begin());

  return render_cmds;
}

template<size_t N>
std::array<SubmitResources, N> make_submit_resources(vk::Device dev)
{
  std::array<SubmitResources, N> submit_resources;
  const auto semaphore_ci = vk::SemaphoreCreateInfo();

  const auto fence_ci = vk::FenceCreateInfo()
    .setFlags(vk::FenceCreateFlagBits::eSignaled);

  for (auto& e : submit_resources) {
    e = SubmitResources { 
      dev.createSemaphore(semaphore_ci),
      dev.createSemaphore(semaphore_ci),
      dev.createSemaphore(semaphore_ci),
      dev.createFence(fence_ci)
    };
  }

  return submit_resources; 
}

std::tuple<
  std::vector<vk::CommandBuffer>, 
  std::vector<vk::CommandBuffer>
> make_copy_commands(
    const vk::Device dev,
    const vk::SwapchainKHR swapchain,
    const vk::Image render_output, 
    const vk::Extent2D render_extent,
    const vk::CommandPool graphics_command_pool,
    const vk::CommandPool present_command_pool,
    uint32_t graphics_family, 
    uint32_t present_family)
{
  const auto images = dev.getSwapchainImagesKHR(swapchain);
  
  std::tuple<
    std::vector<vk::CommandBuffer>,
    std::vector<vk::CommandBuffer>
  > commands;

  std::vector<vk::CommandBuffer>& copy_cmds = 
    std::get<0>(commands);
  
  std::vector<vk::CommandBuffer>& acquire_cmds = 
    std::get<1>(commands);

  copy_cmds = dev.allocateCommandBuffers(
    vk::CommandBufferAllocateInfo {
      graphics_command_pool,
      vk::CommandBufferLevel::ePrimary,
      static_cast<uint32_t>(images.size())
    });

  if (graphics_family != present_family) {
    acquire_cmds = dev.allocateCommandBuffers(
      vk::CommandBufferAllocateInfo {
        present_command_pool,
        vk::CommandBufferLevel::ePrimary,
        static_cast<uint32_t>(images.size())
      });
  }

  const auto full_image_copy = vk::ImageCopy {
    vk::ImageSubresourceLayers {
      vk::ImageAspectFlagBits::eColor, 0, 0, 1 }, 
    vk::Offset3D { 0, 0, 0 }, 
    vk::ImageSubresourceLayers {
      vk::ImageAspectFlagBits::eColor, 0, 0, 1 }, 
    vk::Offset3D { 0, 0, 0 },
    vk::Extent3D { render_extent, 1 }
  };

  for (size_t i = 0; i < images.size(); ++i) {
    const auto begin_info = vk::CommandBufferBeginInfo {
      vk::CommandBufferUsageFlagBits::eSimultaneousUse, 
      nullptr
    };

    copy_cmds[i].begin(begin_info);

    const auto image_layout_transition = vk::ImageMemoryBarrier {
      vk::AccessFlags(),
      vk::AccessFlags(),
      vk::ImageLayout::eUndefined,
      vk::ImageLayout::eTransferDstOptimal,
      graphics_family,
      present_family,
      images[i],
      vk::ImageSubresourceRange { 
        vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1 } 
    };

    copy_cmds[i].pipelineBarrier(
        vk::PipelineStageFlagBits::eTopOfPipe,
        vk::PipelineStageFlagBits::eTransfer,
        vk::DependencyFlags(),
        0, nullptr,
        0, nullptr,
        1, &image_layout_transition); 

    copy_cmds[i].copyImage(
        render_output, vk::ImageLayout::eTransferSrcOptimal,
        images[i], vk::ImageLayout::eTransferDstOptimal,
        1, &full_image_copy);

    const auto image_ownership_transfer = vk::ImageMemoryBarrier {
      vk::AccessFlags(),
      vk::AccessFlags(),
      vk::ImageLayout::eTransferDstOptimal,
      vk::ImageLayout::ePresentSrcKHR,
      graphics_family,
      present_family,
      images[i],
      vk::ImageSubresourceRange { 
        vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1 } 
    };

    copy_cmds[i].pipelineBarrier(
        vk::PipelineStageFlagBits::eTransfer,
        vk::PipelineStageFlagBits::eColorAttachmentOutput,
        vk::DependencyFlags(),    
        0, nullptr,
        0, nullptr,
        1, &image_ownership_transfer
    );

    copy_cmds[i].end();
    
    if (graphics_family != present_family) { 
      acquire_cmds[i].begin(begin_info);
      
      acquire_cmds[i].pipelineBarrier(
          vk::PipelineStageFlagBits::eTopOfPipe,
          vk::PipelineStageFlagBits::eTopOfPipe,
          vk::DependencyFlags(),
          0, nullptr,
          0, nullptr,
          1, &image_ownership_transfer 
      );

      acquire_cmds[i].end();
    }
  }

  return commands;
}

} //namespace

PresentedRenderer::PresentedRenderer(
    DeviceManager& device_manager, 
    vk::Extent2D desired_output_extent)
  : device_(device_manager.device())
  , alloc_(device_manager.allocator())
{

  uint32_t graphics_family;
  uint32_t present_family;

  std::tie(graphics_queue_, graphics_family) = device_manager.getGraphicsQueue();
  std::tie(present_queue_, present_family) = device_manager.getPresentQueue();

  const vk::PhysicalDevice pdev = device_manager.physicalDevice();
  const vk::Device dev = device_manager.device();
  const vma::Allocator alloc = device_manager.allocator();
  const vk::SurfaceKHR surface = device_manager.surface(); 

  const auto surf_cap = 
      pdev.getSurfaceCapabilitiesKHR(surface);

  const auto output_extent = clamp_output_extent(
      desired_output_extent, surf_cap);
  
  const auto surf_format = select_surf_format(
      pdev.getSurfaceFormatsKHR(surface));

  const auto depth_stencil_format = select_depth_stencil_format(pdev);

  attachments_ = make_attachments(
      dev, alloc, output_extent, surf_format.format, depth_stencil_format);

  const auto render_pass = make_render_pass(
      dev, surf_format.format, depth_stencil_format);

  const auto framebuffer = make_framebuffer(
      dev, output_extent, attachments_, render_pass);

  clear_values_[0].setColor(
      std::array<float, 4>{ { 0.2f, 0.2f, 0.2f, 1.0f } } );
  clear_values_[1].setDepthStencil({1.0f, 0});

  render_pass_begin_info_ = vk::RenderPassBeginInfo {
    render_pass,
    framebuffer,
    vk::Rect2D { vk::Offset2D { 0, 0 }, output_extent },
    static_cast<uint32_t>(clear_values_.size()),
    clear_values_.data()
  };

  swapchain_ = make_swapchain(
      pdev, dev, surface, surf_cap, output_extent, surf_format);

  graphics_command_pool_ = device_manager.getCommandPool(graphics_family);
  present_command_pool_ = device_manager.getCommandPool(present_family);

  render_cmd_sets_ = 
    allocate_render_cmds<QueuedFramesCap*2>(dev, graphics_command_pool_);
  submit_resources_ = make_submit_resources<QueuedFramesCap>(dev);

  std::tie(copy_cmds_, present_acquire_cmds_) = make_copy_commands(
      dev, swapchain_, 
      attachments_[0].image, output_extent,
      graphics_command_pool_, present_command_pool_,
      graphics_family, present_family);
  current_cmd_set_ = 0;
}

PresentedRenderer::~PresentedRenderer() 
{
  graphics_queue_.waitIdle();
  present_queue_.waitIdle();

  for (auto&& sr : submit_resources_) {
    device_.destroy(sr.swapchain_acquire_semaphore);
    device_.destroy(sr.copy_semaphore);
    device_.destroy(sr.present_acquire_semaphore);
  }

  for (auto&& sr : submit_resources_) {
    device_.destroy(sr.fence);
  }

  device_.destroy(render_pass_begin_info_.renderPass);
  device_.destroy(render_pass_begin_info_.framebuffer);
  device_.destroy(swapchain_);

  for (auto&& a : attachments_) device_.destroy(a.view); 
  for (auto&& a : attachments_) alloc_.destroy(a.image, a.allocation);
}

std::vector<vk::CommandBuffer> PresentedRenderer::AllocateCommandBuffers(
    uint32_t count,
    vk::CommandBufferUsageFlags usage) 
{

  usage = usage | vk::CommandBufferUsageFlagBits::eRenderPassContinue
                | vk::CommandBufferUsageFlagBits::eSimultaneousUse;

  const auto inheritence_info = vk::CommandBufferInheritanceInfo()
    .setRenderPass(render_pass_begin_info_.renderPass)
    .setSubpass(0)
    .setFramebuffer(render_pass_begin_info_.framebuffer)
    .setOcclusionQueryEnable(vk::Bool32(false));

  const auto begin_info = vk::CommandBufferBeginInfo {
    usage,
    &inheritence_info
  }; 

  std::vector<vk::CommandBuffer> command_buffers = device_.allocateCommandBuffers(
      vk::CommandBufferAllocateInfo {
        graphics_command_pool_,
        vk::CommandBufferLevel::eSecondary,
        count
      });

  for (auto&& cmd : command_buffers) cmd.begin(begin_info);

  return command_buffers;
}

void PresentedRenderer::FreeCommandBuffers(
    const vk::ArrayProxy<const vk::CommandBuffer>& command_buffers)
{
  device_.freeCommandBuffers(graphics_command_pool_, command_buffers);
}

vk::RenderPass PresentedRenderer::render_pass() 
{
  return render_pass_begin_info_.renderPass;
}

vk::Extent2D PresentedRenderer::output_extent()
{
  return render_pass_begin_info_.renderArea.extent;
}

void PresentedRenderer::SetColorClearValue(std::array<float, 4> color) 
{
  clear_values_[0].setColor(color);
}

void PresentedRenderer::SetDepthClearValue(float depth) 
{
  clear_values_[1].depthStencil.setDepth(depth);
}

void PresentedRenderer::SetStencilClearValue(uint32_t stencil)
{
  clear_values_[1].depthStencil.setStencil(stencil);
}

void PresentedRenderer::SetCommandBuffersSet(
    vk::Fence fence,
    const std::array<
      vk::ArrayProxy<const vk::CommandBuffer>, 2>& command_buffers_set)
{


  for (auto&& cmd : command_buffers_set[0]) cmd.end();
  for (auto&& cmd : command_buffers_set[1]) cmd.end();

  //const auto next_submit = (current_submit_ + 1) % QueuedFramesCap;

  //check if previous cmd has been submited. if not, current_cmd_ is not modified.

  if (!state_bits_[0] ) {
    state_bits_.set(0);
    current_cmd_set_ = (current_cmd_set_ + 1) % RenderCmdSetInstances;
    
    constexpr uint64_t disable_timeout = std::numeric_limits<uint64_t>::max();
    if (fence) device_.waitForFences(1, &fence, false, disable_timeout);
  }

  //vk::CommandBuffer cmd = render_cmds_[current_cmd_];

  const auto begin_info = vk::CommandBufferBeginInfo {
    vk::CommandBufferUsageFlagBits::eSimultaneousUse,
    nullptr
  }; 

  for (size_t i = 0; i < command_buffers_set.size(); ++i) { 
    vk::CommandBuffer cmd = 
      render_cmd_sets_[current_cmd_set_*QueuedFramesCap + i];

    cmd.begin(begin_info);
    cmd.beginRenderPass(
      render_pass_begin_info_,
      vk::SubpassContents::eSecondaryCommandBuffers);
    cmd.executeCommands(
        command_buffers_set[i].size(), 
        command_buffers_set[i].data());
	  cmd.endRenderPass(); 
    cmd.end();
  }

}

void PresentedRenderer::SubmitRender(
    uint32_t command_index, 
    vk::Fence fence,
    vk::Semaphore semaphore,
    const vk::ArrayProxy<const vk::Semaphore>& waitSemaphores,
    const vk::ArrayProxy<const vk::PipelineStageFlags>& waitMasks)
{
  constexpr uint64_t disable_timeout = std::numeric_limits<uint64_t>::max();

  state_bits_.reset(0);

  //current_submit_ = (current_submit_ + 1) % QueuedFramesCap;
  
  const auto render_cmd = 
    render_cmd_sets_[current_cmd_set_*RenderCmdSetInstances + command_index];
  
  // There is no need to wait for the availability of the output_image
  // because it is guaranteed by a pipeline barrier in the copy image commandbuffer.
  const auto render_submit = std::array<vk::SubmitInfo, 1> {
    vk::SubmitInfo {
      waitSemaphores.size(), waitSemaphores.data(), waitMasks.data(),
      1, &render_cmd,
      1, &semaphore, 
    }
  };

  device_.waitForFences(1, &fence, false, disable_timeout);
  device_.resetFences(1, &fence);
  graphics_queue_.submit(render_submit, fence);

/*
  const auto present_resources = submit_resources_[current_submit_].present;

  auto [swapchain_acquire_result, image_index] = 
      device_.acquireNextImageKHR(
        swapchain_, 
        disable_timeout,
        present_resources.swapchain_acquire_semaphore, 
        vk::Fence());

  const auto copy_wait_semaphores = std::array<vk::Semaphore, 2> {
      render_resources.semaphore, 
      present_resources.swapchain_acquire_semaphore
  };

  const auto copy_wait_masks = 
      std::array<vk::PipelineStageFlags, copy_wait_semaphores.size()> {
        vk::PipelineStageFlagBits::eColorAttachmentOutput, 
        vk::PipelineStageFlagBits::eTopOfPipe
      };

  const auto copy_submit = std::array<vk::SubmitInfo, 1> {
    vk::SubmitInfo {
	  static_cast<uint32_t>(copy_wait_semaphores.size()),
	  copy_wait_semaphores.data(), copy_wait_masks.data(),
	  1, &copy_cmds_[image_index],
	  1, &present_resources.copy_semaphore
    } 
  };

  device_.waitForFences(1, &present_resources.fence, false, disable_timeout);
  device_.resetFences(1, &present_resources.fence); 
  graphics_queue_.submit(
      copy_submit, present_acquire_cmds_.empty() ? present_resources.fence 
                                                 : vk::Fence());

  vk::Semaphore image_ready_semaphore = present_resources.copy_semaphore;
  
  const vk::PipelineStageFlags acquire_wait_mask = 
      vk::PipelineStageFlagBits::eTopOfPipe;

  if (!present_acquire_cmds_.empty()) {
    const auto acquire_submit = std::array<vk::SubmitInfo, 1> {
      vk::SubmitInfo {
        1, &present_resources.copy_semaphore, &acquire_wait_mask,
        1, &present_acquire_cmds_[image_index],
        1, &present_resources.present_acquire_semaphore
      }
    };

    present_queue_.submit(acquire_submit, present_resources.fence);
    
    image_ready_semaphore = 
        present_resources.present_acquire_semaphore;
  }

  vk::Result result;
  const auto image_present = vk::PresentInfoKHR {
    1, &image_ready_semaphore,
    1, &swapchain_, &image_index, &result
  };

  present_queue_.presentKHR(image_present);
*/
  //return render_resources.semaphore;
}

vk::Result PresentedRenderer::SubmitPresent(uint32_t command_index, vk::Semaphore render_semaphore) 
{
  const auto present_resources = submit_resources_[command_index];

  constexpr uint64_t disable_timeout = std::numeric_limits<uint64_t>::max();
  
  auto [swapchain_acquire_result, image_index] = 
      device_.acquireNextImageKHR(
        swapchain_, 
        disable_timeout,
        present_resources.swapchain_acquire_semaphore, 
        vk::Fence());

  const auto copy_wait_semaphores = std::array<vk::Semaphore, 2> {
      render_semaphore,
      present_resources.swapchain_acquire_semaphore
  };

  const auto copy_wait_masks = 
    std::array<vk::PipelineStageFlags, copy_wait_semaphores.size()> {
      vk::PipelineStageFlagBits::eColorAttachmentOutput, 
      vk::PipelineStageFlagBits::eTopOfPipe
    };

  const auto copy_submit = std::array<vk::SubmitInfo, 1> {
    vk::SubmitInfo {
	    static_cast<uint32_t>(copy_wait_semaphores.size()),
	    copy_wait_semaphores.data(), copy_wait_masks.data(),
	    1, &copy_cmds_[image_index],
	    1, &present_resources.copy_semaphore
    } 
  };

  device_.waitForFences(1, &present_resources.fence, false, disable_timeout);
  device_.resetFences(1, &present_resources.fence); 
  graphics_queue_.submit(
      copy_submit, present_acquire_cmds_.empty() ? present_resources.fence 
                                                 : vk::Fence());

  vk::Semaphore image_ready_semaphore = present_resources.copy_semaphore;
  
  const vk::PipelineStageFlags acquire_wait_mask = 
      vk::PipelineStageFlagBits::eTopOfPipe;

  if (!present_acquire_cmds_.empty()) {
    const auto acquire_submit = std::array<vk::SubmitInfo, 1> {
      vk::SubmitInfo {
        1, &present_resources.copy_semaphore, &acquire_wait_mask,
        1, &present_acquire_cmds_[image_index],
        1, &present_resources.present_acquire_semaphore
      }
    };

    present_queue_.submit(acquire_submit, present_resources.fence);
    
    image_ready_semaphore = 
        present_resources.present_acquire_semaphore;
  }

  vk::Result result;
  const auto image_present = vk::PresentInfoKHR {
    1, &image_ready_semaphore,
    1, &swapchain_, &image_index, &result
  };

  present_queue_.presentKHR(image_present);

  return result;
}
