#include <DeviceManager.hpp>

namespace {

std::vector<uint32_t> get_queue_families(
    vk::PhysicalDevice pdev, vk::SurfaceKHR surf)
{
  auto q_family_props = pdev.getQueueFamilyProperties();

  constexpr auto max_uint32_t = std::numeric_limits<uint32_t>::max(); 
  std::vector<uint32_t> q_families(5, max_uint32_t);

  for (uint32_t i = 0; i < q_family_props.size(); ++i) {
    const auto queue_flags = q_family_props[i].queueFlags;
    const bool is_graphics = 
      (queue_flags & vk::QueueFlagBits::eGraphics) != vk::QueueFlags();

    const bool is_compute =
      (queue_flags & vk::QueueFlagBits::eCompute) != vk::QueueFlags();

    const bool supports_surface = pdev.getSurfaceSupportKHR(i, surf);

    if (is_graphics and is_compute and supports_surface) q_families[4] = i;
    if (is_graphics and supports_surface) q_families[3] = i;
    else if (supports_surface) q_families[2] = i;
    else if (is_graphics) q_families[3] = i;
    else if (is_compute) q_families[4] = i;
  }

  if (q_families[4] != max_uint32_t) {
    q_families[0] = q_families[4];
    q_families[1] = q_families[4];
    q_families[2] = q_families[4];
  }
  else if (q_families[3] != max_uint32_t) {
    q_families[0] = q_families[3];
    q_families[2] = q_families[3];
  }
  
  if (q_families[0] == max_uint32_t or
      q_families[1] == max_uint32_t or     
      q_families[2] == max_uint32_t) {
    q_families.clear();
  }
  else {
    q_families.resize(3);
  }

  return q_families;
}

vk::Device make_device(
    vk::PhysicalDevice pdev,
    const vk::PhysicalDeviceFeatures& features,
    std::vector<uint32_t> q_families/*, 
    const std::vector<const char*>& extencions*/)
{

  float priority = 1.0f;

  const auto last = std::unique(q_families.begin(), q_families.end());

  std::vector<vk::DeviceQueueCreateInfo> queues_ci(
      std::distance(q_families.begin(), last));

  for (size_t i = 0; i < queues_ci.size(); ++i) {
    queues_ci[i] = vk::DeviceQueueCreateInfo()
      .setQueueFamilyIndex(q_families[i])
      .setQueueCount(1)
      .setPQueuePriorities(&priority);
  }

  std::array<const char*, 1> extensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};
  const auto device_create_ci = vk::DeviceCreateInfo()
    .setQueueCreateInfoCount(queues_ci.size())
    .setPQueueCreateInfos(queues_ci.data())
    .setEnabledExtensionCount(extensions.size())
    .setPpEnabledExtensionNames(extensions.data())
    .setPEnabledFeatures(&features);

  return pdev.createDevice(device_create_ci);
}

vma::Allocator make_allocator(vk::PhysicalDevice pdev, vk::Device dev)
{
  const auto allocator_ci = vma::AllocatorCreateInfo()
    .setPhysicalDevice(pdev)
    .setDevice(dev);

  return vma::createAllocator(allocator_ci);
}

}


DeviceManager::DeviceManager(
      vk::PhysicalDevice pdevice,
      const vk::PhysicalDeviceFeatures& features,
      vk::SurfaceKHR surface)
  : pdevice_(pdevice)
  , surface_(surface)
  , queue_families_(get_queue_families(pdevice, surface))
{

  if (queue_families_.empty()) {
    throw std::runtime_error("No suitable queues found");
  }

  device_ = make_device(pdevice_, features, queue_families_);
  command_pools_ = std::vector<std::vector<command_pool_t>>(queue_families_.size());
  allocator_ = make_allocator(pdevice_, device_);
}

DeviceManager::~DeviceManager()
{
  device_.waitIdle();

  for (auto&& pools : command_pools_) {
    for (auto&& cp : pools) device_.destroy(std::get<0>(cp));
  }
    
  allocator_.destroy();
  device_.destroy();
}

const vk::PhysicalDevice& DeviceManager::physicalDevice() const
{
  return pdevice_;
}

const vk::Device& DeviceManager::device() const 
{
  return device_;
}

const vk::SurfaceKHR& DeviceManager::surface() const
{
  return surface_;
}

const vma::Allocator& DeviceManager::allocator() const
{
  return allocator_;
}

std::tuple<vk::Queue, uint32_t> DeviceManager::getGraphicsQueue()
{
  return {
    device_.getQueue(queue_families_[0], 0), 
    queue_families_[0]
  };
}

std::tuple<vk::Queue, uint32_t> DeviceManager::getComputeQueue()
{
  return {
    device_.getQueue(queue_families_[1], 0), 
    queue_families_[1]
  };
}

std::tuple<vk::Queue, uint32_t> DeviceManager::getPresentQueue()
{
  return {
    device_.getQueue(queue_families_[3], 0), 
    queue_families_[3]
  };
}

vk::CommandPool DeviceManager::getCommandPool(
     uint32_t queueFamilyIndex, 
     vk::CommandPoolCreateFlags flags) 
{
  uint32_t i = 0;
  for (; i < queue_families_.size(); ++i) {
    if (queue_families_[i] == queueFamilyIndex) break;
  }

  if (i == queue_families_.size()) return vk::CommandPool();

  auto& q_fam_idx_pools = command_pools_[i];

  for (const auto& p : command_pools_[i]) {
    if (std::get<1>(p) == flags) return std::get<0>(p);
  }

  const auto command_pool_ci = vk::CommandPoolCreateInfo()
    .setFlags(flags)
    .setQueueFamilyIndex(queueFamilyIndex);

  vk::CommandPool pool = device_.createCommandPool(command_pool_ci);
  q_fam_idx_pools.push_back({pool, flags});

  return pool;
}

