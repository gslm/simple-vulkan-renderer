#include <EnvPipeline.hpp>

#include <array>

namespace {

template<vk::ShaderStageFlagBits>
struct Shaders;

template<>
struct Shaders<vk::ShaderStageFlagBits::eVertex> {
  static constexpr uint32_t code[] = { 
    #include <env.vert.spv>
  };

  static constexpr const char* entry_point = "main";

};

template<>
struct Shaders<vk::ShaderStageFlagBits::eFragment> {
  static constexpr uint32_t code[] = {
    #include <env.frag.spv>
  };
  
   static constexpr const char* entry_point = "main";
};

template<vk::ShaderStageFlagBits Stage>
vk::PipelineShaderStageCreateInfo make_shader_stage_ci(const vk::Device dev)
{
  const auto shader_module_ci = vk::ShaderModuleCreateInfo()
    .setCodeSize(sizeof(Shaders<Stage>::code))
    .setPCode(Shaders<Stage>::code);

 	const auto shader_module = 
    dev.createShaderModule(shader_module_ci);

	return vk::PipelineShaderStageCreateInfo()
    .setStage(Stage)
    .setModule(shader_module)
    .setPName(Shaders<Stage>::entry_point);
}

vk::Pipeline make_pipeline(
    vk::Device dev,
    vk::PipelineCache cache,
    vk::PipelineLayout layout,
    vk::Extent2D render_extent,
    vk::RenderPass render_pass)
{

  const auto stages = std::array<vk::PipelineShaderStageCreateInfo, 2> {
    make_shader_stage_ci<vk::ShaderStageFlagBits::eVertex>(dev),
    make_shader_stage_ci<vk::ShaderStageFlagBits::eFragment>(dev),
  };

  const auto vertex_input_state_ci = 
    vk::PipelineVertexInputStateCreateInfo()
      .setVertexBindingDescriptionCount(0)
      .setPVertexBindingDescriptions(nullptr)
      .setVertexAttributeDescriptionCount(0)
      .setPVertexAttributeDescriptions(nullptr);

  const auto input_assembly_state_ci = 
    vk::PipelineInputAssemblyStateCreateInfo()
      .setTopology(vk::PrimitiveTopology::eTriangleList);
   
  const auto viewport = vk::Viewport {
    0.0f, 0.0f, 
    static_cast<float>(render_extent.width), 
    static_cast<float>(render_extent.height),
    0.0f, 1.0f 
  };

  const auto scissors = vk::Rect2D {
    vk::Offset2D(0, 0), render_extent };

  const auto viewport_state_ci =
    vk::PipelineViewportStateCreateInfo()
      .setViewportCount(1).setPViewports(&viewport)
      .setScissorCount(1).setPScissors(&scissors);

  const auto rasterization_state_ci = 
    vk::PipelineRasterizationStateCreateInfo()
      .setPolygonMode(vk::PolygonMode::eFill)
      .setCullMode(vk::CullModeFlagBits::eNone)
      .setFrontFace(vk::FrontFace::eClockwise)
      .setLineWidth(1.0f);

  const auto multisample_state_ci = 
    vk::PipelineMultisampleStateCreateInfo();

  const auto depth_stencil_state_ci =
    vk::PipelineDepthStencilStateCreateInfo()
      .setDepthTestEnable(vk::Bool32(true))
      .setDepthWriteEnable(vk::Bool32(false))
      .setDepthCompareOp(vk::CompareOp::eLess);

  const auto color_blend_attachment_state = 
    vk::PipelineColorBlendAttachmentState()
      .setColorWriteMask(
          vk::ColorComponentFlagBits::eR 
          | vk::ColorComponentFlagBits::eG 
          | vk::ColorComponentFlagBits::eB
          | vk::ColorComponentFlagBits::eA);

  const auto color_blend_state_ci =
    vk::PipelineColorBlendStateCreateInfo()
      .setAttachmentCount(1)
      .setPAttachments(&color_blend_attachment_state);

  const auto graphics_pipeline_ci = 
   vk::GraphicsPipelineCreateInfo()
      .setStageCount(stages.size()).setPStages(stages.data())
      .setPVertexInputState(&vertex_input_state_ci)
      .setPInputAssemblyState(&input_assembly_state_ci)
      .setPViewportState(&viewport_state_ci)
      .setPRasterizationState(&rasterization_state_ci)
      .setPMultisampleState(&multisample_state_ci)
      .setPDepthStencilState(&depth_stencil_state_ci)
      .setPColorBlendState(&color_blend_state_ci)
      .setLayout(layout)
      .setRenderPass(render_pass)
      .setSubpass(0);
 
  const auto pipeline = dev.createGraphicsPipeline(
      cache, graphics_pipeline_ci);

  for (auto&& s : stages) dev.destroy(s.module);

  return pipeline;
}

vk::PipelineLayout make_pipeline_layout(
    const vk::Device dev, 
    const vk::DescriptorSetLayout envmap_descriptor_set_layout,
    const vk::DescriptorSetLayout view_descriptor_set_layout)
{

  std::array<vk::DescriptorSetLayout, 2> descriptor_set_layouts;

  descriptor_set_layouts[0] = envmap_descriptor_set_layout;
  descriptor_set_layouts[1] = view_descriptor_set_layout;

  const auto push_constant_ranges = std::array<vk::PushConstantRange, 1> {
    vk::PushConstantRange {
      vk::ShaderStageFlagBits::eFragment, 0, sizeof(float) }
  };

  const auto pipeline_layout_ci = vk::PipelineLayoutCreateInfo()
    .setSetLayoutCount(descriptor_set_layouts.size())
    .setPSetLayouts(descriptor_set_layouts.data())
    .setPushConstantRangeCount(push_constant_ranges.size())
    .setPPushConstantRanges(push_constant_ranges.data());

  return dev.createPipelineLayout(pipeline_layout_ci);
}

}; //namespace


EnvPipeline::EnvPipeline() {}
EnvPipeline::EnvPipeline(
    vk::Device device,
    vk::PipelineCache pipeline_cache,
    vk::DescriptorSetLayout envmap_descriptor_set_layout, 
    vk::DescriptorSetLayout view_descriptor_set_layout,
    vk::Extent2D render_extent,
    vk::RenderPass render_pass)
  : pipeline_layout_(make_pipeline_layout(
        device, envmap_descriptor_set_layout, view_descriptor_set_layout))
  , pipeline_(make_pipeline(
        device, pipeline_cache, pipeline_layout_, render_extent, render_pass))
{
}

EnvPipeline::operator vk::Pipeline() const
{
  return pipeline_;
}

vk::PipelineLayout EnvPipeline::layout() const
{
  return pipeline_layout_;
}
