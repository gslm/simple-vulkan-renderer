#include <EqRectToCubePipeline.hpp>

#include <array>
namespace {

template<vk::ShaderStageFlagBits>
struct Shaders;

template<>
struct Shaders<vk::ShaderStageFlagBits::eCompute> {
  static constexpr uint32_t code[] = { 
    #include <eqrect_to_cube.comp.spv>
  };

  static constexpr const char* entry_point = "main";

};

template<vk::ShaderStageFlagBits Stage>
vk::PipelineShaderStageCreateInfo make_shader_stage_ci(const vk::Device dev)
{
  const auto shader_module_ci = vk::ShaderModuleCreateInfo()
    .setCodeSize(sizeof(Shaders<Stage>::code))
    .setPCode(Shaders<Stage>::code);

 	const auto shader_module = 
    dev.createShaderModule(shader_module_ci);

	return vk::PipelineShaderStageCreateInfo()
    .setStage(Stage)
    .setModule(shader_module)
    .setPName(Shaders<Stage>::entry_point);
}

vk::Pipeline make_pipeline(
    vk::Device dev,
    vk::PipelineLayout layout)
{
  const auto stage_ci = vk::PipelineShaderStageCreateInfo(
    make_shader_stage_ci<vk::ShaderStageFlagBits::eCompute>(dev));

  const auto compute_pipeline_ci = vk::ComputePipelineCreateInfo()
    .setStage(stage_ci)
    .setLayout(layout);

  const auto pipeline = dev.createComputePipeline(
      vk::PipelineCache(), compute_pipeline_ci);

  dev.destroy(stage_ci.module);

  return pipeline;
}

vk::PipelineLayout make_pipeline_layout(
    vk::Device dev,
    vk::DescriptorSetLayout descriptor_set0_layout,
    vk::DescriptorSetLayout descriptor_set1_layout)
{
  std::array<vk::DescriptorSetLayout, 2> descriptor_set_layouts;

  descriptor_set_layouts[0] = descriptor_set0_layout;
  descriptor_set_layouts[1] = descriptor_set1_layout;

  const auto push_constant_range = vk::PushConstantRange {
    vk::ShaderStageFlagBits::eCompute, 0, sizeof(float)
  };

  const auto pipeline_layout_ci = vk::PipelineLayoutCreateInfo()
    .setSetLayoutCount(descriptor_set_layouts.size())
    .setPSetLayouts(descriptor_set_layouts.data())
    .setPushConstantRangeCount(1)
    .setPPushConstantRanges(&push_constant_range);

  return dev.createPipelineLayout(pipeline_layout_ci);
}

} //namespace

EqRectToCubePipeline::EqRectToCubePipeline() {}

EqRectToCubePipeline::EqRectToCubePipeline(
    vk::Device device,
    vk::DescriptorSetLayout descriptor_set0_layout,
    vk::DescriptorSetLayout descriptor_set1_layout)
  : pipeline_layout_(make_pipeline_layout(
        device, 
        descriptor_set0_layout,
        descriptor_set1_layout))
  , pipeline_(make_pipeline(
        device, pipeline_layout_))
{
}

EqRectToCubePipeline::operator vk::Pipeline() const
{
  return pipeline_;
}

vk::PipelineLayout EqRectToCubePipeline::layout() const
{
  return pipeline_layout_;
}
