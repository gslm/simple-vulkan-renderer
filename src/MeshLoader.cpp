#include <MeshLoader.hpp>

#define AI_CONFIG_PP_SBP_REMOVE aiPrimitiveType_POINT | aiPrimitiveType_LINE 
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h> 

#include <utils/LocalStackMemoryPool.hpp>

#include <tuple>
#include <limits>

namespace {

using StagingResources = MeshLoader_detail::StagingResources;

std::array<StagingResources, 2> make_staging_resources(
    vk::Device device, vma::Allocator alloc, vk::CommandPool command_pool)
{
  std::array<StagingResources, 2> staging_resources;

  const auto cmd_alloc_i = vk::CommandBufferAllocateInfo()
    .setCommandPool(command_pool)
    .setLevel(vk::CommandBufferLevel::ePrimary)
    .setCommandBufferCount(1);

  const auto fence_ci = vk::FenceCreateInfo()
    .setFlags(vk::FenceCreateFlagBits::eSignaled);

  const auto buffer_ci = vk::BufferCreateInfo()
    .setSize(1 << StagingResources::Log2StagingBufferSize)
    .setUsage(vk::BufferUsageFlagBits::eTransferSrc)
    .setSharingMode(vk::SharingMode::eExclusive);
    
  const auto allocation_ci = vma::AllocationCreateInfo()
    .setUsage(vma::MemoryUsage::eCpuToGpu)
    .setRequieredFlags(vk::MemoryPropertyFlagBits::eHostVisible 
        | vk::MemoryPropertyFlagBits::eHostCoherent);

  
  ut::LocalStackMemoryPool<vk::CommandBuffer, 2> cmd_mem_pool;
  auto sp_alloc = cmd_mem_pool.allocator();

  for (auto& sr : staging_resources) {
    sr.cmd = device.allocateCommandBuffers(
        cmd_alloc_i, sp_alloc, vk::DispatchLoaderStatic())[0];
    
    sr.fence = device.createFence(fence_ci);
    
    std::tie(sr.buffer, sr.allocation) = 
        alloc.createBuffer(buffer_ci, allocation_ci);
  }

  return staging_resources; 
}

std::tuple<vk::Buffer, vma::Allocation> allocate_device_buffer(
  vma::Allocator alloc, size_t size, vk::BufferUsageFlagBits usage)
{ 
  const auto buffer_ci = vk::BufferCreateInfo()
    .setSize(size)
    .setUsage(vk::BufferUsageFlagBits::eTransferDst | usage)
    .setSharingMode(vk::SharingMode::eExclusive);
    
  const auto allocation_ci = vma::AllocationCreateInfo()
    .setUsage(vma::MemoryUsage::eGpuOnly)
    .setPreferredFlags(vk::MemoryPropertyFlagBits::eDeviceLocal);

  return alloc.createBuffer(buffer_ci, allocation_ci);
}

void staging_copy(
    vk::Device device, vma::Allocator alloc, vk::Queue queue,
    vk::Buffer dst, void* src, size_t offset, size_t size, 
    const StagingResources& staging)
{
  const uint64_t disable_timeout = std::numeric_limits<uint64_t>::max();
  device.waitForFences(1, &staging.fence, vk::Bool32(true), disable_timeout); 
  
  auto mapped_ptr = alloc.mapMemory(staging.allocation);
  std::memcpy( mapped_ptr, static_cast<uint8_t*>(src) + offset, size);
  alloc.unmapMemory(staging.allocation);

  const auto regions = std::array<vk::BufferCopy, 1> {
    vk::BufferCopy { 0, offset, size }
  };
  
  const auto cmd_begin_i = vk::CommandBufferBeginInfo()
    .setFlags(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);

  staging.cmd.begin(cmd_begin_i);
  staging.cmd.copyBuffer(staging.buffer, dst, regions); 
  staging.cmd.end();
  
  const auto submit = std::array<vk::SubmitInfo, 1> {
    vk::SubmitInfo()
      .setCommandBufferCount(1)
      .setPCommandBuffers(&staging.cmd)
  };

  device.resetFences(1, &staging.fence);
  queue.submit(submit, staging.fence);
}

void copy_to_device(
    vk::Device device, vma::Allocator alloc, vk::Queue queue,
    vk::Buffer dst, void* src, size_t size,
    std::array<StagingResources, 2> stagings)
{
  const size_t staging_size = 1 << StagingResources::Log2StagingBufferSize;
  const size_t full_copy_count = 
      size >> StagingResources::Log2StagingBufferSize;

  size_t i = 0;
  for (; i < full_copy_count; ++i) {
    staging_copy(
        device, alloc, queue,
        dst, src, i*staging_size, staging_size, 
        stagings[i&1]);
  }

  size_t rem_size = size & (staging_size - 1);

  if (rem_size > 0) {
    staging_copy(
        device, alloc, queue,
        dst, src, i*staging_size, rem_size, 
        stagings[i&1]);
  }

  const uint64_t disable_timeout = std::numeric_limits<uint64_t>::max();

  device.waitForFences(1, &stagings[0].fence, vk::Bool32(true), disable_timeout);
  device.waitForFences(1, &stagings[1].fence, vk::Bool32(true), disable_timeout);
}

std::tuple<size_t, size_t> get_mesh_sizes(const aiScene* ai_scene)
{ 
  size_t vertex_count = 0;
  size_t face_count = 0;

  const size_t mesh_count = ai_scene->mNumMeshes;
  for (size_t i = 0; i < mesh_count; ++i) {
    aiMesh* mesh = ai_scene->mMeshes[i];

    vertex_count += mesh->mNumVertices;
    face_count += mesh->mNumFaces;
  }

  return {vertex_count, face_count};
}

} //namespace


struct MeshLoader::private_data_t {
  Assimp::Importer importer;
};

MeshLoader::MeshLoader(DeviceManager& device_manager)
  : device_(device_manager.device())
  , alloc_(device_manager.allocator())
{
  auto [queue, q_family] = device_manager.getGraphicsQueue();
  queue_ = queue;

  vk::CommandPool command_pool = device_manager.getCommandPool(
      q_family,         
      vk::CommandPoolCreateFlagBits::eTransient 
        | vk::CommandPoolCreateFlagBits::eResetCommandBuffer);

  staging_resources_ = make_staging_resources(device_, alloc_, command_pool);
}

MeshLoader::~MeshLoader()
{
  for (auto& sr : staging_resources_) {
    device_.destroy(sr.fence);
    alloc_.destroy(sr.buffer, sr.allocation);
  }
}

MeshLoader::Mesh MeshLoader::load(const std::string& file_path)
{
  Assimp::Importer& importer = private_data_.get().importer;

  const aiScene* ai_scene = importer.ReadFile(
      file_path,
      aiProcess_Triangulate
      | aiProcess_GenNormals
      | aiProcess_GenSmoothNormals
      | aiProcess_PreTransformVertices);

  if (!ai_scene) {
    throw std::runtime_error(importer.GetErrorString());
  }
 
  auto [vertex_count, face_count] = get_mesh_sizes(ai_scene); 

  std::vector<Mesh::vertex_type> vertices(vertex_count);
  std::vector<Mesh::vertex_type> normals(vertex_count);
  std::vector<Mesh::triangle_type> triangles(face_count);

  Mesh mesh;
  constexpr float max_float = std::numeric_limits<float>::max();
  constexpr float min_float = std::numeric_limits<float>::min();

  mesh.bbox[0] = Mesh::vertex_type(max_float);
  mesh.bbox[1] = Mesh::vertex_type(min_float);
  {
    const size_t mesh_count = ai_scene->mNumMeshes;
    auto ver_it = vertices.begin();
    auto nor_it = normals.begin();
    auto tri_it = triangles.begin();

    for (size_t i = 0; i < mesh_count; ++i) {
      aiMesh* ai_mesh = ai_scene->mMeshes[i];

      ver_it = std::transform(
          ai_mesh->mVertices, ai_mesh->mVertices + ai_mesh->mNumVertices, 
          ver_it,
          [&mesh](const aiVector3D& v) {
            Mesh::vertex_type tv(v.x, v.y, v.z);

            mesh.bbox[0] = glm::min(tv, mesh.bbox[0]);
            mesh.bbox[1] = glm::max(tv, mesh.bbox[1]);

            return Mesh::vertex_type(tv.x, tv.y, tv.z);       
          });

      nor_it = std::transform(
          ai_mesh->mNormals, ai_mesh->mNormals + ai_mesh->mNumVertices, 
          nor_it,
          [](const aiVector3D& v) {
            return Mesh::normal_type(v.x, v.y, v.z);       
          });

      tri_it = std::transform(
          ai_mesh->mFaces, ai_mesh->mFaces + ai_mesh->mNumFaces, 
          tri_it,
          [](const aiFace& face) -> Mesh::triangle_type {
            return {face.mIndices[0], face.mIndices[1], face.mIndices[2]};       
          });
    }
  }

  importer.FreeScene();
  
  mesh.vertices_size = vertices.size();
  mesh.triangles_size = triangles.size();

  std::tie(mesh.vertices_buffer, mesh.vertices_alloc) = 
    allocate_device_buffer(
      alloc_, 
      vertices.size() * sizeof(Mesh::vertex_type), 
      vk::BufferUsageFlagBits::eVertexBuffer);
  
  std::tie(mesh.normals_buffer, mesh.normals_alloc) = 
    allocate_device_buffer(
      alloc_, 
      vertices.size() * sizeof(Mesh::normal_type), 
      vk::BufferUsageFlagBits::eVertexBuffer);
  
  std::tie(mesh.triangles_buffer, mesh.triangles_alloc) = 
    allocate_device_buffer(
      alloc_, 
      triangles.size() * sizeof(Mesh::triangle_type),
      vk::BufferUsageFlagBits::eIndexBuffer);

  copy_to_device(
      device_, alloc_, queue_, 
      mesh.vertices_buffer, vertices.data(), 
      vertices.size() * sizeof(Mesh::vertex_type),
      staging_resources_);
  
  copy_to_device(
      device_, alloc_, queue_, 
      mesh.normals_buffer, normals.data(), 
      vertices.size() * sizeof(Mesh::normal_type),
      staging_resources_);
  
  copy_to_device(
      device_, alloc_, queue_, 
      mesh.triangles_buffer, triangles.data(), 
      triangles.size() * sizeof(Mesh::triangle_type),
      staging_resources_);

  return mesh;
}
