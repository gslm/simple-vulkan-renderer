#include <Scene.hpp>

#include <IblPbrPrecompFn.hpp>

#include <utils/LocalStackMemoryPool.hpp>

#include <glm_user_flags.hpp>

#include <glm/glm.hpp>
#include <glm/gtx/type_aligned.hpp>

#include <array>
#include <cstdio>
#include <thread>
#include <chrono>

namespace {

using BufferAlloc = Scene_detail::BufferAlloc;
using SampledImage = Scene_detail::SampledImage;

struct Transform {
  glm::aligned_vec3 view_positon;
  glm::aligned_mat4 model_view;
  glm::aligned_mat4 projection;
};

struct View {
  float aspect;
  float focal_length;
  glm::aligned_mat4 world;
};


vk::PipelineCache make_pipeline_cache(const vk::Device dev)
{
  const auto pipeline_cache_ci = vk::PipelineCacheCreateInfo();

  return dev.createPipelineCache(pipeline_cache_ci);
}

vk::DescriptorPool make_descriptor_pool(const vk::Device dev)
{
  const auto pool_sizes = std::array<vk::DescriptorPoolSize, 2> {
    vk::DescriptorPoolSize { vk::DescriptorType::eUniformBuffer, 4 },
    vk::DescriptorPoolSize { vk::DescriptorType::eCombinedImageSampler, 3 }
  };  

  const auto descriptor_pool_ci = vk::DescriptorPoolCreateInfo()
    .setMaxSets(5)
    .setPoolSizeCount(pool_sizes.size())
    .setPPoolSizes(pool_sizes.data());

  return dev.createDescriptorPool(descriptor_pool_ci);
}

vk::DescriptorSetLayout make_transform_descriptor_set_layout(
    const vk::Device dev)
{
  const auto layout_bindings = std::array<vk::DescriptorSetLayoutBinding, 1> {
    vk::DescriptorSetLayoutBinding()
      .setBinding(0)
      .setDescriptorType(vk::DescriptorType::eUniformBuffer)
      .setDescriptorCount(1)
      .setStageFlags(vk::ShaderStageFlagBits::eVertex)
  };

  const auto descriptor_set_layout_ci = vk::DescriptorSetLayoutCreateInfo()
    .setBindingCount(layout_bindings.size())
    .setPBindings(layout_bindings.data());

  return dev.createDescriptorSetLayout(descriptor_set_layout_ci);
}

vk::DescriptorSetLayout make_view_descriptor_set_layout(
    const vk::Device dev)
{
   const auto layout_bindings = std::array<vk::DescriptorSetLayoutBinding, 1> {
    vk::DescriptorSetLayoutBinding()
      .setBinding(0)
      .setDescriptorType(vk::DescriptorType::eUniformBuffer)
      .setDescriptorCount(1)
      .setStageFlags(vk::ShaderStageFlagBits::eVertex)
  };

  const auto descriptor_set_layout_ci = vk::DescriptorSetLayoutCreateInfo()
    .setBindingCount(layout_bindings.size())
    .setPBindings(layout_bindings.data());

  return dev.createDescriptorSetLayout(descriptor_set_layout_ci); 
}

vk::DescriptorSetLayout make_envmap_descriptor_set_layout(
    const vk::Device dev)
{
  const auto layout_bindings = std::array<vk::DescriptorSetLayoutBinding, 3> {
    vk::DescriptorSetLayoutBinding()
      .setBinding(0)
      .setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
      .setDescriptorCount(1)
      .setStageFlags(vk::ShaderStageFlagBits::eFragment)
    ,
    vk::DescriptorSetLayoutBinding()
      .setBinding(1)
      .setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
      .setDescriptorCount(1)
      .setStageFlags(vk::ShaderStageFlagBits::eFragment)
    ,
    vk::DescriptorSetLayoutBinding()
      .setBinding(2)
      .setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
      .setDescriptorCount(1)
      .setStageFlags(vk::ShaderStageFlagBits::eFragment)
  };

  const auto descriptor_set_layout_ci = vk::DescriptorSetLayoutCreateInfo()
    .setBindingCount(layout_bindings.size())
    .setPBindings(layout_bindings.data());

  return dev.createDescriptorSetLayout(descriptor_set_layout_ci);
}

void make_descriptor_sets(
    const vk::Device dev, 
    const vk::DescriptorPool pool,
    const vk::DescriptorSetLayout env_descriptor_set_layout,
    const vk::DescriptorSetLayout transform_descriptor_set_layout,
    const vk::DescriptorSetLayout view_descriptor_set_layout,
    vk::DescriptorSet& env_descriptor_set,
    std::array<vk::DescriptorSet, 2>& transform_descriptor_set,
    std::array<vk::DescriptorSet, 2>& view_descriptor_set)
{

  const auto set_layouts = std::array<vk::DescriptorSetLayout, 5> {
    env_descriptor_set_layout,
    transform_descriptor_set_layout,
    transform_descriptor_set_layout,
    view_descriptor_set_layout,
    view_descriptor_set_layout,
  };

  const auto allocation_info = vk::DescriptorSetAllocateInfo()
    .setDescriptorPool(pool)
    .setDescriptorSetCount(set_layouts.size())
    .setPSetLayouts(set_layouts.data());

  ut::LocalStackMemoryPool<vk::DescriptorSet, 5> mem_pool;
  const auto descriptor_sets = dev.allocateDescriptorSets(
        allocation_info, mem_pool.allocator(), vk::DispatchLoaderStatic());

  env_descriptor_set = descriptor_sets[0]; 
  transform_descriptor_set[0] = descriptor_sets[1];
  transform_descriptor_set[1] = descriptor_sets[2];
  view_descriptor_set[0] = descriptor_sets[3];
  view_descriptor_set[1] = descriptor_sets[4];
  
}

void update_descriptor_sets(
    vk::Device dev,
    const std::array<BufferAlloc, 2>& uniform_buffer,
    const SampledImage& env_sampled_image,
    const SampledImage& ird_sampled_image,
    const SampledImage& brdf_sampled_image,
    vk::DescriptorSet env_descriptor_set,
    std::array<vk::DescriptorSet, 2> transform_descriptor_sets,
    std::array<vk::DescriptorSet, 2> view_descriptor_sets)
{
  const auto transform_buffer_info = [&uniform_buffer]() {
    std::array<vk::DescriptorBufferInfo, 2> buffer_infos;
    
    for (size_t i = 0; i < uniform_buffer.size(); ++i) {
      buffer_infos[i] = vk::DescriptorBufferInfo()
        .setBuffer(uniform_buffer[i].buffer)
        .setOffset(0)
        .setRange(sizeof(Transform));
    }

    return buffer_infos;
  }();
  
  const auto view_buffer_info = [&uniform_buffer]() {
    std::array<vk::DescriptorBufferInfo, 2> buffer_infos;
    
    for (size_t i = 0; i < uniform_buffer.size(); ++i) {
      buffer_infos[i] = vk::DescriptorBufferInfo()
        .setBuffer(uniform_buffer[i].buffer)
        .setOffset(sizeof(Transform))
        .setRange(sizeof(View));
    }

    return buffer_infos;
  }();

  const auto env_image_info = vk::DescriptorImageInfo()
      .setSampler(env_sampled_image.sampler)
      .setImageView(env_sampled_image.view)
      .setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);

  const auto ird_image_info = vk::DescriptorImageInfo()
      .setSampler(ird_sampled_image.sampler)
      .setImageView(ird_sampled_image.view)
      .setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);

  const auto brdf_image_info = vk::DescriptorImageInfo()
      .setSampler(brdf_sampled_image.sampler)
      .setImageView(brdf_sampled_image.view)
      .setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal);

  const auto write_descriptor_sets = [&]() {
    std::array<vk::WriteDescriptorSet, 7> sets;

    sets[0] = vk::WriteDescriptorSet()
      .setDstBinding(0)
      .setDstSet(env_descriptor_set)
      .setDescriptorCount(1)
      .setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
      .setPImageInfo(&env_image_info);
    
    sets[1] = vk::WriteDescriptorSet()
      .setDstBinding(1)
      .setDstSet(env_descriptor_set)
      .setDescriptorCount(1)
      .setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
      .setPImageInfo(&ird_image_info);
    
    sets[2] = vk::WriteDescriptorSet()
      .setDstBinding(2)
      .setDstSet(env_descriptor_set)
      .setDescriptorCount(1)
      .setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
      .setPImageInfo(&brdf_image_info);
    
    for (size_t i = 0; i < transform_descriptor_sets.size(); ++i) {
      sets[3 + i] = 
        vk::WriteDescriptorSet()
          .setDstBinding(0)
          .setDstSet(transform_descriptor_sets[i])
          .setDescriptorCount(1)
          .setDescriptorType(vk::DescriptorType::eUniformBuffer)
          .setPBufferInfo(&transform_buffer_info[i]);
    }
    
    for (size_t i = 0; i < view_descriptor_sets.size(); ++i) {
      sets[3 + transform_descriptor_sets.size() + i] = 
        vk::WriteDescriptorSet()
          .setDstBinding(0)
          .setDstSet(view_descriptor_sets[i])
          .setDescriptorCount(1)
          .setDescriptorType(vk::DescriptorType::eUniformBuffer)
          .setPBufferInfo(&view_buffer_info[i]);
    }

    return sets;
  }();

  dev.updateDescriptorSets(write_descriptor_sets, {});
}

std::array<BufferAlloc, 2> make_uniform_buffers(
    const vk::Device dev, vma::Allocator alloc)
{
  const auto buffer_ci = vk::BufferCreateInfo()
    .setSize(sizeof(Transform) + sizeof(View))
    .setUsage(vk::BufferUsageFlagBits::eUniformBuffer)
    .setSharingMode(vk::SharingMode::eExclusive);

  auto allocation_ci = vma::AllocationCreateInfo()
    .setUsage(vma::MemoryUsage::eCpuToGpu)
    .setRequieredFlags(
        vk::MemoryPropertyFlagBits::eHostVisible | 
        vk::MemoryPropertyFlagBits::eHostCoherent);

  auto [buffer0, allocation0] = alloc.createBuffer(buffer_ci, allocation_ci);
  auto [buffer1, allocation1] = alloc.createBuffer(buffer_ci, allocation_ci);

  return { BufferAlloc{buffer0, allocation0}, 
           BufferAlloc{buffer1, allocation1} };
}

SampledImage make_envcube_sampled_image(
    const vk::Device dev,
    vk::Image image, 
    vma::Allocation allocation) 
{
   const auto sampler_ci = vk::SamplerCreateInfo()
    .setMagFilter(vk::Filter::eLinear)
    .setMinFilter(vk::Filter::eLinear)
    .setMipmapMode(vk::SamplerMipmapMode::eLinear)
    .setAddressModeU(vk::SamplerAddressMode::eClampToEdge)
    .setAddressModeV(vk::SamplerAddressMode::eClampToEdge)
    .setAnisotropyEnable(vk::Bool32(false))
    .setCompareEnable(vk::Bool32(false))
    .setMinLod(0.0f)
    .setMaxLod(1000.0f)
    .setMipLodBias(0.0f)
    .setUnnormalizedCoordinates(vk::Bool32(false));

  const auto image_view_ci = vk::ImageViewCreateInfo()
    .setImage(image)
    .setViewType(vk::ImageViewType::eCube)
    .setFormat(vk::Format::eR32G32B32A32Sfloat)
    .setComponents(vk::ComponentMapping {
        vk::ComponentSwizzle::eR, 
        vk::ComponentSwizzle::eG,
        vk::ComponentSwizzle::eB,
        vk::ComponentSwizzle::eA })
    .setSubresourceRange(vk::ImageSubresourceRange {
        vk::ImageAspectFlagBits::eColor, 0, VK_REMAINING_MIP_LEVELS, 0, 6});

  return SampledImage { 
      image, 
      allocation, 
      dev.createImageView(image_view_ci), 
      dev.createSampler(sampler_ci) 
  };
}

SampledImage make_brdf_sampled_image(
    const vk::Device dev,
    vk::Image image, 
    vma::Allocation allocation) 
{
   const auto sampler_ci = vk::SamplerCreateInfo()
    .setMagFilter(vk::Filter::eLinear)
    .setMinFilter(vk::Filter::eLinear)
    .setMipmapMode(vk::SamplerMipmapMode::eLinear)
    .setAddressModeU(vk::SamplerAddressMode::eClampToEdge)
    .setAddressModeV(vk::SamplerAddressMode::eClampToEdge)
    .setAnisotropyEnable(vk::Bool32(false))
    .setCompareEnable(vk::Bool32(false))
    .setMinLod(0.0f)
    .setMaxLod(1000.0f)
    .setMipLodBias(0.0f)
    .setUnnormalizedCoordinates(vk::Bool32(false));

  const auto image_view_ci = vk::ImageViewCreateInfo()
    .setImage(image)
    .setViewType(vk::ImageViewType::e2D)
    .setFormat(vk::Format::eR32G32Sfloat)
    .setComponents(vk::ComponentMapping {
        vk::ComponentSwizzle::eR, 
        vk::ComponentSwizzle::eG,
        vk::ComponentSwizzle::eB,
        vk::ComponentSwizzle::eA })
    .setSubresourceRange(vk::ImageSubresourceRange {
        vk::ImageAspectFlagBits::eColor, 0, VK_REMAINING_MIP_LEVELS, 0, 1});

  return SampledImage { 
      image, 
      allocation, 
      dev.createImageView(image_view_ci), 
      dev.createSampler(sampler_ci) 
  };
}

} //namespace

Scene::Scene(
    DeviceManager& device_manager,
    const vk::Extent2D render_extent,
    const vk::RenderPass render_pass,
    const Scene::SceneDescription& scene_description)
  : device_(device_manager.device())
  , alloc_(device_manager.allocator())
  , mesh_loader_(device_manager)
  , texture_loader_(device_manager)
  , pipeline_cache_(make_pipeline_cache(device_))
{
  std::tie(graphics_queue_, graphics_family_) = 
    device_manager.getGraphicsQueue();

  std::cout << "Loading Mesh..." << std::flush;
  
  mesh_ = mesh_loader_.load(scene_description.model_path);

  std::cout << " Done." << std::endl;
  
  TextureLoader::LoadFlags envmap_load_flags = 
      TextureLoader::LoadFlags::Hdr 
      | TextureLoader::LoadFlags::AllocateMipLevels
      | TextureLoader::LoadFlags::GenerateMipLevels;

  std::cout << "Loading Enviroment Image..." << std::flush;
  
  Texture eqrect_env_texture = texture_loader_.load(
      scene_description.env_path, envmap_load_flags); 
  
  std::cout << " Done." << std::endl;

  using IblPbrPrecompFnT = IblPbrPrecompFn<DeviceManager>;
  IblPbrPrecompFnT ibl_pbr_precomp(device_manager);

  std::cout << "Precomputing lighting. This may take a while..." << std::flush;
  
  using namespace std::chrono_literals;
  // Give time to output the message. 
  // My system (APU) goes unresponsive while precomputing lighting.
  // so that the warning message does not apear until the computation is done.
  std::this_thread::sleep_for(100ms);

  IblPbrPrecompFnT::IblPbrPrecompData ibl_data = 
      ibl_pbr_precomp(
        eqrect_env_texture.image, 
        vk::ImageLayout::eShaderReadOnlyOptimal,
        vk::ImageSubresourceRange { 
          vk::ImageAspectFlagBits::eColor, 0, VK_REMAINING_MIP_LEVELS, 0, 1 },
        eqrect_env_texture.extent, 
        alloc_);

  std::cout << " Done." << std::endl;

  alloc_.destroy(eqrect_env_texture.image, eqrect_env_texture.allocation);

  env_miplevel_count_ = static_cast<float>(ibl_data.env_miplevel_count); 

  env_sampled_image_ = make_envcube_sampled_image(
      device_, ibl_data.env_image, ibl_data.env_allocation);

  ird_sampled_image_ = make_envcube_sampled_image(
      device_, ibl_data.ird_image, ibl_data.ird_allocation);

  brdf_sampled_image_ = make_brdf_sampled_image(
      device_, ibl_data.brdf_image, ibl_data.brdf_allocation);

  uniform_buffer_allocs_ = make_uniform_buffers(device_, alloc_); 

  env_descriptor_set_layout_ = 
    make_envmap_descriptor_set_layout(device_);
  transform_descriptor_set_layout_ = 
    make_transform_descriptor_set_layout(device_);
  view_descriptor_set_layout_ = 
    make_view_descriptor_set_layout(device_);

  descriptor_pool_ = make_descriptor_pool(device_);
  make_descriptor_sets(
      device_, 
      descriptor_pool_, 
      env_descriptor_set_layout_,
      transform_descriptor_set_layout_,
      view_descriptor_set_layout_, 
      env_descriptor_set_,
      transform_descriptor_set_,
      view_descriptor_set_);

  update_descriptor_sets(
      device_,
      uniform_buffer_allocs_,
      env_sampled_image_,
      ird_sampled_image_,
      brdf_sampled_image_,
      env_descriptor_set_,
      transform_descriptor_set_,
      view_descriptor_set_);

  pbr_pipeline_ = PbrPipeline(
      device_, 
      pipeline_cache_, 
      env_descriptor_set_layout_, 
      transform_descriptor_set_layout_,
      render_extent, render_pass);

  env_pipeline_ = EnvPipeline(
      device_, 
      pipeline_cache_, 
      env_descriptor_set_layout_, 
      view_descriptor_set_layout_,
      render_extent, render_pass);

  float aspect = static_cast<float>(render_extent.width) 
                  / static_cast<float>(render_extent.height);
  camera_ = Camera(80.0f, aspect, 0.1f, 100.0f);

  glm::vec3 bv = mesh_.bbox[1] - mesh_.bbox[0];

  camera_.set_target(mesh_.bbox[0] + bv * 0.5f, glm::length(bv));
}

Scene::~Scene()
{
  device_.destroy(env_descriptor_set_layout_);
  device_.destroy(transform_descriptor_set_layout_);
  device_.destroy(view_descriptor_set_layout_);

  device_.destroy(pbr_pipeline_.layout());
  device_.destroy(pbr_pipeline_);
  device_.destroy(env_pipeline_.layout());
  device_.destroy(env_pipeline_);

  device_.destroy(descriptor_pool_);
  device_.destroy(pipeline_cache_);
 
  device_.destroy(env_sampled_image_.view);
  device_.destroy(ird_sampled_image_.view);
  device_.destroy(brdf_sampled_image_.view);

  device_.destroy(env_sampled_image_.sampler);
  device_.destroy(ird_sampled_image_.sampler);
  device_.destroy(brdf_sampled_image_.sampler);

  alloc_.destroy(env_sampled_image_.image, env_sampled_image_.allocation);
  alloc_.destroy(ird_sampled_image_.image, ird_sampled_image_.allocation);
  alloc_.destroy(brdf_sampled_image_.image, brdf_sampled_image_.allocation);

  for (auto& a : uniform_buffer_allocs_) {
    alloc_.destroy(a.buffer, a.allocation);
  }

  alloc_.destroy(mesh_.vertices_buffer, mesh_.vertices_alloc);
  alloc_.destroy(mesh_.normals_buffer, mesh_.vertices_alloc);
  alloc_.destroy(mesh_.triangles_buffer, mesh_.triangles_alloc);
}

void Scene::RecordRenderCommands(
    const std::array<vk::CommandBuffer, 2>& cmds)
{
  const auto ibl_descriptor_set_bindings = 
    std::array<vk::DescriptorSet, 1> {
        env_descriptor_set_,
  };

  for (int i = 0; i < 2; ++i) {
    auto cmd = cmds[i];

    cmd.pushConstants<float>(
        pbr_pipeline_.layout(),
        vk::ShaderStageFlagBits::eFragment,
        0, std::array<float, 1>{env_miplevel_count_});

    cmd.bindDescriptorSets(
      vk::PipelineBindPoint::eGraphics, 
      pbr_pipeline_.layout(), 
      0, ibl_descriptor_set_bindings, {});
   
    const auto transform_descriptor_set_bindings =
      std::array<vk::DescriptorSet, 1> { 
        transform_descriptor_set_[i],
    };

    cmd.bindDescriptorSets(
      vk::PipelineBindPoint::eGraphics, 
      pbr_pipeline_.layout(), 
      1, transform_descriptor_set_bindings, {});

    // Draw mesh.
    cmd.bindPipeline(vk::PipelineBindPoint::eGraphics, pbr_pipeline_); 

    const auto vertex_buffers = std::array<vk::Buffer, 2> {
      mesh_.vertices_buffer,
      mesh_.normals_buffer
    };
    
    const auto offsets = std::array<vk::DeviceSize, 2> {
      0, 0
    };

    cmd.bindVertexBuffers(0, vertex_buffers, offsets);
    cmd.bindIndexBuffer(mesh_.triangles_buffer, 0, Mesh::IndexType);
    cmd.drawIndexed(3*mesh_.triangles_size, 1, 0, 0, 0);

    const auto view_descriptor_set_bindings = 
      std::array<vk::DescriptorSet, 1> {
        view_descriptor_set_[i],
    };

    // Draw background.
    cmd.bindDescriptorSets(
      vk::PipelineBindPoint::eGraphics, 
      env_pipeline_.layout(), 
      1, view_descriptor_set_bindings, {});
    
    cmd.bindPipeline(vk::PipelineBindPoint::eGraphics, env_pipeline_);
    // Draw background.
    cmd.draw(3, 2, 0, 0);
  }
}

void Scene::Update(
    uint32_t buffer_index, 
    vk::Fence wait_fence,
    const vkwapp::App::Input& input, 
    const vkwapp::App::Time& time) 
{
  camera_.update(input, time);

  Transform t {
    camera_.position(),
    camera_.view(),
    camera_.projection()
  };

  //const glm::quat o = camera_.orientation();

  View v {
    camera_.aspect(),
    camera_.focal_length(),
    camera_.world()
  };

  if (wait_fence != vk::Fence()) {
    constexpr uint64_t disable_timeout = std::numeric_limits<uint64_t>::max();
    device_.waitForFences(1, &wait_fence, false, disable_timeout); 
  }

  uint8_t* mapped_ptr = static_cast<uint8_t*>(
      alloc_.mapMemory(uniform_buffer_allocs_[buffer_index].allocation));

  std::memcpy(mapped_ptr + 0, &t, sizeof(t)); 
  std::memcpy(mapped_ptr + sizeof(t), &v, sizeof(v));

  alloc_.unmapMemory(uniform_buffer_allocs_[buffer_index].allocation);

}

