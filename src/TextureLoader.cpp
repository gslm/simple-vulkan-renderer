#include <TextureLoader.hpp>

#include <OpenImageIO/imageio.h>

#include <utils/LocalStackMemoryPool.hpp>

#include <array>
#include <limits>
#include <memory>

namespace {
using StagingResources = TextureLoader_detail::StagingResources;
using MipLevelsComputationResources =
      TextureLoader_detail::MipLevelsComputationResources;


uint32_t mip_level_count(vk::Extent2D extent)
{
  return static_cast<uint32_t>(
      std::floor(std::log(std::max(extent.width, extent.height)))) + 1;
}

void transition_layout_to_dst_optimal(
  vk::Device device, vk::Queue queue, 
  const TextureLoader::Texture& texture,
  vk::CommandBuffer cmd, vk::Fence fence)
{  
  //const uint32_t mip_levels = mip_levels_count(texture.extent);

  const auto layout_transition_barrier = vk::ImageMemoryBarrier()
      .setSrcAccessMask(vk::AccessFlags())
      .setDstAccessMask(vk::AccessFlagBits::eTransferWrite)
      .setOldLayout(vk::ImageLayout::eUndefined)
      .setNewLayout(vk::ImageLayout::eTransferDstOptimal)
      .setSrcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
      .setDstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
      .setImage(texture.image)
      .setSubresourceRange(vk::ImageSubresourceRange { 
            vk::ImageAspectFlagBits::eColor, 0, texture.mip_levels, 0, 1});

  const auto cmd_begin_i = vk::CommandBufferBeginInfo()
    .setFlags(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
  
  const uint64_t disable_timeout = std::numeric_limits<uint64_t>::max();
  device.waitForFences(1, &fence, vk::Bool32(true), disable_timeout);

  cmd.begin(cmd_begin_i);

  cmd.pipelineBarrier(
      vk::PipelineStageFlagBits::eTopOfPipe,
      vk::PipelineStageFlagBits::eTransfer,
      vk::DependencyFlags(),
      {}, {}, layout_transition_barrier);

  cmd.end();

  const auto submit_i = std::array<vk::SubmitInfo, 1> {
      vk::SubmitInfo()
        .setCommandBufferCount(1)
        .setPCommandBuffers(&cmd)
  };

  device.resetFences(1, &fence);
  queue.submit(submit_i, fence);
}

void compute_mip_levels(
  vk::Device device, vk::Queue queue,  
  const TextureLoader::Texture& texture,
  uint32_t mip_levels,
  vk::CommandBuffer cmd, vk::Fence fence)
{
  //uint32_t mip_levels = mip_levels_count(texture.extent);

  const auto cmd_begin_i = vk::CommandBufferBeginInfo()
    .setFlags(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
  
  const uint64_t disable_timeout = std::numeric_limits<uint64_t>::max();
  device.waitForFences(1, &fence, vk::Bool32(true), disable_timeout);

  cmd.begin(cmd_begin_i);
  
  auto src_subresource_layers = vk::ImageSubresourceLayers()
      .setAspectMask(vk::ImageAspectFlagBits::eColor)
      .setMipLevel(0)
      .setBaseArrayLayer(0)
      .setLayerCount(1);

  auto src_subresource_range = vk::ImageSubresourceRange()
      .setAspectMask(vk::ImageAspectFlagBits::eColor)
      .setBaseMipLevel(0)
      .setLevelCount(1)
      .setBaseArrayLayer(0)
      .setLayerCount(1);

  vk::Extent2D src_extent = texture.extent;

  const auto img_barrier_tmpl = vk::ImageMemoryBarrier()
      .setImage(texture.image)
      .setSrcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
      .setDstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED);

  auto pre_layout_transition_barrier = 
    std::array<vk::ImageMemoryBarrier, 1> {
      vk::ImageMemoryBarrier(img_barrier_tmpl)
        .setSrcAccessMask(vk::AccessFlagBits::eTransferWrite)
        .setDstAccessMask(vk::AccessFlagBits::eTransferRead)
        .setOldLayout(vk::ImageLayout::eTransferDstOptimal)
        .setNewLayout(vk::ImageLayout::eTransferSrcOptimal)
  };

  auto post_layout_transition_barrier = 
    std::array<vk::ImageMemoryBarrier, 1> {
      vk::ImageMemoryBarrier(img_barrier_tmpl)
        .setSrcAccessMask(vk::AccessFlagBits::eTransferRead)
        .setDstAccessMask(vk::AccessFlagBits::eShaderRead)
        .setOldLayout(vk::ImageLayout::eTransferSrcOptimal)
        .setNewLayout(vk::ImageLayout::eShaderReadOnlyOptimal)
  };

  for (uint32_t i = 1; i < mip_levels; ++i) {
    const auto dst_extent = vk::Extent2D { 
      std::max(1u, src_extent.width >> 1), 
      std::max(1u, src_extent.height >> 1) 
    };
    
    const auto dst_subresource_layers  = 
      vk::ImageSubresourceLayers(src_subresource_layers)
        .setMipLevel(i);
    
    const auto dst_subresource_range  = 
      vk::ImageSubresourceRange(src_subresource_range)
        .setBaseMipLevel(i);
 
    pre_layout_transition_barrier[0]
      .setSubresourceRange(src_subresource_range);

    cmd.pipelineBarrier(
        vk::PipelineStageFlagBits::eTransfer,
        vk::PipelineStageFlagBits::eTransfer,
        vk::DependencyFlags(),
        {}, {}, pre_layout_transition_barrier);

    const auto blit = std::array<vk::ImageBlit, 1> {
      vk::ImageBlit()
        .setSrcSubresource(src_subresource_layers)
        .setSrcOffsets(
            { vk::Offset3D {0, 0, 0}, 
              vk::Offset3D (src_extent.width, src_extent.height, 1) })
        .setDstSubresource(dst_subresource_layers)
        .setDstOffsets(
            { vk::Offset3D {0, 0, 0}, 
              vk::Offset3D(dst_extent.width, dst_extent.height, 1) })
    };

    cmd.blitImage(
        texture.image, vk::ImageLayout::eTransferSrcOptimal,
        texture.image, vk::ImageLayout::eTransferDstOptimal,
        blit,
        vk::Filter::eCubicIMG);

    post_layout_transition_barrier[0]
      .setSubresourceRange(src_subresource_range);

    cmd.pipelineBarrier(
          vk::PipelineStageFlagBits::eTransfer,
          vk::PipelineStageFlagBits::eFragmentShader,
          vk::DependencyFlags(),
          {}, {}, post_layout_transition_barrier);

    src_subresource_layers = dst_subresource_layers;
    src_subresource_range = dst_subresource_range;
    src_extent = dst_extent;
  }

  post_layout_transition_barrier[0]
    .setOldLayout(vk::ImageLayout::eTransferDstOptimal)
    .setSubresourceRange(src_subresource_range);

  cmd.pipelineBarrier(
    vk::PipelineStageFlagBits::eTransfer,
    vk::PipelineStageFlagBits::eFragmentShader 
      | vk::PipelineStageFlagBits::eComputeShader,
    vk::DependencyFlags(),
    {}, {}, post_layout_transition_barrier);

  cmd.end();

  const auto submit_i = std::array<vk::SubmitInfo, 1> {
    vk::SubmitInfo()
      .setCommandBufferCount(1)
      .setPCommandBuffers(&cmd)
  };

  device.resetFences(1, &fence);
  queue.submit(submit_i, fence);

  device.waitForFences(1, &fence, vk::Bool32(true), disable_timeout);
}

void staging_copy_subimage(
    vk::Device device, vma::Allocator alloc, vk::Queue queue,
    vk::Image dst, void* src,
    vk::Offset2D dst_offset, size_t src_offset, 
    vk::Extent2D extent, 
    size_t pixel_size,
    size_t src_row_size,
    StagingResources staging) 
{
  const uint64_t disable_timeout = std::numeric_limits<uint64_t>::max();
  device.waitForFences(1, &staging.fence, vk::Bool32(true), disable_timeout);

  void* mapped_ptr = alloc.mapMemory(staging.allocation);
  for (uint32_t i = 0; i < extent.height; ++i) {
    std::memcpy(
        static_cast<uint8_t*>(mapped_ptr) + i*extent.width*pixel_size, 
        static_cast<uint8_t*>(src) + src_offset + i*src_row_size, 
        extent.width * pixel_size);
  }
  alloc.unmapMemory(staging.allocation);

  const auto image_copy = std::array<vk::BufferImageCopy, 1> {
    vk::BufferImageCopy()
      .setBufferOffset(0)
      .setBufferRowLength(0)
      .setImageSubresource(vk::ImageSubresourceLayers 
          { vk::ImageAspectFlagBits::eColor, 0, 0, 1 })
      .setImageOffset(vk::Offset3D(dst_offset, 0))
      .setImageExtent(vk::Extent3D(extent, 1))
  };

  const auto cmd_begin_i = vk::CommandBufferBeginInfo()
    .setFlags(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);

  staging.cmd.begin(cmd_begin_i);
  staging.cmd.copyBufferToImage(
      staging.buffer, 
      dst, 
      vk::ImageLayout::eTransferDstOptimal,
      image_copy);
  staging.cmd.end();

  const auto submit_i = std::array<vk::SubmitInfo, 1> {
    vk::SubmitInfo()
      .setCommandBufferCount(1)
      .setPCommandBuffers(&staging.cmd)
  };

  device.resetFences(1, &staging.fence);
  queue.submit(submit_i, staging.fence);
}

void copy_to_device(
    vk::Device device, vma::Allocator alloc, vk::Queue queue,
    vk::Image dst, uint8_t* src, vk::Extent2D extent, size_t pixel_size, 
    std::array<StagingResources, 2> staging_resources)
{

  const uint32_t staging_size = StagingResources::StagingBufferSize;
  const uint32_t staging_extent = staging_size/pixel_size;
  const uint32_t row_size = extent.width * pixel_size;

  const uint32_t stagings_per_row = row_size / staging_size;

  uint32_t r = 0;
  for (; r < extent.height; ++r) {
    if (stagings_per_row == 0) break;

    for (uint32_t j = 0; j < stagings_per_row; ++j) {
      const uint32_t src_offset = r * row_size + j * staging_size;
      const auto dst_offset = vk::Offset2D(staging_extent * j, r);  

      staging_copy_subimage(
          device, alloc, queue, 
          dst, src, 
          dst_offset, src_offset, 
          vk::Extent2D(staging_extent, 1), 
          pixel_size, 0,
          staging_resources[(j + r) & 1]);
    }
  }

  const uint32_t subimg_width = 
    extent.width - (stagings_per_row * staging_extent);
  const uint32_t subimg_rows_per_staging = staging_extent / subimg_width;

  uint32_t subimg_staging_copies = extent.height / subimg_rows_per_staging;

  uint32_t i = 0;
  for (; i < subimg_staging_copies; ++i) {
    const uint32_t src_offset = 
      i*subimg_rows_per_staging*row_size + stagings_per_row*staging_size;
    const auto dst_offset = vk::Offset2D(
      stagings_per_row*staging_extent, 
      i*subimg_rows_per_staging
    ); 

    staging_copy_subimage(
        device, alloc, queue,
        dst, src, 
        dst_offset, src_offset, 
        vk::Extent2D(subimg_width, subimg_rows_per_staging), 
        pixel_size, row_size,
        staging_resources[(i + r) & 1]); 
  }

  uint32_t rem_subrows = static_cast<uint32_t>(extent.height) % subimg_staging_copies;
  if (rem_subrows > 0) {
    const uint32_t src_offset = 
      i*subimg_rows_per_staging*row_size + stagings_per_row*staging_size;
    const auto dst_offset = vk::Offset2D(
      stagings_per_row*staging_extent, 
      i*subimg_rows_per_staging
    ); 

    staging_copy_subimage(
        device, alloc, queue,
        dst, src, 
        dst_offset, src_offset, 
        vk::Extent2D(subimg_width, rem_subrows),
        pixel_size, row_size,
        staging_resources[(i + r) & 1]); 
  }
}

std::array<StagingResources, 2> make_staging_resources(
    vk::Device device, vma::Allocator alloc, vk::CommandPool command_pool)
{
  std::array<StagingResources, 2> staging_resources;

  const auto cmd_alloc_ci = vk::CommandBufferAllocateInfo()
    .setCommandPool(command_pool)
    .setLevel(vk::CommandBufferLevel::ePrimary)
    .setCommandBufferCount(1);

  const auto fence_ci = vk::FenceCreateInfo()
    .setFlags(vk::FenceCreateFlagBits::eSignaled);

  const auto buffer_ci = vk::BufferCreateInfo()
    .setSize(StagingResources::StagingBufferSize)
    .setUsage(vk::BufferUsageFlagBits::eTransferSrc)
    .setSharingMode(vk::SharingMode::eExclusive);
    
  const auto allocation_ci = vma::AllocationCreateInfo()
    .setUsage(vma::MemoryUsage::eCpuToGpu)
    .setRequieredFlags(vk::MemoryPropertyFlagBits::eHostVisible 
        | vk::MemoryPropertyFlagBits::eHostCoherent);
 
  ut::LocalStackMemoryPool<vk::CommandBuffer, 2> cmd_mem_pool;
  auto sp_alloc = cmd_mem_pool.allocator();

  for (auto& sr : staging_resources) {
    sr.cmd = device.allocateCommandBuffers(
        cmd_alloc_ci, sp_alloc, vk::DispatchLoaderStatic())[0];
    
    sr.fence = device.createFence(fence_ci);
    
    std::tie(sr.buffer, sr.allocation) = 
        alloc.createBuffer(buffer_ci, allocation_ci);
  }

  return staging_resources; 
}

vk::Format vulkan_format(int components, bool hdr)
{ 

  constexpr std::array<vk::Format, 4> hdr_formats = {
    vk::Format::eR32Sfloat,
    vk::Format::eR32G32Sfloat,
    vk::Format::eR32G32B32Sfloat,
    vk::Format::eR32G32B32A32Sfloat
  };

  constexpr std::array<vk::Format, 4> std_formats = {
    vk::Format::eR8Uint,
    vk::Format::eR8G8Uint,
    vk::Format::eR8G8B8Uint,
    vk::Format::eR8G8B8A8Uint
  };

  return hdr ? hdr_formats[components - 1] : std_formats[components - 1];
}

std::tuple<vk::Image, vma::Allocation> allocate_texture_image(
    vma::Allocator alloc, 
    vk::Extent2D extent, 
    vk::Format format,
    uint32_t mip_levels,
    vk::ImageCreateFlags create_flags,
    vk::ImageUsageFlags usage)
{
  const auto image_ci = vk::ImageCreateInfo()
    .setFlags(create_flags)
    .setImageType(vk::ImageType::e2D)
    .setFormat(format)
    .setExtent(vk::Extent3D {extent, 1})
    .setMipLevels(mip_levels)
    .setArrayLayers(1)
    .setSamples(vk::SampleCountFlagBits::e1)
    .setTiling(vk::ImageTiling::eOptimal)
    .setUsage(usage
              | vk::ImageUsageFlagBits::eTransferDst 
              | vk::ImageUsageFlagBits::eTransferSrc)
    .setSharingMode(vk::SharingMode::eExclusive)
    .setInitialLayout(vk::ImageLayout::eUndefined);
  
  const auto allocation_ci = vma::AllocationCreateInfo()
    .setUsage(vma::MemoryUsage::eGpuOnly)
    .setPreferredFlags(vk::MemoryPropertyFlagBits::eDeviceLocal);

  return alloc.createImage(image_ci, allocation_ci);
}

void expand_pixels(
    std::vector<uint8_t>& pixels, 
    int comp_size, int src_comps, int dst_comps,
    void* fill_value)
{

  const size_t src_pixel_size = src_comps * comp_size;
  const size_t dst_pixel_size = dst_comps * comp_size;
  const size_t pixel_count =  pixels.size() / dst_pixel_size;

  //assert(pixels.size() / pixels_count >= 4)

  for (int i = pixel_count - 1; i > 0; --i) {
    std::memcpy(
        pixels.data() + i*dst_pixel_size, 
        pixels.data() + i*src_pixel_size, 
        src_pixel_size);
  }

  for (int i = pixel_count; i > 0; --i) {
    std::memcpy(
        pixels.data() + i*dst_pixel_size - comp_size, 
        fill_value, 
        comp_size);
  }
}

} //namespece


TextureLoader::TextureLoader(DeviceManager& device_manager)
  : device_(device_manager.device())
  , alloc_(device_manager.allocator())
{
  auto [queue, q_family] = device_manager.getGraphicsQueue();
  queue_ = queue;

  auto command_pool = device_manager.getCommandPool(
      q_family,         
      vk::CommandPoolCreateFlagBits::eTransient 
        | vk::CommandPoolCreateFlagBits::eResetCommandBuffer);

  staging_resources_ = make_staging_resources(device_, alloc_, command_pool);
}

TextureLoader::~TextureLoader()
{
  for (auto&&  sr : staging_resources_) {
    device_.destroy(sr.fence);
    alloc_.destroy(sr.buffer, sr.allocation);
  }
}

TextureLoader::Texture TextureLoader::load(
    const std::string& file_path, TextureLoader::LoadFlags flags)
{
  auto in = OIIO::ImageInput::open(file_path);

  if (!in) {
    throw std::runtime_error(
        std::string("TextureLoader::load: ") + OIIO::geterror());
  }

  const OIIO::ImageSpec& spec = in->spec();

  const bool hdr = static_cast<bool>(flags & LoadFlags::Hdr);

  size_t component_size = hdr 
                        ? sizeof(float) 
                        : sizeof(uint8_t);

  OIIO::TypeDesc read_format = hdr 
                             ? OIIO::TypeDesc::FLOAT 
                             : OIIO::TypeDesc::UINT8;

  const int component_count = spec.nchannels == 3 ? 4 : spec.nchannels;

  std::vector<uint8_t> pixels(
      spec.width * spec.height * component_count*component_size);

  in->read_image(read_format, pixels.data());
 
  if (component_count > spec.nchannels) {
    float fill_value_storage = 1.0f;//std::numeric_limits<float>::max();

    if (!hdr) {
      *std::launder(reinterpret_cast<uint8_t*>(&fill_value_storage)) = 255;
    }

    expand_pixels(
        pixels, 
        component_size, 
        spec.nchannels, 
        component_count, 
        &fill_value_storage);
  }

  Texture texture;

  texture.extent = vk::Extent2D(spec.width, spec.height);
  texture.format = vulkan_format(component_count, hdr);

  in->close();

  const bool allocate_mip_levels = static_cast<bool>(
      flags & TextureLoader::LoadFlags::AllocateMipLevels);

  texture.mip_levels =  allocate_mip_levels 
                     ? mip_level_count(texture.extent) 
                     : 1;

  std::tie(texture.image, texture.allocation) = allocate_texture_image(
        alloc_,
        texture.extent, texture.format, texture.mip_levels,
        vk::ImageCreateFlags(), 
        vk::ImageUsageFlagBits::eSampled);

  transition_layout_to_dst_optimal(
      device_, queue_, texture, 
      staging_resources_[0].cmd, staging_resources_[0].fence);
  
  copy_to_device(
      device_, alloc_, queue_, 
      texture.image, pixels.data(), 
      texture.extent, component_count*component_size, 
      staging_resources_);
 
  const bool generate_mip_levels = static_cast<bool>(
      flags & TextureLoader::LoadFlags::GenerateMipLevels);

  uint32_t generate_mip_level_count = generate_mip_levels 
                                    ? texture.mip_levels 
                                    : 1;

  compute_mip_levels(
      device_, queue_, texture, generate_mip_level_count,
      staging_resources_[0].cmd, staging_resources_[0].fence);

  return texture;
}


