#include <RendererLogic.hpp>
#include <vulkan/vulkan.hpp>

#include <iostream>
#include <thread>

namespace {

using RenderSyncResources = RenderLogic_detail::RenderSyncResources;

vk::PhysicalDevice select_physical_device(vk::Instance instance)
{
  const auto phy_devs = instance.enumeratePhysicalDevices();
 
  if (phy_devs.empty()) {
    throw std::runtime_error("No Physical Device Found");
  }

  return phy_devs[0];
}

vk::DebugUtilsMessengerEXT make_debug_messenger(
    const vk::Instance instance, 
    const vk::DispatchLoaderDynamic& dldi) 
{

  using debug_callback_t = decltype(
      std::declval<vk::DebugUtilsMessengerCreateInfoEXT>().pfnUserCallback);

  constexpr debug_callback_t debug_callback = [](
      VkDebugUtilsMessageSeverityFlagBitsEXT severity,
      VkDebugUtilsMessageTypeFlagsEXT type,
      const VkDebugUtilsMessengerCallbackDataEXT* pcallback_data,
      void* user_data) -> VkBool32
  {
    std::cerr << "[" << pcallback_data->messageIdNumber << "] : ";
    std::cerr << pcallback_data->pMessage << std::endl;

    return vk::Bool32(false);
  };

  const auto debug_utils_messenger_ci = vk::DebugUtilsMessengerCreateInfoEXT()
    .setMessageSeverity(
          vk::DebugUtilsMessageSeverityFlagBitsEXT::eVerbose 
        | vk::DebugUtilsMessageSeverityFlagBitsEXT::eInfo
        | vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning 
        | vk::DebugUtilsMessageSeverityFlagBitsEXT::eError)
    .setMessageType(
          vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral 
        | vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation 
        | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance)
    .setPfnUserCallback(debug_callback);

  return instance.createDebugUtilsMessengerEXT(
      debug_utils_messenger_ci, nullptr, dldi);
}

template<size_t N>
std::array<RenderSyncResources, N> make_render_sync_resources(
    const vk::Device device)
{
  std::array<RenderSyncResources, N> render_sync_resources;

  const auto semaphore_ci = vk::SemaphoreCreateInfo();

  for (auto& r : render_sync_resources) {
    r.semaphore = device.createSemaphore(semaphore_ci);
  }
  
  const auto fence_ci = vk::FenceCreateInfo()
    .setFlags(vk::FenceCreateFlagBits::eSignaled);
  
  for (auto& r : render_sync_resources) {
    r.fence = device.createFence(fence_ci);
  }

  return render_sync_resources;
}

} //namespace


RendererLogic::RendererLogic(
    vkwapp::App& app, 
    const RendererLogic::Args& args) noexcept
  : device_manager_(
      select_physical_device(app.instance()), 
      RendererLogic::features(),
      app.window().surface())
  , renderer_(PresentedRenderer(
      device_manager_, 
      app.window().extent()))
  , scene_(Scene(
      device_manager_, 
      renderer_.output_extent(), 
      renderer_.render_pass(),
      Scene::SceneDescription{args.model_path, args.env_path}))
  , instance_dynamic_dispatch_(
      std::make_unique<vk::DispatchLoaderDynamic>(
        app.instance(), 
        vkGetInstanceProcAddr))
{

#if !NDEBUG
  debug_messenger_ = make_debug_messenger(
      app.instance(), *instance_dynamic_dispatch_);
#endif

  const vk::Device device = device_manager_.device();

  render_sync_resources_ = make_render_sync_resources<QueuedFramesCap>(device);
}

RendererLogic::~RendererLogic() 
{
}

void RendererLogic::start(vkwapp::App& app) 
{ 
  render_commands_ = renderer_.AllocateCommandBuffers(2);

  scene_.RecordRenderCommands(
      std::array<vk::CommandBuffer, 2> {
        render_commands_[0], 
        render_commands_[1] });
  
  renderer_.SetCommandBuffersSet(
      vk::Fence(),
      std::array<vk::ArrayProxy<const vk::CommandBuffer>, 2> {
        std::array<vk::CommandBuffer, 1>{ render_commands_[0] }, 
        std::array<vk::CommandBuffer, 1>{ render_commands_[1] }, 
      });
  
  frame_ = 0;
  scene_.Update(
      frame_ & 1, 
      render_sync_resources_[frame_ & 1].fence, 
      app.input(), 
      app.time());
}

bool RendererLogic::loop(vkwapp::App& app) 
{
  //std::cout << frame_ << std::endl;

  std::array<vk::Semaphore, 0> wait_semaphores;
  std::array<vk::PipelineStageFlags, 0> wait_masks; 

  const auto current_index = frame_ & 1;

  renderer_.SubmitRender(
      current_index,
      render_sync_resources_[current_index].fence,
      render_sync_resources_[current_index].semaphore,
      wait_semaphores, 
      wait_masks);
  
  const auto next_index = ++frame_ & 1;

  scene_.Update(
      next_index, 
      render_sync_resources_[next_index].fence, 
      app.input(), 
      app.time());
  
  // This might block. It's called after scene update so to give it time to release
  // potential blocking resources. 
  // It is not clear to me this is always the best strategy.
  renderer_.SubmitPresent(
      current_index, 
      render_sync_resources_[current_index].semaphore);

  using Key = vkwapp::App::Input::Key;

  return !app.input().key(Key::Escape);
}

int RendererLogic::end(vkwapp::App& app) 
{
  const vk::Device device = device_manager_.device();
  device.waitIdle();

  for (auto& r : render_sync_resources_) device.destroy(r.semaphore); 
  for (auto& r : render_sync_resources_) device.destroy(r.fence); 

#if !NDEBUG
  app.instance().destroy(
      debug_messenger_, nullptr, *instance_dynamic_dispatch_);  
#endif

  return 0;
}
