
#include <GLFW/glfw3vk.h>
#include <vkwapp/detail/AppInternal.h>
#include <vkwapp/detail/TimeInternal.h>
#include <vulkan/vulkan.hpp>

#include <vkwapp/App.h>
namespace vkwapp {

struct App::Window::Impl {
    detail::AppInternal* app_;
};

struct App::Input::Impl {
    GLFWwindow* window_;
};

struct App::Time::Impl {
    detail::TimeInternal* time_;
};

namespace {

vk::Instance make_instance(
       const std::vector<const char*>& user_exts, 
       const std::vector<const char*>& window_exts,
       const std::vector<const char*>& layers)
{
	std::vector<const char*> exts(user_exts.size() + window_exts.size());
    std::copy(user_exts.begin(), user_exts.end(), 
        std::copy(window_exts.begin(), window_exts.end(), exts.begin()));
    
  std::sort(exts.begin(), exts.end(), 
            [](const char* a, const char* b) {
                return std::strcmp(a, b) < 0;
            });
    
  auto exts_last = std::unique(exts.begin(), exts.end(),
            [](const char* a, const char* b) {
                return std::strcmp(a, b) == 0;
            });
    
  exts.resize(std::distance(exts.begin(), exts_last));

  return vk::createInstance(vk::InstanceCreateInfo { 
    vk::InstanceCreateFlags(), 
    nullptr, 
    static_cast<uint32_t>(layers.size()), layers.data(), 
    static_cast<uint32_t>(exts.size()), exts.data() 
  });
}

std::vector<const char*> glfw_required_extensions()
{
  uint32_t count; 
  const char** glfw_exts = glfwGetRequiredInstanceExtensions(&count);

  std::vector<const char*> exts(count);
  std::copy(glfw_exts, glfw_exts + count, exts.begin());

  return exts;
}

vk::SurfaceKHR make_surface(GLFWwindow* window, vk::Instance instance)
{
  VkSurfaceKHR surface;
  auto r = vk::Result(glfwCreateWindowSurface(
    static_cast<VkInstance>(instance), window, nullptr, &surface));

	if (r != vk::Result::eSuccess) {
		vk::throwResultException(r, "Surface creation");
	}

  return static_cast<vk::SurfaceKHR>(surface);
}

} //namespace

namespace detail {

AppInternal::AppInternal(
    const App::Args& args,
		const std::vector<const char*>& extencions,
		const std::vector<const char*>& layers) 
{
  glfwInit();

  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

  bool fullscreen = args.flags.test(0) || args.width <= 0 || args.height <= 0;

  GLFWmonitor* monitor = fullscreen 
                       ? glfwGetPrimaryMonitor() 
                       : nullptr;
  
  const GLFWvidmode* mode = monitor 
                          ? glfwGetVideoMode(monitor)
                          : nullptr;

  int window_width = monitor ? mode->width : args.width;
  int window_height = monitor ? mode->height : args.height;

  window_ = glfwCreateWindow(
      window_width, window_height, args.name, monitor, nullptr);

  if (!window_) {
    throw std::runtime_error(
        "vkwapp::detail::AppInternal::AppInternal: Window creation");
  }

  glfwSetInputMode(window_, GLFW_STICKY_KEYS, GLFW_TRUE);
  glfwSetInputMode(window_, GLFW_STICKY_MOUSE_BUTTONS, GLFW_TRUE);

  instance_ = make_instance(
      extencions, glfw_required_extensions(), layers);

  surface_ = make_surface(window_, instance_);
}

AppInternal::~AppInternal()
{
  instance_.destroy(surface_);
  instance_.destroy();
  glfwDestroyWindow(window_);
  glfwTerminate();
}

void AppInternal::update()
{
  glfwPollEvents();
  time_.update();
}

bool AppInternal::should_close() const
{
  return glfwWindowShouldClose(window_);
}

GLFWwindow* AppInternal::window() const
{
  return window_;
}

vk::Instance AppInternal::instance() const
{
  return instance_;
}

vk::SurfaceKHR AppInternal::surface() const
{
	return surface_;
}

const TimeInternal& AppInternal::time() const
{
  return time_;
}

App AppInternal::public_interface()
{
  App app;
  app.instance_ = instance_;
  app.window_.private_data_.get().app_ = this;
  app.input_.private_data_.get().window_ = window_;
  app.time_.private_data_.get().time_ = &time_;

  return app; 
}

} //namespace detail


vk::Instance App::instance() const
{
  return instance_;
}

const App::Window& App::window() const
{
  return window_;
}

App::Window& App::window() 
{
  return window_;
}

const App::Input& App::input() const
{
  return input_;
}

const App::Time& App::time() const
{
  return time_;
}

App::Input::~Input() {}

bool App::Input::key(App::Input::Key key) const
{
  return glfwGetKey(
    private_data_.get().window_, static_cast<int>(key)) == GLFW_PRESS;
}

bool App::Input::mouse_button(App::Input::MouseButton button) const
{
  return glfwGetMouseButton(
    private_data_.get().window_, static_cast<int>(button)) == GLFW_PRESS;
}

glm::dvec2 App::Input::mouse_position() const
{
  glm::dvec2 p;
  glfwGetCursorPos(private_data_.get().window_, &p.x, &p.y);
  return p;
}

App::Window::~Window() {}

vk::SurfaceKHR App::Window::surface() const
{
  return private_data_.get().app_->surface();
}

vk::Extent2D App::Window::extent() const
{
  GLFWwindow* window = private_data_.get().app_->window();

  glm::ivec2 size;
  glfwGetFramebufferSize(window, &size.x, &size.y);

	return vk::Extent2D(size.x, size.y);
}

void App::Window::set_title(const char* title)
{
  GLFWwindow* window = private_data_.get().app_->window();
  glfwSetWindowTitle(window, title);
}

App::Time::~Time() {}

double App::Time::delta() const
{
  return private_data_.get().time_->delta<double>();
}

double App::Time::now() const
{
  return private_data_.get().time_->now<double>();
}

double App::Time::time() const
{
  return private_data_.get().time_->template time<double>();
}

int64_t App::Time::frames() const
{
  return private_data_.get().time_->frames();
}


} //namespace vkwapp

