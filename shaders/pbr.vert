#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 1, binding = 0) uniform Transform {
  vec3 view_position;
  mat4 mv;
  mat4 projection;
} transform;

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;

layout (location = 0) out vec3 out_position;
layout (location = 1) out vec3 out_normal;
layout (location = 2) out vec3 out_view;


void main() 
{
  out_position = position;
  out_normal = normal;

  out_view = transform.view_position;

  gl_Position = transform.projection * transform.mv * vec4(position, 1.0);
}
