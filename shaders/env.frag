#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform PushConstantsBlock {
  float env_lods;
} push_constants;

layout(set = 0, binding = 0) uniform samplerCube env; 
layout(set = 0, binding = 1) uniform samplerCube ird; 
layout(set = 0, binding = 2) uniform sampler2D brdf; 

layout(location = 0) in vec3 focal_plane;

layout(location = 0) out vec4 color;

const float pi = 3.14159265359;
const float epsilon = 0.00001;

vec3 tonemap(vec3 c)
{
  return vec3(1.0 - exp(-c*0.2));
}
/*
vec4 textureEqRectLod(sampler2D eqrect_sampler, vec3 d, float lod)
{ 
  vec2 uv = (vec2(atan(d.z, d.x) + pi, acos(d.y))) / vec2(2.0*pi, pi);
  //uv.y = - uv.y;

  return textureLod(eqrect_sampler, uv, lod);
}
*/
void main()
{
  vec3 rd = normalize(focal_plane);
 
  vec3 c = tonemap(textureLod(env, rd, 0.0).rgb);

  color = vec4(c, 1.0);
}
