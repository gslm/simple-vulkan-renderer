#version 450
#extension GL_ARB_separate_shader_objects : enable


layout(set = 1, binding = 0) uniform View {
  float aspect;
  float focal_length;
  mat4 world;
} view;

layout(location = 0) out vec3 focal_plane;

const vec2[4] position = vec2[4](
  vec2(-1.0), vec2(1.0), vec2(1.0, -1.0), vec2(-1.0, 1.0)
);

const float epsilon = 0.0001;

vec3 quat_rotate(vec4 q, vec3 v)
{
  return v + (2.0 * cross(q.xyz, cross(q.xyz, v) + q.w * v));
} 

void main()
{
  int position_index = (gl_VertexIndex & 1) 
      + ((gl_InstanceIndex + 2) & ((3 + ((~gl_VertexIndex & 2) >> 1)) & 3));

  gl_Position = vec4(position[position_index], 1.0 - epsilon, 1.0);

  focal_plane = (view.world *
    vec4(gl_Position.xy * vec2(view.aspect, -1.0), view.focal_length, 0.0)).xyz; 

  //focal_plane = vec3(gl_Position.xy * 0.5 + 0.5, 0.0);
}
