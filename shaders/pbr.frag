#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(push_constant) uniform PushConstantsBlock {
  float env_lods;
} push_constants;

layout(set = 0, binding = 0) uniform samplerCube env; 
layout(set = 0, binding = 1) uniform samplerCube ird; 
layout(set = 0, binding = 2) uniform sampler2D brdf; 

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec3 view;

layout(location = 0) out vec4 color; 

//const float pi = 3.14159265359;
//const float epsilon = 0.00001;

struct Material {
  vec3 f0; 
  float metallic;
  vec3 albedo;
  float roughness;
};

vec3 tonemap(vec3 c)
{
  //return c;
  return vec3(1.0 - exp(-c*0.2));
}

vec3 fresnel_schlick(float ndotv, vec3 f0, float r)
{  
  //return f0 + (1.0 - f0) * pow(1.0 - ndotv, 5.0);
  float p = 1.0 - ndotv;
  float p2 = p*p;
  return f0 + (max(vec3(1.0 - r), f0) - f0) * p2*p2*p;//pow(1.0 - ndotv, 5.0);
}

vec3 lighting(vec3 n, vec3 v, Material mat)
{
  float ndotv = max(0.0, dot(n, -v));

  vec3 diff = mat.albedo * texture(ird, n).rgb;

  vec2 spec_brdf = textureLod(brdf, vec2(ndotv, mat.roughness), 0.0).rg;
 
  float lod = push_constants.env_lods*mat.roughness;
  vec3 spec_pref = textureLod(env, reflect(v, n), lod).rgb;

  vec3 f = fresnel_schlick(ndotv, mat.f0, mat.roughness);

  vec3 spec = spec_pref * (f*spec_brdf.x + spec_brdf.y);

  return  (1.0 - f) * (1.0 - mat.metallic) * diff + spec;
}

void main() 
{
  vec3 n = normalize(normal);
  vec3 v = normalize(position - view);

  const Material mat = Material( 
    vec3(0.04),//vec3(1.0, 0.71, 0.29),
    0.0,
    vec3(0.01, 0.01, 0.01)*0.2, 
    0.3 
  ); 

  color = vec4(tonemap(lighting(n, v, mat)), 1.0);
}
